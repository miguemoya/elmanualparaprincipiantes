# ![debian](img/debian-bouton.png) ¿Debian? ¿Qué es eso?

![debian](img/debian-scratch.png) Distribución, software libre, comunidad, paquetes, código fuente... ¿pero qué es Debian en realidad?

## El sistema Debian GNU/Linux

**Debian** es un sistema operativo **libre** (como libertad de expresión) y **gratis** (como en cerveza gratis). 
Debian permite que su computadora funcione y le ofrece un conjunto completo de **Software Libre** para 
todas las prácticas habituales (navegar por la web, enviar correos electrónicos, reproducir archivos multimedia, 
hacer tareas de oficina), y mucho más...![](img/smile.png)  
Esta colección de Software Libre proviene en gran medida del
[proyecto GNU](http://www.gnu.org/home.es.html), lanzado en 1983 por[
Richard M. Stallman](http://es.wikipedia.org/wiki/Richard_Stallman). 
El [núcleo de Linux](http://es.wikipedia.org/wiki/Linux_kernel) desarrollado por
[Linus Torvalds](http://es.wikipedia.org/wiki/Linus_Torvalds) vino a completar 
ese software para hacer **GNU/Linux**.

![GNU y Tux, logotipos del proyecto GNU y el núcleo Linux por Péhä CC-BY-SA](img/avatar_tuxgnu_closed-modarp-220.png) 

La distribución **Debian GNU/Linux** se inició con 
[Ian Murdock](https://es.wikipedia.org/wiki/Ian_Murdock) *(rip)* en agosto de 1993. 
Todo comenzó con un pequeño, pero sólido, grupo de hackers de *software libre*, 
que creció hasta convertirse en una comunidad de desarrolladores y usuarios finales grande y bien organizada. 
**Debian** se desarrolla actualmente por miles de [voluntarios](https://www.debian.org/devel/people) 
repartidos [por todo el mundo](https://www.debian.org/devel/developers.loc).

![en memoria de Ian Murdock por Péhä CC-0](img/ian_murdock_20151230_by_peha.png)

Por lo tanto, **Debian** es un *conjunto completo de software libre*. El **software libre** se define por 
las [4 libertades](https://www.gnu.org/philosophy/free-sw.es.html): da a los usuarios finales 
la libertad de **utilizar**, **estudiar**, **compartir** y **modificar** ese software, 
sin infringir la ley. Para ello, es necesario que el desarrollador distribuya 
el código fuente y autorice al usuario final a ejercer los derechos que le otorga una **licencia gratuita**.
Uno de los intereses principales del software libre es permitir a la gente competente 
auditar el código del programa, para asegurar en especial que **sólo hace** lo que se supone que debe hacer.  
Por lo tanto, es una barrera adicional **para proteger su privacidad** ![](img/cool.png).

Debian implementa este principio en su 
[Contrato Social](https://www.debian.org/social_contract.es.html), 
y en particular en las 
[Directrices de Software Libre](https://www.debian.org/social_contract.es.html#guidelines) según Debian.  
Este contrato establece que el proyecto Debian contendrá sólo software libre. 
Por lo tanto, durante la instalación de una distribución *Debian*, 
**no se instalarán controladores que por defecto no sean libres**. 
Sin embargo, el contrato reconoce que algunos usuarios pueden necesitar componentes "no libres" para 
hacer funcionar sus sistemas, como algunos controladores periféricos, o aplicaciones que decodifican 
algunos archivos de música o vídeo, por ejemplo. Es por eso que el software distribuido se separa en
 3 secciones:

- **main** para los paquetes de software libre disponibles por defecto,  
- **contrib** para los paquetes que respetan las directrices de software libre por sí mismos, 
pero que dependen de software no libre, que no cumplen con estas directrices, 
- **non-free** para paquetes que no cumplen con las directrices de software libre.

![logo Debian texte](img/logo_debian_txt_big.png)

La identidad de Debian se desarrolló muy a fondo. **Cada nueva versión estable** es 
probada cuidadosamente por los usuarios antes de ser lanzada. Y este lanzamiento ocurre *cuando está lista*. 
Por lo tanto, se requieren **pocos trabajos de mantenimiento** una vez que el sistema 
está instalado y los problemas que se presentan son muy raros.

Como muchas otras distribuciones libres, Debian no es muy sensible al 
malware (como virus, troyanos, spyware...) y por varias razones:


- Esta gran variedad de software está disponible en **repositorios** alojados en 
**servidores controlados por el proyecto**. Por lo tanto, no es necesario buscar 
programas para instalar en sitios dudosos que distribuyen virus y programas no 
deseados además del que estaba buscando.
- Los derechos de *administrador* y *usuario* están claramente separados, lo cual ayuda 
mucho a limitar los daños: En caso de infección por virus, sólo se verán afectados 
los documentos del usuario. Esta clara separación de los derechos limita también los 
riesgos de error entre el teclado y la silla. Para más detalles sobre los derechos, véase el capítulo 3.7.

![](img/chevalier.gif) **La copia de seguridad** de sus datos de forma regular sigue siendo el 
mejor seguro para protegerlos contra posibles virus o problemas técnicos, pero también 
contra sus propios errores (cap. 9).


## Dónde encontrar ayuda

¿Necesita ayuda? El primer paso, si es posible, es consultar la documentación. 
Luego le siguen los diversos foros de usuarios, y por último un grupo GNU/Linux (GUL), si tiene 
la suerte de estar ubicado cerca. También hay eventos dedicados al software libre 
organizados por diversas asociaciones: podrá fijar citas cerca de su casa consultando 
las agendas del software libre. 
[https://es.wikipedia.org/wiki/Lista_de_eventos_de_software_libre](https://es.wikipedia.org/wiki/Lista_de_eventos_de_software_libre).

- La **documentación integrada en el propio sistema**: en general, las aplicaciones instaladas 
incluyen un manual disponible desde la línea de órdenes (capítulo 3.8) tecleando 'man nombre_aplicación' 
y/o desde el menú gráfico con el botón "Ayuda" de la aplicación.
- La **documentación en línea**: cuando utiliza una distribución GNU/Linux como Debian, 
puede acceder a una documentación en línea detallada, con una lista de las funcionalidades 
de las aplicaciones integradas. Debian le proporciona una documentación oficial: [https://wiki.debian.org](https://wiki.debian.org).
- Autoayuda y **foros de apoyo**: la comunidad del software libre está repartida 
en una gran cantidad de foros, webs y blogs de información. Encontrar el camino en esta 
abundancia de comunicación es a veces difícil, por lo que más bien es preferible 
los sitios dedicados a su propio entorno o distribución. 
Con respecto a Debian, hay 2 foros principales de autoayuda disponibles para asistirle: 
Foro de usuarios de Debian ([http://forums.debian.net/](http://forums.debian.net/)) y 
la Ayuda de Debian ([forum http://www.debianhelp.org/](forum http://www.debianhelp.org/)). 
Puede obtener información adicional en la página de Ayuda Oficial de Debian: [https://www.debian.org/support](https://www.debian.org/support)
- **Asociaciones y LUGs**: si tiene suerte, no vive muy lejos de un 
grupo de usuarios de Linux o de una asociación donde los miembros se reúnen regularmente. 
En este caso no dude en visitarlos para una pequeña charla 
([http://www.tldp.org/HOWTO/User-Group-HOWTO-3.html](http://www.tldp.org/HOWTO/User-Group-HOWTO-3.html))

### Sobre foros, frikis y la terminal

La comunidad GNU/Linux de autoayuda y asistencia esta formada principalmente por **voluntarios apasionados** 
que comparten sus conocimientos con gran placer. También son muy técnicos y son llamados amigablemente 
los frikis (usualmente con barba) con varios años de práctica en computación a su espalda. 
Esta experiencia les lleva a dominar la **terminal**, que es la herramienta más eficiente para administrar 
un sistema GNU/Linux: por lo tanto, las primeras respuestas que se encuentran en los foros se darán 
naturalmente en forma de un conjunto de operaciones de línea de órdenes. No tenga miedo: en la mayoría 
de los casos existe una solución gráfica (usando el ratón dentro de una ventana).  
Pida amablemente y recibirá una explicación.

**Para poder hacer una pregunta en un foro de autoayuda y asistencia** normalmente debe 
registrarse primero. Necesita una dirección de correo electrónico válida para registrarse y 
recibir un mensaje de solicitud de confirmación, así como las notificaciones sobre sus respuestas una vez registrado.

**Antes de hacer una pregunta**, por favor recuerde mirar primero en las Preguntas/Respuestas 
ya resueltas: la mayoría de los foros incluyen una función de búsqueda por palabra clave, que le 
ayudará a averiguar si su problema ya está descrito allí y tiene una solución documentada.

No olvide que un foro normalmente lo mantienen **voluntarios**, no debe confundirlo 
con una organización de servicio al cliente post-venta. ![](img/wink.png).

## Algunos enlaces antes de seguir adelante

- La Fundación por el Software Libre: 
[https://www.fsf.org/?set_language=es](https://www.fsf.org/?set_language=es)
- Acerca de Debian: 
[https://www.debian.org/intro/about.es.html](https://www.debian.org/intro/about.es.html)
- Introduction to Debian: 
[https://www.debian.org/intro/index.es.html](https://www.debian.org/intro/index.es.html)
- La Wiki oficial de Debian: 
[https://wiki.debian.org/es/DebianIntroduction](https://wiki.debian.org/es/DebianIntroduction)
- el rincón del desarrollador: [https://www.debian.org/devel/](https://www.debian.org/devel/)
- los detalles históricos: 
[https://www.debian.org/doc/manuals/project-history/](https://www.debian.org/doc/manuals/project-history/)
- El placer de codificar [https://twitter.com/joyofcoding](https://twitter.com/joyofcoding)

