# ![desktop](img/desktop.png) El entorno gráfico

**Empecemos por ver los diferentes elementos que aparecen en la pantalla.**
En esta sencilla imagen, ya se pueden identificar los primeros elementos que 
se explican a continuación en este manual:

**El "panel" o barra de tareas** (en la parte superior de la pantalla, en 
este ejemplo, pero se puede mover a donde desee)) que incluye, de izquierda a derecha:

- el "menú de aplicación" con la lista del software instalado,
- una colección de lanzadores (pequeños iconos) para las aplicaciones favoritas,
- la lista de las ventanas abiertas actualmente,
- la lista de escritorios virtuales,
- la hora,
- el área de notificación (mostrando la conexión de red por cable),
- el botón de usuario que le permite salir de su sesión de trabajo, detener o reiniciar 
su ordenador,

**una ventana del administrador de archivos** que muestra los directorios que 
contiene su directorio de inicio.

![El escritorio Xfce con el menú de aplicaciones y Thunar](img/deb9-prezdesktop.png)  

Encontrará otras interfaces y diseños en el capítulo dedicado a los entornos de 
escritorio (capítulo 4.2).

## La barra de tareas

El "panel", la barra de tareas, el área de notificación... todas estas palabras 
para hablar sobre la información y la interfaz del lanzador de su sistema.

Por lo general, se trata de una pancarta situada en la parte superior o inferior de la 
pantalla, que muestra varios tipos de información, lanzadores, menús para acceder 
rápidamente a los datos o las aplicaciones, pero también para informarle (ha llegado un 
nuevo correo electrónico, qué hora es, un disco USB acaba de conectarse...) a través 
del área de notificación.

Aquí después de una rápida vista general de los diversos "paneles" en los escritorios 
de Debian: Gnome, Xfce, LXDE, MATE, Cinnamon y KDE:

![panel Gnome-Shell](img/deb9-gnome-panel.png)

![panel Xfce](img/deb9-xfce-panel.png)

![panel LXDE](img/panel_lxde.png)

![panel MATE](img/panel_mate.png)

![panel Cinnamon](img/deb9-cinnamon-panel.png)

![panel KDE](img/deb9-kde-panel.png)

Irrelevante para el tipo de escritorio, la barra de tareas muestra, como mínimo, los siguientes elementos:

- **Un menú de aplicación**, que le permite acceder al software instalado... En general, 
las aplicaciones se clasifican y agrupan por categorías (Multimedia, Ofimática...) y se 
inician haciendo clic con el botón izquierdo del ratón sobre sus etiquetas o iconos.
- **Una lista de ventanas activas** o al menos el título de las ventanas activas. Dependiendo 
del contexto, un clic izquierdo en el botón correspondiente minimizará, restaurará o 
pondrá en primer plano la aplicación de destino.
- **La hora con la fecha** también se muestra cuando el cursor pasa por encima del área. En 
algunos escritorios se abre una agenda haciendo clic con el botón izquierdo del ratón sobre la hora.
- **Un área de notificación** con más o menos información, dependiendo del escritorio, 
destinada a mostrar mensajes procedentes de las aplicaciones (nuevo correo electrónico, conexión de red...)
- **Un botón de acción** para salir de la sesión de trabajo, suspender, detener o reiniciar 
el equipo. Dependiendo de su configuración puede cambiar de usuario, con este botón, y 
aprovechar el modo multiusuario de los sistemas Debian GNU/Linux.

Como casi todos los elementos de los escritorios GNU/Linux, la barra de tareas puede ser configurada 
y adornada con complementos (pequeños módulos especializados) o simplemente eliminada ![](img/wink.png) ! 

Un clic con el botón derecho del ratón sobre el "panel" abre un menú desplegable que le permite cambiar 
su configuración (excepto para el escritorio Gnome-3). A continuación un ejemplo de configuración 
para el panel Xfce:

![Acceder a la configuración del panel Xfce con un clic derecho](img/panel_xfce_cfg1.jpg)  

![Las 3 pestañas del panel de configuración de Xfce](img/panel_xfce_cfg2.jpg)  

Para más detalles, consulte la sección descubrir escritorios GNU/Linux (capítulo 4.2).

## Menús

Las diferentes aplicaciones instaladas en su sistema están disponibles a través de varios vectores: 
lanzadores en el escritorio, o en las barras de tareas, y más generalmente usando los **menús**.

En los menús, los programas se **listan y ordenan por categorías**. Al hacer clic con el botón izquierdo 
del ratón en el icono del menú se muestran las entradas de la aplicación y de la sección.
Al hacer clic con el botón izquierdo del ratón en un icono de programa, se inicia la aplicación 
correspondiente. Cada escritorio tiene su propio menú específico:


Menú por categoría de Cinnamon:

![menú Cinnamon](img/deb9-cinnamon-menu.png)

Clásico menú desplegable de Xfce:

![menú Xfce](img/deb9-xfce-menu.png)

Menú de pantalla completa de Gnome-Shell:

![menú Gnome-Shell](img/deb9-gnome-menu.png)

## Ventanas

Las aplicaciones, los datos, las llaves USB, se muestran normalmente dentro de un marco 
decorado. Cada entorno de escritorio decora las ventanas a su manera, pero la mayoría de 
las veces implementan las mismas funciones:

- **la barra de título** en la parte superior muestra el título de la ventana y los botones 
de acción (minimizar/maximizar/cerrar)
- **la decoración** alrededor de la ventana, que incluye dos áreas en la parte inferior para 
cambiar el tamaño de la ventana con un clic izquierdo del ratón
- **el área de estado**: algunas aplicaciones muestran información en la parte inferior de la 
ventana (como el espacio libre disponible en la partición activa, el tamaño del archivo 
actualmente apuntado a...) 

A continuación un ejemplo de ventanas en el escritorio de Gnome:

![El navegador web Firefox y el gestor de archivos en Gnome](img/presentation_fenetres.png)

Las acciones sobre las ventanas se ejecutan generalmente con el ratón, pero también es posible 
hacerlo con el teclado: ya sea directamente con una tecla de función o utilizando un atajo de teclado (cap. 2.3.2).

### Cerrar una ventana

Para cerrar una ventana, haga clic con el botón izquierdo en el botón "cerrar", normalmente 
simbolizado por una cruz en la esquina superior derecha de la ventana.


![Cerrar una ventana con el ratón](img/fermer_une_fenetre.jpg)

### Mover una ventana

Para mover una ventana por la pantalla, simplemente mueva el puntero en la **barra de título** 
(la pancarta superior de la ventana) y mantenga pulsado el botón izquierdo del ratón. 
El puntero cambiará su aspecto habitual:  

de ![puntero](img/pointeur_default.png) a ![principal](img/pointeur_main.png)

La ventana seguirá el movimiento del ratón hasta que suelte el botón izquierdo.
Si no tiene acceso a la barra de título, puede usar la tecla especial [Alt] del 
teclado junto con el clic izquierdo del ratón para agarrar y mover la ventana.

### Cambiar el tamaño de una ventana

Las ventanas se abren automáticamente en la pantalla con un tamaño determinado. 
Para cerrarlas, ya hemos visto que tenemos que hacer clic en el botón de cerrar 
simbolizado por una cruz.

**Para cambiar el tamaño de una ventana**, debe mover el cursor sobre los bordes 
de la ventana o, mejor aún, sobre una de las esquinas inferiores. 
El puntero cambiará entonces su aspecto habitual: de ![puntero](img/pointeur_default.png) 
a ![puntero bl](img/pointeur_bl.png) o ![puntero br](img/pointeur_br.png) 
dependiendo de su posición izquierda o derecha. Cuando el aspecto del puntero cambie, 
presione el botón izquierdo y mueva el ratón para cambiar el tamaño de la ventana como desee. 

Una solución alternativa consiste en utilizar la tecla especial [Alt] del teclado. 
Mueva el puntero dentro de la ventana y, a continuación, pulse la tecla [Alt] y el botón 
derecho del ratón. Después podrá cambiar el tamaño de la ventana.

**Para maximizar la ventana**, haga doble clic (haga clic dos veces rápidamente 
con el botón izquierdo del ratón) en la barra de título (cuando utilice el entorno GNOME) o 
utilice el botón maximizar situado junto al botón cerrar, si está presente (y es el caso 
en la mayoría de los escritorios).

## Escritorios virtuales

Para evitar sobrecargar el espacio de trabajo, los entornos Debian GNU/Linux permiten, 
desde hace varios años, el concepto de escritorio virtual: los elementos mostrados en 
su pantalla se encuentran en un "escritorio". Puede tener varios, lo que significa que 
si se muda a otro "escritorio", al principio estará vacío. Si vuelve al escritorio anterior, 
lo encontrará en el mismo estado en el que lo dejó (con las aplicaciones abiertas).

![novata](img/noob-modarp-circle-icon.png)  
**... "Varios escritorios" ... ¿Qué quieres decir exactamente?**  
Bueno, es como tener **varias pantallas** para su computadora, alineadas
 **detrás de cada una**, con la capacidad de cambiar el orden a voluntad. 
Abre el navegador de Internet a pantalla completa en el primer escritorio virtual, y 
después le apetece echar un vistazo a las últimas fotos de su familia.
Así que **va al siguiente escritorio** que resulta estar vacío, y entonces puede mostrar allí 
las imágenes también en modo de pantalla completa, y volver al primer escritorio y disfrutar de 
todas las propiedades verdaderas para la comodidad de la navegación.  
También es un buen medio para **organizar las tareas**: el primer escritorio está reservado 
para las aplicaciones de Internet, el segundo para las aplicaciones multimedia, el tercero 
para el trabajo de oficina y así sucesivamente, permitiéndole dejar abierto su trabajo en 
curso, para futuras modificaciones, sin afectar las otras actividades.

Cada entorno tiene su propia forma de representar los escritorios virtuales:

**Gnome** muestra escritorios reducidos en un panel lateral:

![Gnome y 4 escritorios virtuales mostrados en el panel derecho](img/deb9-gnome-bureau-virtuel.png)

**Xfce** muestra los escritorios virtuales en su panel superior, con formas de marco que 
representan los diferentes espacios de trabajo.

![Xfce y 4 escritorios virtuales mostrados en el panel](img/deb9-xfce-bureau-virtuel.png)

## Gestión de archivos en GNU/Linux

Debian GNU/Linux considera todos los datos, vídeos, documentos e imágenes como archivos, 
y estos archivos están organizados en directorios.  
Debian es un sistema operativo (la gran pieza de software que hace que su computadora funcione) 
que organiza los datos según sus respectivas direcciones, es decir, la ruta a seguir, 
para acceder a ellos, desde el punto de partida general 
(la raíz del sistema identificada por el símbolo "/").

![novata](img/noob-modarp-circle-icon.png)  
**... la imagen de la abuela es un "archivo" que tiene una "dirección" ¿¿¿relativa a una "raíz"???**  
Imagine que su computadora es como su casa. Si su libro favorito se encuentra en un 
lugar determinado, este "lugar" es como una dirección relativa a la "casa".  Por 
ejemplo, si el libro está en el segundo cajón de la mesita de noche, se puede 
definir su dirección (el camino a seguir para llegar a ella) así: 
Casa, dormitorio, mesita de noche, segundo cajón, libro favorito.  
Para separar los distintos "elementos" utilizamos el símbolo "/", que da la siguiente 
dirección en lenguaje informático: casa/dormitorio/mesita de noche/2º cajón/libro favorito.  
La "raíz del sistema" está simbolizada por un simple "/". El directorio principal que contiene 
todos los datos del usuario está simbolizado por "/home/", y su directorio personal se 
llama "/home/Alice/".  
Si volvemos al ejemplo del libro favorito, su dirección podría ser:  
"/home/Alice/Documents/books/my_favorite_book.pdf"

### Los datos personales

Los datos personales se almacenan en su *directorio personal*, cuya dirección es 
"/home/su_nombre_de_usuario". Están organizados en directorios para facilitar la 
búsqueda y consulta de los archivos, por usted mismo o por algunos programas 
(es bastante lógico que un visor de fotos se abra primero en el directorio "Imágenes").

Para ayudarle a organizar mejor los datos, Debian se distribuye con pocos directorios 
ya definidos en su directorio personal: Escritorio, Documentos, Descargas, Modelos, 
Música, Imágenes y Videos.

![El gestor de archivos del sistema Nautilus, en Gnome, con dos pestañas abiertas](img/donnees_perso.png)

**NOTA**: La mayoría de los objetos que se encuentran en el sistema de archivos 
de la computadora son archivos o directorios.  
En las siguientes secciones de este manual utilizaremos la palabra "Elemento" cuando 
la distinción entre "Archivo" y "Directorio" sea irrelevante. 

### Los archivos ocultos

Algunos elementos dentro de su directorio personal, se relacionan con la configuración 
de su interfaz, las diferentes fuentes que está utilizando, las contraseñas almacenadas en Firefox, etc.  
No es necesario que este tipo de datos se muestren siempre en la pantalla cuando 
se consultan las fotos o los documentos: por eso estos elementos están *ocultos*.

Se identifican por la forma "/home/su_nombre_de_usuario/.archivo_oculto". Tenga 
en cuenta el "**.**" (punto) delante del nombre del archivo.  
Cuando quiera visualizarlos pulse juntos [Ctrl]+'h' (h obviamente para "hidden" oculto en inglés), 
o desde el menú de la ventana de su gestor de archivos vaya a "Ver" > "Mostrar archivos ocultos":

![Mostrar archivos ocultos en Nautilus en Gnome](img/fichiers_caches.png)  

### Archivos del sistema

Debian GNU/Linux incluye una colección de programas para navegar por Internet, 
en su directorio personal, entre todos los álbumes de fotos, etc. Estos programas 
se almacenan en "directorios del sistema".

Estos directorios están protegidos contra escritura y algunos también contra 
lectura: esto significa que puede consultar algunos de ellos, pero no puede 
modificar ninguno como si fuera un simple usuario.  
Para modificar estos elementos, debe utilizar la cuenta de administrador del 
sistema: "root" (cap. 3.8.3).

## Un ejemplo de administrador de archivos del sistema: Thunar

![](img/thunar.png)  
**Thunar** es el administrador de archivos por defecto en el escritorio **Xfce**. 
Este programa le permite navegar por los diferentes directorios, "Imágenes", 
"Documentos", etc. utilizando el ratón (haga doble clic sobre un directorio para 
ver su contenido) o el teclado (navegue entre los directorios con las teclas de 
las flechas y pulse [Intro] para abrir el seleccionado). La función de un gestor 
de archivos es mostrar en pantalla el contenido de estos directorios: los datos 
personales. Se puede acceder a Thunar desde el menú de la aplicación Xfce en la 
barra de tareas, bajo el nombre " administrador de archivos ".

Cada entorno de escritorio GNU/Linux tiene su gestor de archivos dedicado (si no, 
no tiene gracia) con una interfaz ligeramente distinta a la de Thunar. 
Sin embargo, encontrará las mismas funciones de consulta, búsqueda y modificación 
de los datos.

### Presentación simplificada de Thunar

Cuando Thunar muestra su directorio personal, una serie de información y herramientas 
ya están disponibles para usted: 

![Presentación del administrador de archivos Thunar](img/thunar_default.png)

- **#1 La barra de título**: muestra el directorio que se está visualizando y el 
programa que se ha iniciado. Incluye los "botones de acción" que permiten, con un 
clic izquierdo del ratón, minimizar la ventana (luego se colocará en la barra de 
tareas), maximizar la ventana (ocupará toda la pantalla), cerrar la ventana o 
cualquier otra acción disponible con el gestor de ventanas en uso.
- **#2 La barra de menú**: cada menú le da la posibilidad de actuar sobre los 
elementos (copiar/pegar/eliminar/renombrar, etc.), cambiar la ubicación, el 
estilo de visualización (iconos, lista detallada, lista compacta), e incluso 
cerrar la ventana, siempre con un clic izquierdo.
- **#3 La barra de ubicación**: le dice en qué directorio se encuentra actualmente 
y, si hay suficiente espacio, los últimos directorios visitados. 
- **#4 La barra de pestañas**: le dice qué directorios están abiertos en las ventanas 
activas. Las pestañas Thunar funcionan como las de su navegador de Internet.
- **#5 El panel lateral**: muestra los directorios principales (directorio personal, 
papelera, sistema de archivos, escritorio), los accesos directos (ditectorios favoritos) 
y los volúmenes externos (llaves USB o unidades de disco duro conectadas como dispositivos 
USB); un clic izquierdo en la etiqueta mostrará el directorio seleccionado. Un clic en 
el medio abrirá el directorio en una nueva pestaña (útil para copiar o mover datos). 
El panel lateral también puede mostrar la estructura de árbol del sistema, es decir, 
el conjunto completo de directorios y archivos ordenados jerárquicamente. 
Puede enmascarar/visualizar el panel lateral con la combinación de teclas [Ctrl]+'b'.
- **#6 El panel principal**: muestra el contenido del directorio. Si este directorio 
incluye otros directorios, se colocan delante de los archivos "simples" (como fotos, 
documentos PDF, etc.) y, por defecto, se ordenan por orden alfabético.
- **#7 La barra de estado**: muestra el número de elementos del directorio en cuestión 
y el espacio libre restante en el sistema de archivos actual , o indica si uno o varios 
elementos están seleccionados.

### Uso y funcionalidad de Thunar

Thunar le permitirá consultar los datos, clasificarlos y modificarlos.

Tenga en cuenta que otros administradores de archivos tienen un aspecto diferente (GTK3), 
como por ejemplo colocar el menú de aplicación directamente en la barra de título como 
por ejemplo Gnome (capítulo 4.2.1).

#### Consultar los datos

Consultar los datos es muy sencillo. Inicie Thunar, que se abre de forma predeterminada 
en su directorio personal. A continuación, puede decidir abrir otro directorio 
específico en función del tipo de datos que esté buscando.

**Para abrir o explorar un directorio**, coloque el puntero del ratón sobre él: 
un doble clic con el botón izquierdo abrirá este directorio en la misma ventana. 
Un clic en el medio abrirá el directorio en una nueva pestaña de la ventana.

También puede hacer clic en los accesos directos ubicados en el panel izquierdo 
de la ventana de Thunar.

**Para abrir un archivo**, coloque el puntero del ratón sobre él: un doble clic 
con el botón izquierdo abrirá este archivo con la aplicación asignada por defecto. 
Un clic con el botón derecho mostrará un menú contextual que, entre otras cosas, 
le permite abrir el archivo con otra aplicación de su elección.

#### Selección de datos

Para seleccionar varios elementos mueva el cursor del ratón en un área vacía de 
la ventana, haga clic con el botón izquierdo del ratón, manténgalo pulsado y 
simplemente muévalo sobre todos los elementos que desee seleccionar. Luego suelte 
el botón. Después de esto, puede eliminar uno o varios elementos de la selección 
utilizando el acceso directo [Ctrl]+clic izquierdo en cada uno de ellos.  
Para más detalles, véase la iniciación simplificada (cap. 2.2.4.4).

Una vez seleccionados, se pueden aplicar las modificaciones a estos elementos 
tal y como se detalla en el capítulo "Modificaciones" a continuación.

#### Clasificar los datos

Va a decir:

> Puedo hacer lo que quiera con mis propios datos

...y **tiene toda la razón** ![](img/big_smile.png) !  
Sin embargo, algunas aplicaciones accederán a los datos más fácilmente si se almacenan 
en los directorios específicos. De este modo, el programa de captura de pantalla 
grabará las imágenes en el directorio "Imágenes", el navegador de Internet grabará 
los archivos descargados en el directorio "Descargas", el reproductor de música 
buscará en el directorio "Música"... Y así sucesivamente.

**Thunar** le permite crear atajos para facilitar el acceso, la clasificación y 
por lo tanto la recuperación de los datos.

**Para crear un atajo**, sólo tiene que "arrastrar y soltar" el directorio deseado 
en el panel lateral. Este directorio estará siempre accesible con un simple clic.


#### Modificación de datos usando el menú contextual

Cuando se hace clic con el botón derecho del ratón en un elemento (es decir, un directorio 
o un archivo), aparece un menú contextual y, a continuación, se le permite realizar una 
serie de acciones/modificaciones sobre el elemento seleccionado.

![Menú contextual en Thunar](img/thunar_menu_contextuel.png)  

En nuestro ejemplo, el menú contextual propone las siguientes acciones:

- **Abrir con la aplicación predeterminada**, aquí el visor de imágenes Ristretto.
- **Abrir con** otra aplicación: si elige esta opción, se abrirá otra ventana que 
le permitirá navegar por el sistema para seleccionar una aplicación diferente 
(inicie la búsqueda en /usr/bin/app_name).
- **Enviar a** es un menú para compartir este elemento a través del correo electrónico, 
o crear un enlace en el escritorio, u otras acciones diferentes dependiendo del tipo de 
elemento seleccionado.
- **Cortar**: esta acción **quitará** el elemento seleccionado con el objetivo de 
**pegarlo** en otro lugar. A continuación, basta con navegar hacia el **directorio 
de destino**, hacer clic con el botón derecho en un área vacía de la ventana y seleccionar 
la acción "**Pegar**" en el menú.
- **Copiar**: esta acción deja el elemento seleccionado en el directorio original, pero 
permite copiarlo (clonarlo) en el directorio de destino, siguiendo el mismo procedimiento 
que el descrito en la acción "Cortar" anterior.
- **Mover a la Papelera**: esta acción **quita** el elemento seleccionado de su directorio 
y lo mueve directamente a la Papelera (ver el siguiente capítulo)
- **Borrar**: borrar definitivamente el elemento seleccionado del sistema.
- **Renombrar**: esta acción permite modificar el nombre del elemento seleccionado.
- **Fijar como fondo de pantalla**: es bastante explícito y sólo aparece si se selecciona un archivo de tipo imagen. 
- **Crear Archivo**: esta acción le permite comprimir uno o varios elementos seleccionados. 
En la ventana que se abre, puede elegir el directorio de destino, navegando en el panel lateral, 
introduzca un nombre para este archivo comprimido y seleccione su tipo. A continuación, haga 
clic en el botón "Nuevo" para iniciar el proceso de archivado.
- **Propiedades**: esta acción permite modificar el nombre de un archivo, la aplicación por defecto 
al abrirlo, asignarle un "icono" o gestionar los derechos de acceso y los "Permisos" del elemento 
seleccionado.

#### Eliminar los datos

El famoso "cubo de basura" (o "papelera") es accesible directamente desde el panel lateral 
de la ventana del Thunar. El icono de "papelera llena" indica que algunos elementos 
están actualmente en la papelera.

![Vaciar la papelera utilizando el menú Thunar](img/thunar_corbeille.png)

**Para vaciar la papelera** y eliminar definitivamente todos los elementos de 
la misma, haz clic con el botón izquierdo del ratón en el icono correspondiente 
y realiza la acción "Vaciar la papelera". También puede utilizar el menú "Archivo" 
y hacer clic en "Vaciar la papelera".

A veces el menú contextual tiene la entrada "Borrar". Tenga cuidado porque esta 
entrada de menú no moverá los archivos a los contenedores de reciclaje: se 
eliminarán de forma inmediata y definitiva.

## Derechos y permisos

Debian es un sistema GNU/Linux **multiusuario**. Por lo tanto, es necesario establecer 
un mecanismo para proteger los elementos que pertenecen a cada usuario, de modo que el usuario 
*Alice* no pueda modificar, por ejemplo, la lista "tax & due" de usuarios *arp*.

Para lograr este objetivo, cada archivo y cada directorio pertenece a un *propietario* y 
a un *grupo de usuarios*.

Para cada elemento, uno puede dar derechos **R**ead (lectura), **W**rite (escritura) y 
e**X**ecute (ejecucion) distintamente a su propietario, a su grupo o a todos los demás 
(es decir, personas que no son ni el propietario ni un miembro del grupo propietario).

Para visualizar esta información, se puede utilizar la orden "ls" (*lista el contenido 
del directorio*), con la opción "-l", dentro de una ventana del terminal (que se descubrirá 
en el siguiente capítulo):

    ls -l
    -rw-r----- 1 arp money  1794 Nov 20 14:46 tax_and_due.txt

Aquí se puede ver que el usuario *arp* tiene los derechos "**r**ead" (lectura) y "**w**rite" (escritura), 
en el archivo 'tax_and_due.txt', mientras que los miembros del grupo *money* (del que forma 
parte el usuario *Alice*) sólo pueden "**r**ead" (leer) este archivo, pero no modificarlo, 
y los demás ni siquiera pueden abrirlo ("**-**", significa ningún derecho).

![derechos y permisos](img/droits-permissions.jpg)

### Derechos y permisos desde el menú contextual

Los administradores de archivos integrados en los distintos escritorios de Debian también le permiten 
mostrar y/o modificar los derechos y permisos de los directorios y archivos del sistema 
"sólo con el ratón". Para ello, debe abrir el menú contextual haciendo clic con el botón 
derecho del ratón en el archivo/directorio que le interese y haciendo clic en "Propiedades":

![menú contextual: propiedades](img/menucontextuel_proprietes.png)

En la nueva ventana que se abre, haga clic en la pestaña "Permisos":

![Edición de derechos y permisos desde el menú contextual](img/menucontextuel_permissions.png)  

En este ejemplo, el archivo pertenece a "Mí" (arpinux), que tiene el acceso de Lectura y 
Escritura, mientras que los otros usuarios o grupos tienen un derecho de acceso de Lectura.


## La consola o terminal

![terminal](img/terminal.png)  
Cuando lanza una aplicación desde el menú, o cuando mueve un archivo de un directorio a otro 
utilizando el ratón, envía instrucciones a su computadora en modo gráfico.

Estas instrucciones también se pueden enviar directamente, sin necesidad de pasar 
por un menú ni iniciar una aplicación, gracias al **Terminal** que permite acceder 
a **la línea de órdenes**. Esta herramienta es más precisa que la interfaz gráfica 
porque le permite utilizar todas las opciones disponibles en su aplicación. También 
es su último recurso cuando la sesión gráfica no está.

La única objeción es que esta cajita negra, llena de signos extraños, es un poco 
aterradora... Pero vamos a desmitificar todo esto y mostrarle que la terminal 
puede convertirse en su nuevo amigo... ![](img/smile.png).

En caso de duda, no dude en hacer sus preguntas en los foros de soporte y 
ayuda (capítulo 1.2).

![advertencia](img/warning.png)  
**¡Nunca ejecute pruebas en la terminal en modo administrador! **: algunas órdenes 
muy útiles como "rm" (**r**e**m**ove / eliminar) le permite eludir la papelera desde el 
principio, pero podría ser devastador en todo el sistema cuando las está ejecutando en modo administrador.

![Chica friki, by Péhä (CC-BY-SA)](img/geekerieeyes-modarp-220.png) 

### Presentación

Comencemos simplemente con lo que ve escrito en la ventana del terminal:

![Presentación de la terminal](img/terminal_presentation.png)  

- **el usuario**, es usted, o más exactamente el que está conectado y usa el terminal
- **el nombre de la máquina**, es el nombre de la máquina en su red local, un nombre 
que introdujo durante la instalación del sistema
- **el camino** es el lugar donde se encuentra actualmente en el sistema de archivos 
(aquí el directorio personal '/home/arp/' simbolizado por la tilde "~")
- **el delimitador del prompt** (simbolizado aquí por el signo "$") después del cual 
entrará en la línea de órdenes. Su salida (o "feed back") se visualizará en la(s) 
siguiente(s) línea(s) de la terminal.

La configuración de la cadena completa del prompt reside en el archivo de configuración 
del intérprete "bash" '~/.bashrc' (archivo oculto en su directorio personal). El programa 
bash maneja las órdenes introducidas en la terminal (para abreviar la historia); 
permite enviar órdenes, tecleadas en la terminal, al sistema informático para darle 
varias instrucciones u obtener alguna información.  
Tenga en cuenta que también puede personalizar la pantalla gráficamente desde el menú
 "Edición > Preferencias".

### Ejemplo de modo usuario

Para domar a la bestia, vamos a empezar con un simple "cd" (que significa cambiar de directorio 
"**c**hange **d**irectory"), que le permite caminar a través del sistema de archivos.

Cuando abre la Terminal, se encuentra por defecto en el directorio personal del usuario 
conectado (aquí es el directorio de inicio /home/arp/). Este directorio incluye 
los subdirectorios Imágenes, Descargas, etc. 

Para entrar en el subdirectorio 'Imágenes', tecleo las palabras "cd Imágenes" - 
sin las comillas - (y la mayúscula es importante aquí, de ahí la necesidad de 
identificar claramente los nombres de los elementos que uno está viendo...) y luego 
pulso [Enter] para *enviar* la orden. Para acceder a este directorio 'Imágenes' desde 
cualquier parte del sistema de archivos, necesita proporcionar su ruta completa 
'/home/arp/Imágenes' (reemplace arp por su nombre de usuario):


![Usando "cd" para navegar por el sistema de archivos en la consola de la Terminal](img/terminal_user1.png)

Puede ver aquí que, dentro del prefijo de la línea de órdenes, la ruta **"~"** se ha 
cambiado a **"~Imágenes"**, porque ha cambiado el directorio en el que estaba, y 
esto es exactamente como abrir un directorio con el administrador de archivos gráficos.

Sin embargo, cuando se abre un directorio con el administrador de archivos, se "ven" todos 
los elementos allí. Dentro de un terminal debe listar los elementos con la orden "ls":

![Lista de directorios y archivos existentes con "ls"](img/terminal_user2.png)

...fácil, ¿no? Ve, no rompió nada. Sí, pero me va a decir que esto es bastante inútil...

OK, pasemos a las órdenes más frías:

- '**uname -r**' da la versión del núcleo de Linux activo.

![consola : uname -r](img/terminal_user3.png)

- '**uptime**' da la hora actual, el tiempo que el sistema lleva funcionando, cuántos 
usuarios están conectados actualmente, el promedio de carga del sistema de los últimos 
1, 5, y 15 minutos, para ver si la lista de espera del proceso no es demasiado larga. 
Si la carga es superior a 1 (para ordenadores antiguos) o 2 (para doble núcleos y +) 
esto significa que un proceso se está ejecutando mientras otro está esperando.

![consola : uptime](img/terminal_user4.png)

- ¿Desea volver a su directorio personal? Apenas un pequeño y simple '**cd**' 
y estará en @home.

![consola : cd](img/terminal_user5.png)

- ¿Su pantalla está un poco abarrotada? Un pequeño '**clear**' vaciará la consola.

![consola : clear](img/terminal_user6.png)

### Ejemplo de modo Administrador

Para ejecutar una orden como administrador, Debian usa el comando "**su**". Se le 
pedirá que introduzca la *contraseña del administrador*, y no se mostrará nada en 
la pantalla cuando la escriba. Es a propósito.

Debian también puede usar "**sudo**"  (hacer como superusuario). Cuando use sudo, 
se le pedirá, esta vez, que introduzca su *propia contraseña*.

![Cambiar al modo administrador con "su"](img/terminal_root1.jpg)

Observe que el signo "**$**" (el delimitador de la línea de órdenes) ha sido sustituido 
por el signo "**#**", que indica la cuenta de administrador "**root**".

Una vez conectado como "root" en la sesión de la terminal, puede lanzar órdenes de 
administración del sistema, por ejemplo aquí, una actualización del repositorio de 
paquetes dentro de una terminal Gnome, usando la orden "apt-get update":

![Terminal en modo administrador: actualización de la lista de paquetes mediante "apt-get update"](img/terminal_root2.jpg)

Mire de nuevo este ejemplo de actualización de la lista de paquetes, es decir, la recuperación 
de la información más reciente sobre paquetes y actualizaciones potenciales. Después de completar 
esta actualización de la lista de paquetes (verificando los repositorios), lanzamos "apt-get upgrade" 
para ejecutar las actualizaciones disponibles para el software que ha instalado.

![Terminal en modo administrador: actualización del paquete mediante "apt-get upgrade"](img/terminal_root3.jpg)

En Gnu/Linux, cuando un proceso debe modificar el sistema, normalmente le pide una confirmación. 
Aquí la terminal le informa sobre el tipo de cambios esperados y espera su consentimiento, 
ya sea simplemente pulsando [Enter] si acepta la opción por defecto (aquí " Sí ") o 
escribiendo "s" para aceptar o "n" para declinar.

Si acepta los cambios, verá el proceso completo de descarga, configuración e instalación 
de los paquetes correspondientes. A continuación, la terminal "le devolverá el control" 
mostrando la línea de órdenes:

![Terminal en modo administrador: ejecución de la actualización](img/terminal_root5.png)

Eso es todo, ¡actualizó su sistema usando la terminal!

Las órdenes individuales tienden a ser aburridas después de un tiempo.... Pero entonces 
es mucho más divertido si se asocian varias de ellas en un 
"[script](https://es.wikipedia.org/wiki/Script)" que permite la automatización 
de una serie de procesos.

Para más información sobre scripts, y si desea "aprender el intérprete de órdenes", 
consulte la página dedicada en linuxcommand.org: 
[http://linuxcommand.org/lc3_learning_the_shell.php](http://linuxcommand.org/lc3_learning_the_shell.php).

Al final de este manual se propone un breve resumen de los comandos de GNU/Linux (capítulo 11).

![novata](img/noob-modarp-circle-icon.png)  
Debe ser extremadamente estricto cuando envíe órdenes en modo *administrador*. Para 
evitar errores, utilice la función de autocompletado. Este mecanismo permite que la 
computadora complete las órdenes que usted comenzó a escribir (3 caracteres son suficientes), 
o sus argumentos correspondientes. Esto se consigue pulsando la tecla [Tab]. 
Si son posibles varias opciones, se le propondrán a usted también.

Algunas aplicaciones en modo gráfico requieren los derechos de administrador. **No debería** 
lanzarlos con **su** o **sudo**, de lo contrario podría romper la sesión gráfica. 
Debería usar en su lugar el comando **gksu** (en escritorios de tipo *gtk*) o **kdesu** (para KDE) 
dependiendo de su entorno de escritorio.

### Lanzar un "terminal de administrador"

Como ha visto en la sección anterior, se utiliza "**su**" (o "**sudo**") en la 
terminal para introducir una orden en modo administrador. También puede iniciar 
directamente la terminal en modo "administrador" mediante una entrada de menú 
o la lista de aplicaciones:

![lanzando la "terminal de administrador"](img/gksuterm.png)

![novata](img/noob-modarp-circle-icon.png)  
Tenga en cuenta que dentro de este tipo de terminal, todas las órdenes introducidas 
se ejecutan con la cuenta "root", y en caso de error o error de pilotaje, el sistema 
puede volverse inestable. Ninguna de las órdenes utilizadas en este manual requiere 
ser identificada como "root", excepto cuando se indique claramente lo contrario.
