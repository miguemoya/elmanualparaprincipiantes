	# ![initiation](img/initiation.png) Una introducción sencilla a los sistemas informáticos

**Comenzamos aquí una página de miedo...**

**Empecemos con una frase simple: Los desarrolladores de software y los usuarios finales 
no hablan el mismo idioma....**

![dev vs users](img/dev_vs_user.jpg)

**Pero entonces... ¿cómo vamos a lograrlo?**

> Fácil: ¡leemos el manual del principiante! ![](img/love.gif)

En un corto período de tiempo, las computadoras se convirtieron en una herramienta esencial de la vida moderna. 
Una cuestión es que la información y la educación no siguen el progreso técnico al 
mismo ritmo, ni las necesidades de los usuarios.

Por lo tanto, es difícil para un "usuario nuevo" descubrir todas las capacidades de su 
computadora. Además, por lo general sólo utilizamos unas pocas aplicaciones.

Intentaremos simplificar este universo en la medida de lo posible, para que pueda sacar 
el máximo partido de su computadora y disfrutar de las capacidades de Debian.

**¿Cómo funciona una computadora?**

Uno lanza aplicaciones, hace clic en iconos, escribe texto en... Se necesita una computadora, 
una pantalla, un teclado, un ratón.

En este manual, le explicaremos el manejo básico para utilizar su ratón 
![mouse](img/mouse.png) y su teclado ![clavier](img/keyboard.png).

**¿Para qué sirve una computadora?**

Parece difícil resumir en pocas frases todo el alcance de la tecnología de la 
información. Sin embargo, su uso está algo más claro: 

- **ver una película**:  ya sea para lo grabado durante las vacaciones de verano, o un DVD, o 
un archivo de vídeo descargado de Internet, Debian le ofrece varios reproductores multimedia.  
Un ejemplo en este manual con VLC (capítulo 6.7)
- **escuchar música**: disfrute de sus cd de audio, de la transmisión directa por radio o de 
su biblioteca de música digital con Rhythmbox (capítulo 6.8) sin ningún problema.
- **buscar algo en Internet**: navegar por la red, visitar páginas, contribuir a 
Internet, usando varias aplicaciones web de Debian, tomemos como ejemplo Firefox (capítulo 6.6).
- **leer o escribir correos electrónicos**: comuníquese con su familia, sus contactos, 
utilizando el cliente de correo electrónico o el navegador de Internet (cap. 6.4)
- **trabajar en formatear documentos o presentaciones**: Debian incluye varias aplicaciones, 
pero la aplicación LibreOffice (capítulo 6.9) le permitirá realizar todo tipo de trabajo 
de oficina usando formatos compatibles.
- **rememorar a través de las fotos de tu familia**: sus memorias en un solo clic, simplemente usando 
los visores de imágenes integrados en todo el escritorio de Debian.
- **imprimir documentos o imágenes**: Debian usa el servidor de impresión CUPS y su 
herramienta de configuración común (cap.6.2.2), pero también puede usar una utilidad integrada 
para una configuración simplificada de la impresora (cap.6.2.1).

Y esto es exactamente lo que va a aprender con **el manual del 
principiante** ![](img/cool.png).


## Ponga a prueba su nivel de habilidades informáticas

Tenga en cuenta que este manual no es inamovible... Nuestros consejos son sólo sugerencias 
con respecto a sus conocimientos informáticos...

Antes que nada, es libre ![](img/big_smile.png) !

Lo más importante es ser lo más honesto posible: todos somos principiantes en un ámbito u otro 
(personalmente, no puedo cambiar los inyectores del carburador de mi coche) y no es culpa mía. 
El problema surge sólo porque las computadoras están ocupando un gran espacio en nuestras vidas 
y penalizan a los novatos. Pero ¡ estamos aquí para cambiar todo eso !

El objetivo de este manual no es transformarle en *sysadmin GNU/Linux* (abreviatura 
de administrador de sistemas), sino simplemente darle las herramientas para **utilizar su 
computadora como usted quiera** !

**¿Un simple principiante?**

¿Nunca o en pocas ocasiones usó un teclado? ¿Todavía se pregunta por qué debe "abrir una ventana" y 
qué es este concepto de "arrastrar y soltar"? Por favor, continúe leyendo este manual y 
siga sus instrucciones. Aprenderá a hacerlo:

- usar el ratón y el teclado: las herramientas para interactuar directamente con la máquina,
- reconocer los elementos básicos de su entorno de escritorio: menús, paneles, ventanas, 
escritorio virtual...
- y después descubrir Debian y sus funcionalidades.

**¿Usuario novato?**

Es usuario de Windows® y/o tiene un poco de experiencia con GNU/Linux, pero nunca 
lo ha instalado: es el momento adecuado para elegir su Debian (capítulo 4) y 
descubrir la interfaz principal de su futuro sistema.

**¿Usuario básico?**

Ya ha utilizado una distribución basada Debian y/o otra distribución libre, y 
sabe exactamente que necesita. Pasemos directamente a las cosas serias con la 
instalación actual (capítulo 5).

## El ratón

El ratón es la **interfaz física** que permite mover el **puntero** en 
la pantalla: los movimientos del ratón están sincronizados con los de la flecha 
pequeña (el puntero) ![puntero](img/pointeur_default.png) en tu escritorio.  
Hay diferentes [tipos de ratón](https://es.wikipedia.org/wiki/Ratón_(informática)); 
Tomaremos aquí el ejemplo del ratón clásico con dos botones y una rueda de desplazamiento.


### Clic izquierdo y doble clic

El **clic izquierdo** ![clic izquierdo](img/clic_gauche.png) (o simple clic) es el más 
común y se utiliza para **apuntar a** (o seleccionar) ya sea una carpeta, o un archivo o 
una imagen, que se puede **abrir** con un **doble clic** (presionando rápidamente 
dos veces el botón izquierdo del ratón). Este clic izquierdo también se utiliza para enviar 
órdenes a la computadora (validando una opción por ejemplo) cuando se pulsa el "botón" o 
algo más sensible al clic (como la cruz cerrando una ventana, por ejemplo).


### Clic derecho

El **clic derecho** ![clic derecho](img/clic_droit.png) se utiliza para abrir un menú 
contextual (una lista variable de opciones, dependiendo del software utilizado y del 
"objeto" apuntado por el ratón) con el fin de modificar un archivo, una carpeta, una 
configuración...

### Clic en el medio

El **clic en el medio** o **rueda de desplazamiento** ![clic-central](img/clic_central.png) 
se utiliza para el desplazamiento y la copia rápida. Si su ratón no tiene ni botón central 
ni rueda de desplazamiento, el "clic en el medio" se puede emular pulsando los dos botones 
(izquierdo y derecho) al mismo tiempo.

### Acciones realizadas con el ratón

La acción principal del ratón es apuntar un elemento para abrirlo (en el caso de un 
documento por ejemplo) o lanzarlo (en el caso de un enlace a una aplicación o 
una entrada de menú). Para ello, no es complicado, simplemente coloque el puntero sobre 
el elemento y haga doble clic con el botón izquierdo del ratón.

Una cosa que **NUNCA DEBE HACER** es hacer clic varias veces en un botón si 
cree que no pasa nada. Lo más probable es que una aplicación no se inicie 
"inmediatamente", depende mucho de su hardware y de la aplicación que se esté 
ejecutando. Por ejemplo, un navegador web tarda mucho más tiempo en iniciarse 
que el gestor de archivos.


#### Arrastrar y soltar

Para mover o copiar gráficamente sus datos, basta con "arrastrarlos" a través de 
la pantalla y "soltarlos" donde quiera (este es el equivalente gráfico de 
la orden **mv**).

Ejemplo: para mover un archivo que acaba de descargar a otro directorio, pulse el 
botón izquierdo del ratón sobre el archivo en cuestión, y mientras mantiene pulsado 
el botón izquierdo, mueva el ratón hacia el directorio de destino y luego suelte 
el botón del ratón:


![arrastrar y soltar: apunte al archivo a mover](img/glisser_deposer_1.jpg)  

![arrastrar y soltar: mantenga presionado el botón mientras mueve el ratón](img/glisser_deposer_2.jpg)  

![arrastrar y soltar: mueva el ratón hacia el directorio de destino](img/glisser_deposer_3.jpg)  

![arrastrar y soltar: suelte el botón](img/glisser_deposer_4.jpg)  

#### Selección de texto

Sitúe el cursor al principio o al final del bloque de texto que desea seleccionar, 
mantenga pulsado el botón izquierdo y mueva el ratón sobre el texto que desea seleccionar. 
A continuación, suelte el botón del ratón.

También puede hacer doble clic (haga clic dos veces rápidamente en el botón izquierdo del ratón) 
en la primera palabra que desee seleccionar y, a continuación, mover el cursor.

![selección de texto en la aplicación Gedit](img/selection_texte.jpg) 

Si es lo suficientemente rápido, un triple clic seleccionará toda la línea o párrafo.

#### Copiar y pegar una selección

**Con el botón derecho**: al hacer clic con el botón derecho se mostrará un menú contextual 
que le permite elegir entre varias acciones, una de las cuales es la solicitud de copiar/pegar.  
Sitúe el cursor en el bloque seleccionado, haga clic con el botón derecho del ratón y seleccione 
la acción "Copiar". A continuación, mueva el cursor a la posición en la que desea pegar el texto 
seleccionado, haga clic de nuevo con el botón derecho del ratón y seleccione "pegar".

![Copiar y pegar un bloque de texto: copie la selección](img/copier_coller_1.jpg)  

![Copiar y pegar un bloque de texto: cursor sobre el destino, clic con el botón derecho > pegar](img/copier_coller_2.jpg)  

![Copiar y pegar un bloque de texto:  selección pegada](img/copier_coller_3.jpg)  

**Con el botón del medio**: este es el método más rápido. Una vez seleccionado el 
bloque de texto, sólo tiene que mover el puntero donde desea pegar la selección 
y hacer un clic en el botón del medio. La copia es inmediata.


#### Selección de varios elementos

Si necesita mover, copiar o borrar varios elementos, puede seleccionarlos juntos.

Para seleccionar un grupo de elementos contiguos: mantenga pulsado el botón izquierdo, 
mueva el ratón para arrastrar un marco alrededor de ellos y suelte el botón cuando 
estén todos seleccionados. A continuación, puede actuar sobre la selección como se 
explicó anteriormente (copiar/pegar o menú contextual)

![Seleccionando varios directorios bajo Gnome](img/selection_contigus.jpg)

Para seleccionar elementos no contiguos, puede hacerlo:

- seleccione cada elemento uno por uno con una combinación de la tecla [Ctrl] 
del teclado y el botón izquierdo del ratón: mantenga pulsada la tecla [Ctrl] y 
haga clic con el botón izquierdo en cada elemento que desee seleccionar.
- o seleccione todos los elementos y luego "elimine" los no deseados utilizando 
una combinación de la tecla [Ctrl] del teclado y el botón izquierdo del ratón: 
mantenga pulsada la tecla [Ctrl] y haga clic con el botón izquierdo en cada 
elemento que desee eliminar de la selección.

![Selección de varios elementos no contiguos en una ventana de Nautilus](img/selection_non_contigus.png)

## El teclado

El teclado es **la interfaz física principal para introducir datos** en la computadora. 
Pero no es sólo el dispositivo que le permite introducir algunas palabras en la barra de 
búsqueda de Internet, o trabajar con un procesador de textos. Incluye también algunas 
teclas especiales, llamadas **teclas especiales**, que permiten ejecutar acciones 
rápidas modificando el comportamiento de las teclas "normales". Las combinaciones de 
algunas teclas "especiales" con otras teclas "normales" forman los ** atajos de teclado**.

**Distribución predeterminada del teclado QWERTY**

![Ejemplo de distribución de un teclado inglés (cc-by-sa)](img/clavier_qwerty.png)  

### Las teclas especiales

Las teclas "no alfanuméricas" del teclado le permiten acceder a funcionalidades adicionales 
durante las fases de acción o edición. Desde el simple retorno de carro dentro de un editor 
de texto con la tecla [Enter], hasta el lanzamiento de una ventana de Ayuda con la tecla [F1], 
a continuación encontrará algunas descripciones de estas teclas especiales:

- **[ENTER]** La primera tecla "especial", que no es realmente una tecla modificadora. Esta es 
la tecla más importante de su teclado ya que le permite terminar una línea de órdenes, lanzar 
una petición de búsqueda. Básicamente esta es la clave que dice "Sí" a la computadora. Cuando se 
abre una ventana de diálogo en la pantalla, ya sea para confirmar una descarga o borrar una aplicación, 
tómese el tiempo necesario para leer el mensaje antes de pulsar [Enter].
- **[Ctrl]** o **[Control]** Situada en la parte inferior del teclado, a ambos lados 
de la barra espaciadora, es la tecla predeterminada que se usa para los atajos. 
- **[Alt]** o **[Alternativa]** De forma predeterminada, esta tecla muestra los accesos directos 
específicos de una aplicación. En una ventana abierta, al pulsar la tecla [Alt] se muestran 
los accesos directos para navegar por los menús o activar algunas acciones. Estas teclas 
de acceso directo se identifican con un guión bajo.
- **[AltGr]** le permite utilizar los caracteres ocultos del teclado. Encontrará más 
información en la sección dedicada a este tema (capítulo 2.3.3).
- **[ESC]** o **[Escape]** Esta tecla cancela la última entrada de un modificador, o 
cierra una ventana de diálogo pidiendo al usuario que haga una elección (equivalente a 
hacer clic en el botón "Cancelar" en la ventana de diálogo).
- **[Tab]** o **[Tabulador]** Simbolizado por dos flechas horizontales opuestas. 
Permite completar una orden o navegar por los distintos campos de un formulario, 
o menús de una ventana.
- **[Mayús]** o **[Mayúsculas]** Tecla simbolizada por una flecha ancha hacia arriba. 
Le permite escribir caracteres en mayúsculas y, a veces, números (dependiendo de la 
distribución del teclado), así como algunos caracteres especiales como "@", "%", "&", etc.
- **[BloqMayús]** o **[Bloqueo de mayúsculas]** Tecla simbolizada por un candado o un [Mayús] más grande, 
hace que todas las letras se generen en mayúsculas hasta que se desactive. Equivalente 
a una tecla [Mayús] hacia arriba permanentemente.
- **[F1]**, **[F2]**...**[F12]** Ejecutar varias funciones... por definición. La tecla [F1] 
se utiliza a menudo para iniciar la función Ayuda dentro de las aplicaciones, [F11] para cambiar 
al modo de pantalla completa; por ejemplo.

### Atajos de teclado

*¿Para qué molestarse?* **¡Porque es mucho más rápido!** ![](img/big_smile.png)

Tenga en cuenta que los atajos se realizan presionando las teclas al mismo tiempo:  
para copiar una selección, mantenga pulsada la tecla [Ctrl] y, a continuación, pulse la tecla 'c'.
Posteriormente, suelte ambas teclas, una copia de su selección se almacena en el "portapapeles" 
(un búfer especial situado en la memoria del sistema operativo).  
A continuación se ofrece un breve resumen de los **atajos de teclado** más útiles:

| atajo | acción |
|----------|--------|
| [Mayús] + flechas | hacer una selección |
| [Ctrl] + 'c' | Copiar la selección actual (en el "portapapeles") |
| [Ctrl] + 'x' | Cortar la selección actual (y guardarla en el "portapapeles") |
| [Ctrl] + 'v' | Pegar la última selección de copiar/cortar |
| [Ctrl] + 'f' | Encontrar una palabra o una expresión |
| [Ctrl] + '+/-' o Rueda de desplazamiento del ratón | Zoom acercar/alejar de la visualización en pantalla |
| [Alt] + [F4] | Cerrar la ventana activa |
| [Alt] + [Tab] | Saltar de la ventana abierta a la siguiente |
| [F1] | Abrir la función Ayuda de la aplicación activa |
| [F11] | Cambiar al modo de pantalla completa |

![noob](img/noob-modarp-circle-icon.png)  
Tenga en cuenta que algunas funcionalidades no sólo están disponibles para bloques de texto (como copiar/pegar),
sino también para archivos: si selecciona varias imágenes en el directorio 'Imágenes', hace [Ctrl]+'c' y luego 
[Ctrl]+'v' en su escritorio, las imágenes seleccionadas se copiarán allí. De la misma manera, 
[Ctrl]+rueda de desplazamiento del ratón acercará o alejará el contenido de su navegador de Internet, 
así como el contenido de una ventana del administrador de archivos del sistema.

### Caracteres especiales

Los teclados no pueden contener tantas teclas como caracteres disponibles. Para escribir 
los caracteres particulares en español, es necesario combinar las teclas como los atajos 
(pulsando simultáneamente las teclas)

Primero, una vista previa de las teclas ocultas del QWERTY:

![Caracteres especiales disponibles en el diseño de teclado QWERTY (cc-by-sa)](img/clavier_qwerty_xtra.png)  

Cada símbolo está asociado a una combinación de teclas. Para escribir "©", hay que 
pulsar simultáneamente [AltGr] + 'c'.

![Caracteres disponibles en la tecla 'c'](img/caracteres_touche.png)  

Si no se siente cómodo con ejercer presión simultáneamente, siempre puede 
copiar y pegar caracteres especiales de 
[una tabla de caracteres](http://www.jchr.be/html/caracteres.htm#).


## Ejercicios en línea

**La página dedicada del manual para principiantes (ratón y teclado):**
[https://lescahiersdudebutant.fr/exercices/index-en.html](https://lescahiersdudebutant.fr/exercices/index-en.html)

**ejercicios con el ratón:**

- [http://pbclibrary.org/mousing/mousercise.htm](http://pbclibrary.org/mousing/mousercise.htm)
- [http://www.mydigitalliteracy.us/](http://www.mydigitalliteracy.us/)

**ejercicios con el teclado:**

- [http://play.typeracer.com/](http://play.typeracer.com/)
- [https://www.typing.com/student/start](https://www.typing.com/student/start)
- [http://www.sense-lang.org/typing/tutor/keyboarding.php](http://www.sense-lang.org/typing/tutor/keyboarding.php)

## Los usuarios

Una de las grandes fortalezas de los sistemas GNU/Linux es la gestión de usuarios. 
El hecho de separar derechos y responsabilidades proporciona una mayor seguridad 
a la hora de ejecutar tareas de administración de sistemas o de intercambiar datos en la red. 
Una pequeña explicación...


### $USUARIO	

![usuario](img/icon_user.png)  
Generalmente USTED es el usuario, por supuesto.  A veces se habla de la 
interfaz Silla-a-Teclado (abreviada ISAT, en inglés CTKI), ya que es totalmente cierto que está 
sentado entre la silla y el teclado, o el ratón. Cuando trabaja en su computadora, ésta no lo ve. 
Siente sólo las acciones realizadas por un usuario con un *nombre de usuario* y a veces una *contraseña*.

Cada usuario tiene permitido realizar cierto número de acciones. Su usuario puede, por 
ejemplo, utilizar el teclado y el ratón, leer y escribir algunos documentos (archivos), 
pero no todos. Llamamos a eso *derechos*: para ejecutar tareas administrativas, 
hay que obtener los derechos del usuario administrador **root** (capítulo 3.8.3).


### ROOT

![root](img/icon_root.png)  
Sólo un usuario tiene todos los derechos, es el *administrador*. Este usuario especial 
es capaz de ejecutar algunas tareas (en particular para la administración del sistema) 
que otros usuarios *normales* no pueden realizar por sí mismos. Pero un solo error en 
una operación realizada por este usuario *root* podría potencialmente fastidiar todo 
el sistema.

En casa, en su computadora de escritorio, puede usar la computadora como *usuario normal* 
y como *administrador*. Debe llevar a cabo acciones bien definidas para cambiar 
de un rol a otro, como por ejemplo introducir la contraseña del administrador root 
(capítulo 3.8.3).

### Separar para asegurar

![chose](img/icon_chose.png)  
Esta clara distinción, que, por cierto, no siempre existe en otros sistemas 
operativos, refuerza la estabilidad y seguridad del sistema Debian GNU/Linux, 
como se menciona al principio de este manual. Cuando se trabaja como usuario *simple/normal* 
no puede hacer que su computadora quede inutilizable (bloquearla), y los virus 
potenciales no pueden infectar todo el sistema.

**Para más detalles sobre derechos y permisos, véase el capítulo 3.7.**

