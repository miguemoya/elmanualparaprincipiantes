# ![glossaire](img/glossaire.png) Glossary

## A

### Administrator

=> With great power comes great responsibility...  
The administrator is a person in the IT department who is responsible for managing 
the company server. He takes care of the system updates, verifies its proper 
functioning, creates new accounts, sets permissions and configures file sharing, 
emails, backups, security scheme ...  
On a computer for private use, the administrator is the one who has the system 
management rights: in this case, yourself.

### ADSL

ADSL means broadband Internet. ADSL (Asymmetric Digital Subscriber Line) is 
practically deployed throughout most of the industrial countries the French and 
allows you to take advantage of speeds up to 30 Megabits/s, which permits you 
to watch videos, download files and navigate the Web very easily.

### ALSA

ALSA (Advanced Linux Sound Architecture) is a Linux kernel module which gather 
the function of several different sound drivers into one. It is used to detect 
sound cards whatever they are; it is very popular and very efficient. It greatly 
simplifies the management of sound on Linux and is often updated.

### Apt

Apt (Advanced Packaging Tool) is the Debian package manager using the .deb 
package formats. There are graphical interfaces like Synaptic or Adept for those 
who are not comfortable with the command lines in the the terminal window.

### Aptitude

Aptitude is an alternative to Apt. It offers other options such as the 
construction of dependency tree, update of package status and many others. 
If Aptitude does not care about broken packages on your system, Aptitude will 
try to repair it during the next update or upgrade, by presenting alternative 
solutions as well as their consequences and let you decide what to do.  
Finally, it is also interesting to see the behavior of Aptitude when removing 
software: it computes useless dependencies and deletes them. On the other end, 
Apt only does only what you clearly ask for.

### Arobase @

The "at" symbol '@' is the little "a" with a circle curling around it. It is 
used to form email addresses (eg user@isp.com). This symbol is not used in 
the web addresses, only for electronic mail.

### Audio Format

An Audio format is a data format used to represent sounds, music and voices 
in digital form, with the purpose of either store or transport them.

### Autonomy

Autonomy is the time that a battery enables a mobile unit to operate without 
having to be recharged by an electric outlet. The autonomy of a laptop is a 
few hours and that a mobile phone a few days in theory.

## B

### Beast

Computer name when it's big and powerfull.

### BIOS

BIOS stands for Basic Input Output System. The BIOS is the software (firmware) 
embedded on the motherboard which allows you to perform basic tasks when you 
turn the computer on (eg read a sector of a hard disk).

### Boot manager

This is the program that launches at startup, allowing the choice of booting 
the computer with one among several operating. The most popular, Lilo and 
Grub are usually installed in the MBR (Master Boot Record) or the GPT 
(GUID Partition Table) of the hard drive. Although Windows has a similar piece 
of software, it is more difficult to use, when implementing a dual-boot 
Linux/Windows (for example), than using Grub.

### Browser

Software used to navigate the Web.

### Bug

A bug is unwanted operation in a computer or a computer program. We are 
talking about bug when the software does not do what you asked, it stops 
or do something weird or shutdown...

## C

### C

The computer language (following the A and B languages) which has become 
one of the most widely used programming languages of all time.

### C++

An evolution of the C programing language. Warning: C+ does not exist!

### Chat

Live chat rooms on the Internet are spaces where you share information via 
text with a group of other users. The ability to converse with multiple people 
in the same conversation differentiates chat rooms from instant messaging 
programs, which are more typically designed for one-to-one communication.

### Click

A click is the action of pressing and releasing (almost) immediately one 
of the mouse buttons. An unspecified "click" actually means a "left-click".

### Client/Server

Common type of architecture on the Internet and more generally in computer 
science: A server contains information, clients connect to it and submit a 
request. Corresponding treatments are performed by the server and the results 
are sent back and displayed on the client computer. The Web is a good example 
of client/server application.
 
### Cloud Computing

Cloud computing is concept gaining wide acceptance: rather than installing 
a program on our computer, which is going to take some space, the current 
trend is to do the work online, without the need to download the software. 
This is a way to do office work with Google Docs without installing anything 
on the computer, a browser is enough.

### Code (to)

to write a program, or to speak the "machine language" (no, it does not mean 
"make beep beep beep" ...).

### Code (Source)

The source code is the text file written by the software developer, which can 
be understood by the compiler and transformed into a directly executable 
program. Some source codes are freely available to developers in particular 
in the context of free software.

### Compile (to)

Start a command that converts source code into an executable program.

### Console

Also named terminal, it is (or not) a graphical window giving access to the 
shell interpretor allowing you to type command lines to be executed.

### Cookie(s)

Files sent by some websites and stored on your hard drive. On subsequent 
connections, your computer let the Web site retrieve data that you previously 
entered. Cookies can only be read by the website that issued them and may contain 
only data that the user voluntarily provided to the site, or that can be 
retrieved from the connection itself (source IP address, etc). They can not read 
the disc's contents or contain an executable program. You can set your browser 
to reject cookies, but you lose an interesting usability (like storing your 
preferences or passwords).

### Copy and paste

Copy and paste represents the action to duplicate a file from one location to 
another. The file is cloned into a new destination. The copy-paste operation is 
done, for example, by right-clicking on a selection and choosing the "Copy" action, 
and then right-clicking in the destination folder and choosing the "Paste" action. 
The keyboard shortcuts for these two actions are the popular [Ctrl]+'c' and [Ctrl]+'v'.

### Core dump

A program crashing under Unix has the effect of copying its entire memory contents 
into a file named "core". Not a good news ... but becoming seldom these days.

### Cut and paste

Cut and paste, moves a file without duplicating it: the file will no longer exists 
in its original location, but only in its destination. The keyboard shortcuts for 
these actions are [Ctrl]+X and [Ctrl]+V.

### Cursor

=> arrow, the movements of which you have a hard time to follow on the screen...  
The cursor is following on the screen the movements of your mouse. Often symbolized 
by a slanted arrow cursor, it lets you point an element: an icon, a menu, a button 
to click on in order to activate a function.

## D

### Debian

=> The mommy of many distributions  
Debian is a community and a democratic organization whose purpose is the development 
of operating systems based exclusively on free software.  
Debian differs from most of the distributions based on it by its non-commercial 
nature and the cooperative governance of the association managing the distribution.

### Dependencies

It is said that there is a dependency, when a package depends on another one to be 
installed and/or running. 

### Desktop Environment

In the Unix world, a DE is a set of programs that provide a user friendly graphical 
interface to the operating system. It usually consists of a window manager and a 
lot of software that fit well with the environment, first and foremost file managers, 
control panels, web browsers, text and image editors, games, and even office and 
messaging suites.

### Device

A device is a piece of hardware that is connected to a computer. USB key, keyboard, 
mouse, printer ... are all devices.

### Dialog

A dialog box is a small window that appears when the system or an application wants 
you to make a choice. You must therefore read the message and click the button that 
corresponds to your choice (Yes or No, Save or Cancel, ...).

### Distribution

Distribution is a GNU/Linux system with a collection of software, which forms a 
complete operating system, ranging from the command line up to the graphical 
environment. The goal is to put together a stable and coherent set of software 
aiming at a particular audience. Distribution provides different tools to easily 
obtain and install the software.  
Some distributions are commercial, that is to say, manufactured and distributed 
by companies, which adhere, more or less, to the free software spirit. Others, 
only developed by volunteers, are completely free.

### Drag and drop

Drag and drop is the action of moving the cursor on an icon, pressing and holding 
a mouse button, moving the mouse cursor to another location, and then releasing 
the button: the selected files follows the mouse and moves directly into this 
new location.

## E

### Email

An email can be written in plain text or in HTML format. In the HTML case, some 
text enrichments are possible (bold, color, image, tables) but some mail client 
software (increasingly rare) do not read messages in HTML format.

### Ergonomics

Ergonomics refers to the ease of handling, understanding and using an equipment 
(eg a phone, a software or a website). More ergonomic it is, and faster the end 
user will master it. An ergonomic system is intuitive.

## F

### File Manager

A file manager or file browser is a computer program that provides an user 
interface to work with file systems. The most common operations performed on 
files or groups of files include creating, opening (e.g. viewing, playing, 
editing or printing), renaming, moving or copying, deleting and searching for 
files, as well as modifying file attributes, properties and file permissions. 
Folders and files may be displayed in a hierarchical tree based on their directory 
structures. Some file managers contain features inspired by web browsers, 
including forward and backward navigational buttons. Some file managers provide 
network connectivity.

### File system

A file system (FS) , represents the way data are organized in a disk partition. 
GNU/Linux has its own FS, called ext2, ext3, ext4, ReiserFS, btrfs ... and handles 
a multitude of file systems from other architectures, including FAT , VFAT (ie FAT32), 
NTFS (DOS/Windows file systems), ISO 9660, etc.  
A file system is called "journalized " when it keeps a record (log) of the operations 
being performed, and then saves them on the disk.  
These types of file systems (for example ext3/4 or Reiserfs and also NTFS) are more 
tolerant to system crashes because they keep the system consistent (no need anymore 
to use *fsck* or *scandisk* at start-up).

### Firewall

The firewall is a software protection located at the entry point of a computer, 
or a local network, to prevent intrusions from the outside. It controls the 
inputs and outputs and transmits only the authorized signals.

### Firmware

A firmware is a program used to connect smart devices (hard drive, DVD burner, 
scanner, ADSL modem) to your computer. It is written in the language understood 
by the electronic device. We can compare the firmware of a device to the BIOS of 
a computer. It is responsible for initializing the device and then executes the 
commands received from more advanced programs.

### Fonts

A font represents a range of characters with a particular size, weight and style 
of a typeface.

### Fork

It is often used in a figurative sense to designate a "branch" or a 
"differentiation" from a common root. It is often through this differentiation 
process that new Linux distributions emerge. More precisely the whole idea is to 
take a basic known kernel or program, to modify it according to some purpose and 
to redistribute it.

### FTP

File Transfert Protocol. Protocol used to transfer files over the Internet. Also 
the name of the program implementing this protocol. It is necessary to have a 
specialized program for access the FTP servers (Example: Filezilla)

## G

### Geek

Although the word may have a broader connotation, a geek is a computer enthusiast, 
usually passionate about other subjects (Eg. science fiction) and generally 
curious. Geek does not mean technology obsessed: geek do not like to only use the 
technology, they love to understand how that works and they do have a critical mind.

### Graphics card

The graphics card is a component of the central unit responsible for the on-screen 
display. Powerful graphics cards also handle the rendering of 3D displays (for video 
games). The main manufacturers of graphics cards are Nvidia and ATI.

### GUI

The graphical user interface refers to how the software is presented on the screen 
to the user. It is the positioning of the elements: menus, buttons, features in the 
window. A well defined graphical interface is ergonomic and intuitive: designed in 
such a smart way that the user understands it immediately.

### Gnome

GNOME stands for **GNU** **N**etwork  **O**bject **M**odel **E**nvironment. It 
is a user friendly free graphical environment which goal is to make the GNU 
operating system usage available to the greatest number of people. This 
interface is currently very popular on the GNU/Linux systems and runs also on 
most of the UNIX-like systems.

### GNU

The GNU project ("GNU's not Unix") is a community initiated in 1983 by Richard 
Stallman to create a free alternative to the Unix operating system.  
GNU is particularly known in the software world because it made major 
contributions such as the development of the GNU compiler collection (GCC), 
the improvements of existing Unix commands or the definition of the free GNU 
General Public License (GPL).  
GNU has also worked on a kernel called HURD, but ultimately it is Linux which 
emerged and became the kernel enabling the usage of all the other bricks of 
the project.

## H

### Hack or Trick

An inelegant but effective solution to a computing problem. Sometime also 
called a workaround.

### Hard Drive

The hard (disk) drive is an important component of a computer. It is the computer 
shed, since its role is to store data. The hard drive contains partitions that 
contain your installed system and your personal data  
=> note : Soft Drive does not exist ...

### Hardware

The collection of physical elements that comprise a computer system (the beast, 
cards, chips, drives, processor, etc.), as opposed to the **Software** part.

### High definition

High Definition is the successor of the television set as we knew it before. 
HDTV is a television system providing an audio quality and an image resolution 
which are substantially higher than that of standard-definition television. The 
difference is really obvious. Video games (PS3 and Xbox360) and Blu-Ray 
(successor to the DVD) also benefit from this technology. However you will need 
some money and compatible hardware to enjoy it (compatible HDTV set, HDMI cable, 
Blu-Ray player ...).

### HDMI

HDMI stands for High-Definition Multimedia Interface. HDMI is a standard for 
connecting together High Definition devices. It is replacing the Scart socket. 
You can connect a Blu-Ray player for example to an HDTV with an HDMI cable, 
and even a PS3 or a Xbox 360.

## I

### Icon

An icon is a pictogram, accompanied by a name, representing an element: folder, 
file, software, shortcut ... 
clicking on an icon will launch a predefined action: open the file, launch the 
software, display the content...

### IM

IM stands for Instant Messaging, and refers to a software where you can communicate 
directly (or chat) with friends and family, possibly share files, using the 
microphone to talk and the webcam to see each others. Unlike a chat, instant 
messaging lets you talk with some people of your contact list, and not just 
anyone on the web.

### Internet

Internet includes all interconnected (wired and wireless) networks in the world 
and their associated websites. With an Internet access, you can access visit all 
the websites of the world in one click, listen to music, communicate, watch videos, 
learn ... let it be from your computer, your tablet or your smart phone.

### IN/OUT

Inputs/Outputs (or I/O) characterize the exchange of information between the 
processor and its associated devices. In practice, within an operating system, 
the inputs are the keyboard, the mouse, the disks, the incoming data from the 
network. When the outputs are the screen, the disks, the printer, the outgoing 
data to the network etc ...

### IP Address

Single address across the Internet network, to uniquely identify a machine. It 
is usually represented by a group of four numbers. If your computer is 
connected to the Internet, it has an IP address that is usually provided at the 
beginning of the connection by your Internet Service Provider (ISP). 

### ISP

Internet Service Providers which let you enjoy Internet. Each of them provides 
an interface allowing you to connect to the Internet, to have an unlimited 
land-line phone and even access to digital TV programs.

### IRC

Internet Relay Chat. Discussion System in real time on the Internet. There are 
several IRC networks themselves divided into "channels" themes.

## J

### Java

Multi-platform programming language by Sun. The principle is that a program 
written once in Java can run on any computer as long as it has the specific 
runtime environment called "Java Virtual Machine". This converts Java virtual 
machine instructions into specific instructions to your computer while 
requiring it to meet certain safety rules.  
Java programs can be embedded within web pages, and in that case they are 
executed while the container page is being displayed. They are named Applet.  
You should be extremely careful, because Java applet can be used as malware 
on your computer.

### JPEG

graphics file format producing an impressive compression ratios compared to 
previously defined formats, but at the expense of image quality. Actually, he 
compression method is usually lossy, meaning that some original image information 
is lost and cannot be restored, possibly affecting image quality. The 
corresponding file extension is JPG.

## K

### Kernel

The core of an operating system, or simply the kernel, is one (if not the most) 
important part of the system. It manages the computer resources and allows 
different components - hardware and software - to communicate with each other.

### Keyboard Shortcut

A keyboard shortcut is a combination of keys pressed simultaneously on the 
keyboard to perform a specific action on the computer. The most commonly used 
keyboard shortcuts are for saving ([Ctrl] + s), to copying ([Ctrl] + c), 
pasting ([Ctrl] + v), or closing a window ([Alt] + F4) ...

### Kiss

The KISS principle, "Keep it Simple Stupid", is a method which advocates 
seeking simplicity in design and avoiding unnecessary complexity. 
Example: Give me a KISS.

## L

### Lag (to)

Run ... very ... slowly. Lagging like a snail.

### Live CD

A live CD (or a live USB key) is a bootable medium. The system starts booting 
the computer and then runs the OS without installation. It actually uses the 
volatile memory (RAM) to run, and does not affect the permanent memory (hard disk).

### Free Software

"Free Software" is a concept of free applications and operating systems, whose 
source codes are left open by the developers, in order for everybody to inspect 
and improve them. One example is the GNU/Linux initiative which regroups free 
and gratis operating systems like Debian, but also the LibreOffice suite, the 
free alternative to Microsoft™ Office, or Firefox and Chromium the free 
alternatives to Microsoft™
Internet Explorer.

### Long (it's not gonna be)

Sentence used by the computer people to say that they still have 2 hours to 
finish (the time to compile, test, fix, compile, test, fix, compile ...)

## M

### Mail

mail refers to a letter sent by Internet via email software (Eg. Icedove). 
The mails are free, usually limited to 10MB, which can be accompanied by attachments 
(images, documents ...) and sent to recipient(s) (At :) and potentially people in 
copy (CC :). Unwanted mails are called junk or spam.

### Memory card

A memory card is a small device able to permanently store digital data, likewise 
a USB key. The memory card is intended to be plugged into digital cameras or 
camcorders. It can store photos and movies, and can be read back by computer 
featuring a memory card slot. Among the most popular formats, we find the 
MemoryStick card for Sony devices, and SD for most others. A recent memory card 
can store thousands of photos.

### Motherboard

The motherboard is the main component of the CPU. Its role is to centralize and 
process all the data exchanged between the processor and the peripheral. The 
motherboard therefore manages the hard disk, the CD/DVD drive, the keyboard, 
the mouse, the network, the various USB ports ...

## N

### No Life

Nolife is an insane geek or nerd so passionate by his computer activity that 
he:she spends all his time with his/her passion neglecting everything else and 
in consequence has no (or very few) social life. No comment ...

### Noob

The "Noob" term (derived from "newbie" or novice) refers to a computer novice. 
It is not necessarily a pejorative term.

### Notification Area

The notification area is usually located at the right of the taskbar. You can 
usually find in there, the time, the network indicator, the sound volume. 
This is where the messages appear when the system has something to tell you 
(eg the battery is running empty, updates are available ...)

## O

### Operating System

This is the set of software that manages the computer hardware and provides 
common services for the application programs. By extension, it is also viewed 
as the main interface with the end users.  
Some examples of operating systems : GNU/Linux, Windows, Mac OS X, FreeBSD.

### OS

Operating System.

## P

### Package Manager

A package manager is the software which installs, updates and uninstalls the 
system packages. On Debian, Synaptic is the graphical front-end of the APT 
Debian package manager.

### Partition

To use certain media such as hard disks, we need to structure them, to break 
them down into rather large subsets: partitions. In turn, each partition is 
then structured as a separate file system. Spliting a hard drive into multiple 
partitions allows, for example, the coexistence, on the same disk, of two 
different operating systems like GNU/Linux and Windows, which use different 
file systems.

### Phishing

Phishing is a scam practice which tricks an user by sending him an email with 
a forged sender address and including a link to a fake website mimicking its 
bank site, or an e-commerce site, and asking him to update its information ... 
If the user get trapped (or phished) then his sensitive information (private 
data, bank account details, ...) can be used to evil ends.

### Pixel

a Pixel is the smallest controllable element of a picture represented on the 
screen. A pixel can display one color at a time. Thus the screen is composed 
with million of these pixels, spread in both directions (height and width), 
and they come together to form the screen image. A pixel is so small that 
you can barely see it with the naked eye. The more pixels compose an image 
and the sharper it is.

### Plugin

A plugin  is a software component that adds a specific feature to an existing 
computer program. Allowing for example a web browser to read more image, 
video, animation or sounds formats. Sometimes also called "addon".

### PS/2 Port

PS/2 port represents a couple of plugs located in the rear of the system unit. 
The purple plug connects the keyboard, while the green one connects the mouse.  
Having said that, we should mention that the PS/2 port not used very often and 
replaced by the universal USB ports.  One of the big disadvantage of this port 
is that it is not a hotplug : a device connected after the computer startup 
won't be recognized.

### Processor

The processor, also named Central Processing Unit (CPU), is the electronic 
circuitry within a computer that carries out the instructions of a computer 
program by performing the basic arithmetic, logical, control and input/output 
(I/O) operations specified by the instructions.  
In particular, it handles the data exchanges between the various components of 
the system (hard drive, memory, mouse, graphics card ...) and performs all the 
computation needed to interact with the you through the data displayed on the 
screen.  
There is also the Graphics Processor Unit (GPU) which is a special component of 
the graphics card which accelerates the creation of images in a frame buffer 
intended for output to a display. 

## Q

### Queue

Used in the sense of "waiting line". For example different files to be printed 
are put first in the printer spooler queue.

### Quit (to)

Terminate the execution of a program. Example : give me a pay raise or a quit my job.

## R

### RAM

RAM stands for Random-Access Memory. It is the computer volatile data storage. Its 
major advantage is the speed at which one can read from or write to it, as opposed 
to other direct access data storage media where the time required to read and write 
data items varies, depending on their locations and/or some mechanical limitations 
such as the media rotation speeds and arm movement delays.

### Right-click

A right-click is the action of pressing and releasing (almost) immediately the mouse 
right button. A right-click will display a contextual menu corresponding to the 
pointed element. In particular, it allows to copy, move, delete and rename the 
pointed element when it is a file or a folder.

### RTFM

Means "Read The Fucking Manual". This is an abbreviation that is thrown wildly at 
the head of someone who asks a question to encourage him to do first his home 
work in the documentation.  
This is because people sometimes ask trivial questions, whose answers are readily 
available in the man pages or on the Internet, and often these people are not used 
to the power of the documentation in the Unix world.

## S

### Search Engine

A search engine is a large software system, hosted by a website and which is designed 
to search for information on the World Wide Web. It lets you ask questions or type 
key words to be searched for. The engine will then return the most relevant results.

### Shortcut

A shortcut is an icon placed anywhere and providing a quick access to a software or 
a location on your computer. Most of the time a shortcut is used to launch a program. 
Deleting a shortcut does not uninstall the program it is associated with, and does 
not delete either the linked file.

### Software

Everything that makes a computer running, except the hardware part.
Applications are software, for example.

## T

### Tab

Generally present in modern Internet browsers, tabs allow to browse multiple sites 
simultaneously. To switch from one site to another, you just have to click on its 
corresponding tab. Tabs are represented like tabs in a workbook.

### Terminal

We call a terminal, the command line console which is available and essential in 
all GNU/Linux distributions.  
Even though many GUIs are available for virtually any application, nothing is more 
efficient, for example, than updating your entire system by typing single command line.

Note : a terminal is a very useful tool to debug a program.

### Troll

In the world of Usenet, forums of all kinds, mailing lists, IRC, and all these places 
open to online discussions, a troll is a person who sows discord by starting arguments 
or upsetting people, by posting inflammatory, extraneous, or off-topic messages with 
the intent of provoking readers into an emotional response. By extension the subject 
of the argument itself.  
Known examples : "GNU/Linux or Windows", "Women in Free Software".

## U

### Unit (Central)

For a desktop, the central unit is the box containing all the electronic equipment 
that allows the computer to operate. The Keyboard, mouse, monitor, speakers etc. 
are all connected to it. The hard disk drive is located in this central unit, 
for example.  
For a laptop, there is no Central Unit. All the electronic components are grouped 
under the Keyboard.

### Unix

Unix is a multitasking, multi-user computer OS. The original Unix system was developed 
in 1969. Several versions were created later on, and today, GNU/Linux is taking its 
inspiration from this family and continues with the same philosophy.  
Unix systems are characterized by a modular design that is sometimes called the 
"Unix philosophy," meaning that the OS provides a set of tools, each of which 
performing a well-defined function, and an unified file system to perform complex 
work-flows.

The main Unices are : the BSD family (NetBSD, FreeBSD, OpenBSD), AIX, Solaris, 
HP-UX, Mac Os X.

### Update

An update is a new version of a program that fix a problem existing in its the 
previous version. The update may fix a security hole, add a new functionality, 
solve an algorithm error ...

### URL

The URL (Uniform Ressource Locator) points to the address of a website like 
"http://www.debian.org". By typing an URL in the browser address bar, you go 
directly to the desired site, without having to go through a search engine. 
You never need to type the %%"http://www."%% address header, simply enter 
"debian.org".

### USB

USB stands for Universal Serial Bus. It is the cable with a rectangular 
connector that wants to be universal: almost all current hardware devices are 
connected to your computer via USB. Computers now have USB ports on the back, 
on the front of the Central Unit, and even sometimes on your monitor.

It usually requires 3 steps to plug an USB connector: try to plug in a first 
try, it does not work. Turn over the connector and retry, it does not work 
too. Retry with the initial position and the connector will be in. This is 
as mysterious as the sock which disappears in the washer...

## V

### Virtual Desktop

It is the full set of the workspaces available on a GNU/Linux desktop. It is 
like having on hand several desktops (with task bar and included windows). 
Very handy to get organized !

### VGA

Video Graphics Array (VGA) refers to the display hardware first introduced with 
the IBM PS/2 in 1987. The term can also mean either an analog computer display 
standard, the 15-pin D-subminiature VGA connector or the 640x480 resolution 
itself. While this resolution was superseded in the personal computer market 
in the 1990s, mobile devices have only caught up in the last few years.

### VPN

Stands for  Virtual Private Network, and refers to an encrypted network on the 
Internet, which allows a company whose premises are geographically dispersed to 
communicate and share documents in a completely secure manner, as if they were 
all connected in a local area network.

## W

### Web

Web is the most common Internet network and refers to all the interconnected 
networks that run across the whole world and connect computers together in 
the manner of a spider web.  
Surfing the Web means : browse the Internet.

### Web Browser

An Internet browser is a software application that allows you to visit websites, 
download files and make some research.

### Wiki

A wiki (Hawaiian word meaning "quick") is a web site whose pages are editable 
by visitors, enabling the creation of collaborative contents. (And aren't you 
afraid to give the keys to strangers ?).

### Window

A window is a rectangular area that appears on the screen to display, for 
example, the contents of a folder, or a text file, or some software output. 
The window can take all the space (full-screen) or only a part of the screen 
real estate. It is possible to view multiple windows simultaneously and drag 
items from one to another using the drag and drop technique.

### Window Manager

The window manager is the X (graphical) client software that controls the 
placement and appearance of windows (title bar, framing, moving, resizing, 
etc..) within a windowing system in a graphical user interface. Most window 
managers are designed to help provide a desktop environment.  
Applications, handle the inside of the window they created and manage their 
content and the interaction with the user.  
It is possible to change the window manager and see all the look and feel of 
the screen change completely.  
When the window manager can do many other things in addition to what has been 
described here, it is a desktop environment like XFCE.

### WWW

Initials of the World Wide Web.

## X

### X Window

X Window System, commonly called X Windows, X11 or just X is the graphics 
subsystem of GNU/Linux. X Window is not only a driver for the video card, it 
is mainly an interface (API) for applications, so that they appear on the 
screen and receive input from the keyboard and mouse.

### X Org

Free X server used by GNU/Linux distributions.

## Y

### Yes We Can

Yes We can make you understand your computer, use it to do what you want and 
share knowledge...

## Z

### Zen

"Stay zen", means keep calm. Zen Buddhism has always been appreciated by hackers. 
This is a fundamental virtue of good programmer, especially in the debugging phase ... 

### Zip

A very popular file compression format. Requires a specific program for decoding 
(expanding) the compressed file.

### Zombie

Program that ended but whose father, informed of its death, does nothing (in 
keeping with the quilt analogy, he is not concerned with the funeral and the 
administrative procedures). It does not exist anymore but is not deleted from 
the processus listr, so it is still a bit alive ... A program whose father was 
destroyed in turn is an orphan.

## Links

- The Jargon Lexicon: http://www.catb.org/jargon/html/go01.html
- WhatIs.com: http://whatis.techtarget.com/
- Techopédia: https://www.techopedia.com/dictionary

