# ![install](img/install.png) Installing Debian

About time to take the plunge ...  
![](img/avatar-handy25-by_peha-ccbysa.png)  
The following section describe the classical **Single-boot installation**: Debian 
will be the only distribution residing on your hard disk and  the install process 
will be automatic (assisted partitioning, with the whole Debian system in a 
single partition).

![noob](img/noob-modarp-circle-icon.png)  
**This manual is intended for beginners**, and does not cover all the install 
capabilities of the Debian system. If you are in a particular situation not 
covered here, look at the on-line Debian installation manual 
([https://www.debian.org/releases/stable/installmanual](https://www.debian.org/releases/stable/installmanual)).  
For installations in a different context (separated /home partition, DualBoot, 
Logical Volume Management, Encryption, Multi-boot, etc.) you will find the links 
to the specific documentations in chapter 5.5.

## Before installing

Most of the users never had to install a system previously (computers are usualy 
delivered with an operating system already installed). And that could be a 
little freaking ... Take the time to be well prepared, take a breath of fresh air, 
and everything is going to be all right.

### Hardware compatibility

One of the first question when you wish to install Debian concerns the hardware 
compatibility: *is Debian going to run smoothly on my computer??*

A fast and simple way to check this out, is to type "Debian" followed by your 
machine type, within your favorite search engine: look for "Debian IBM T60", 
for example. In case of doubts, don't hesitate to ask on a support-and-help 
forum (chap.1.2).

In order to be really sure, you can use a distribution offering a test capability 
through a "live" session (chap.5.3.1).

### Backing up your data

If you anticipate to overwrite your hard disk with Debian, verify first that none 
of your personal data is in there: everything will be lost.

**Remember to back-up your data** before you start messing with the partitions (chap.9)

### Disk space requirement

A Debian distribution occupies 4 GB in average, but you should still plan for a 
minimum of 6GB, to be able to download the updates. For greater safety and if 
you plan to add few applications, reserve 12 GB and you will be comfortable.

### Installation time duration

**To install Debian** from a "net-install" ISO requires some time because the 
software is downloaded from the Debian servers during the process: the time 
duration needed for the installation then depends on the speed and quality of 
your Internet connexion and may vary from 40 up to 90 minutes.

**To install a derivative** or from a Debian Live support, takes far less time 
(around 20 minutes, depending on your computer power), because the packages are 
already included within the downloaded ISO image. However, a system update will 
then be necessary after the installation, because the embedded packages are 
dating back to the time when the ISO image was burned.

### Preparing the hard disk

If you anticipate to install Debian as the unique operating system on your 
machine, you don't have to do anything special: the embedded installer includes 
the tools needed to prepare (I.E. to format) the disk(s).

If you anticipate to install Debian next to another operating system, take good 
care of preparing your hard disk (like defragmenting the Windows partition for 
example). More details in the section dedicated to specific installations (chap.5.5).

## Downloading Debian

Debian ISO images are available for each desktop, in several architecture 
declinations, and supported by various media like netinst, CD, DVD, ...

### Which image to download?

Here after the different ways to obtain a Debian ISO image. If you have other 
questions, read the Debian FAQ: 
[https://www.debian.org/CD/faq/index.en.html](https://www.debian.org/CD/faq/index.en.html).

#### Debian Netinst

If your Internet connection is stable, we encourage you to download an ISO 
image of type "netinst" multi-arch which includes everything you need to install 
Debian on any modern 32 or 64 bits (i386 or amd64) computer, with all the 
desired options: you will be able choose your main interface during the 
installation process.

- The advantage of this ISO image is that it will search for applications on 
Debian servers during installation, allowing you to get a completely 
up-to-date system.
- The disadvantage is that you will have to be connected to the network 
during installation, either wired (by cable, recognized natively in most 
cases) or Wi-fi if recognized.

This is the ISO image used in the installation section of this manual.

To get hold of the "Netinst" ISO image, you can visit this page: 
[https://www.debian.org/CD/netinst/index.en.html](https://www.debian.org/CD/netinst/index.en.html). 
You will find there the torrent links (to share and relieve the main servers) 
and the direct "HTTP" to the ISO images depending on your architecture. Below 
the torrent link to a 64 bits ISO image:

![download link for Debian netinstall](img/debian-netinst-page.png)

Note that if you absolutely need an ISO image already integrating non-free 
firmwares for your hardware (for a Wi-fi installation for example), Debian 
provides you with multi-arch ISOs (installable on 32 rr 64 bts architectures): 
[http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/multi-arch/iso-cd/](http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/multi-arch/iso-cd/)

#### Debian on CD/DVD

To obtain a classic bootable ISO image, pre-configured with a specific desktop, 
you can look at this page: 
[https://www.debian.org/CD/http-ftp/index.en.html](https://www.debian.org/CD/http-ftp/index.en.html).

This ISO file **does not** let you test your system, through a "live" session 
(chap. 5.3.1) to check the hardware compatibility with your PC. It *only* 
allows to install Debian on your computer, which is already cool, and let you 
install directly your preferred desktop, without a stable Internet connection 
at your disposal.

#### Debian torrent

To relieve the main Debian servers and, at the same time, share your ISO image 
with other community members, you can use the BitTorent protocol.

To obtain the list of the available "torrents", you can visit this page: 
[https://www.debian.org/CD/torrent-cd/index.en.html](https://www.debian.org/CD/torrent-cd/index.en.html).

#### Debian live

Other images, called "autonomous", are also available: they let you check your 
particular environment through a "live" session. They include also an 
installation launcher on the desktop, which, once your test is completed, let 
you install Debian directly from the session you are in.
To get hold of a live image, you can visit this page: 
[https://www.debian.org/CD/live/index.en.html](https://www.debian.org/CD/live/index.en.html).

More details on the dedicated "live" section (chap.5.3.1).

Note that if you absolutely need an ISO image already integrating the non-free 
firmwares for your hardware (for a test on Wi-fi for example), Debian puts at 
your disposal ISOs of type Live-non-free: 
[http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/](http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/)

### Verifying the ISO image integrity

In order to verify the integrity of the image, Debian computes the "**md5sum**" 
(short form of "md5 check sum"). This check can be used on all kind of data, 
but is particularly useful when downloading ISO image.

#### Checking md5 on GNU/Linux

The **md5** checking tool is integrated in almost all the GNU/Linux 
distributions. To verify the md5 checksum of a file, just type the following 
command in a terminal emulator, including the path of the file to be checked:

    md5sum debian-xx-amd64-i386-netinst.iso

which will return a result of the type (to be checked against the md5sum given 
by the download site.):

    6753c353cef5f5336079d94562ad15c3  debian-xx-amd64-i386-netinst.iso

**Verification in graphic mode ("with the mouse").**  
The **Gtkhash** program let you also check the md5 signature, but with a 
graphical interface.

- Install 'gtkhash' (using a Terminal command or the Synaptic package manager).
- Launch GtkHash from the application menu.
- In the *File* field: look for the ISO file to be checked.

![GTKHash: selecting the ISO image file to be checked](img/gtkhash1.png)

- In the *Check* field: paste the checksum retrieved from the Internet site.
- Click on the *Hash* button.
- The digital fingerprints (I.E. checksums) will show-up for each hash 
functions (MD5, SHA1, SHA256...)
- If the integrity checksum is exactly matching, a (green) validation symbol 
will be displayed at the right of the field, and in the corresponding one in 
the computed hash field.

![GTKHash: verifying the md5 checksum](img/gtkhash2.png)

#### Verifying md5 on Windows®

To graphically verify the md5 checksum using a Windows® PC, you can use the 
WinMd5Sum software, to be downloaded here: 
http://www.nullriver.com/downloads/Install-winMd5Sum.exe

Once installed, launch it. A small graphical interface is open. In the 
"File Name" field paste or open the Debian image.iso downloaded file. In the 
"Compare" field, make a copy/paste of the md5 checksum retrieved from the 
site given by Debian (see above) and click on the "Compare" button.

![the winMd5Sum software in action on a Debian ISO image](img/winmd5sum.png)

That's all. The md5 checksum is computed within a minute or so (depending 
on the size of the file) and if found to be correct, you can proceed to 
the next step.

### Transfert sur CD/DVD

![dvd](img/dvd.png)  
To burn your Debian ISO on a CD/DVD on a computer under GNU/Linux, simply open 
your favorite burning application, indicate the path of the 'debian-xx-iso' 
downloaded file, and start the media burning.

For the Microsoft® users, read this tutorial made for the Windows®7/8 systems: 
[http://www.digitalcitizen.life/burning-iso-or-img-disk-images-windows-7](http://www.digitalcitizen.life/burning-iso-or-img-disk-images-windows-7)

### ISO transfer on a USB key

![usb](img/usb.png)  
The USB key is the most convenient way to install GNU/Linux distributions, 
because you can change them as often as you want and even test several of them 
in parallel. The key also secures your tests because once the session is over, 
no trace is left on the key nor on the computer which booted from it.

#### From a GNU/Linux system with the terminal

**The recommended method.**  
Plug your USB key in and launch a terminal in "root" administrator mode 
(chap.3.8.3). We are going to identify the USB key to be used, with the command:

    blkid

which returns information of this type:

    /dev/sda1: LABEL="system" UUID="3d378712-1b6e-4f66-b9e8-2a6673c62199" TYPE="ext4" 
    /dev/sdb1: UUID="F9B8-E691" TYPE="vfat"

Here, our key is identified as UUID="F9B8-E691", is formated in "vfat" and includes 
the **sdb1** partition. Note carefully this **sdb1** value, to avoid erasing by 
mistake a partition on your internal hard disk (here **sda1** is another 
partiton on this disk).

The Debian ISO file should be located in the Downloads folder. Let's move in there 
to act on this ISO file (the "$Home" variable replaces the address "/home/your_loginID"):

    cd /$HOME/Downloads

Now, we are going to transfer the ISO content to the USB key, thanks to the 
"**dd**" command.
**Take good care of naming the USB key "*sdb*" and not "*sdb1*"** (in our 
example), because it is the disk device name which is requested, not the partition, 
and don't forget to change the *xx* in the 'debian-xx.iso' file by the 
corresponding version number.  
Within a terminal in "root" administrator mode:

    dd if=debian-xx.iso of=/dev/sdb bs=4M && sync

The transfer duration on the USB key is obviously depending on the size of the 
ISO and on the transfer rate of your USB port. This operation usually lasts 
from 10 to 15 minutes without any sign of activity within the terminal window. 
Once the transfer is completed, the control will be given back to you, that is 
a new command prompt line will be displayed in the terminal.

#### From a Windows® system with Win32DiskImager

Win32DiskImager is a bootable media creation utility for Windows®: it will 
allow you to create a USB key on which you will boot your computer in order 
to install Debian.  
To install it on your Windows® system, go to the main project page and download 
the latest version: https://sourceforge.net/projects/win32diskimager/

Win32DiskImager installs like other Windows®-compatible software. Once in place, 
first plug in your USB stick and note the ID of the disc displayed (disk "F:" 
for example). Then run Win32DiskImager.

![Win32DiskImager](img/win32diskimager.png)

- In **1**, you will choose the downloaded Debian ISO image: Click on the folder 
icon to open the file selector. Win32DiskImager only looks for files of type 
".img" by default: think of setting the search filter to "." and not ".img" 
in order to view your debian-xxx.iso.
- In **2**, you will indicate the device to be used: it is the disk mounted when 
inserting your USB key. **Caution! All data on the key will be deleted** during 
the transfer procedure.
- In **3**, you start the ISO image writing on the key. You can follow the 
progress of the process directly in the Win32DiskImager window.

Wait a moment and you will be in possession of a bootable Debian USB key!

You still have to restart your computer on this bootable key and start the 
Debian installation.

### Booting from the CD/DVD or the USB device

To proceed now with the Debian installation, using your CD/DVD or USB medium, 
you need to ask the computer to boot from this device. If your computer does 
not boot automatically from the desired installation medium, you need to access 
the "Boot menu" or modify the "Boot order" in the BIOS.

Some computers have a function key that let you boot directly from a peripheral 
device, without having to modify the BIOS parameters. Usually, but not always, 
pressing the F12 key at start-up gives you access directly to the boot options. 
On the other end, to access the BIOS configuration you need to tap one key 
like DEL, ESC or F2 during the start-up phase.

More information at: http://www.boot-disk.com/boot_priority.htm

#### The Boot Menu

As mentioned above, the key to access the Boot Menu at start-up may vary from 
one computer to another. This specific key as well as the key to access the BIOS 
configuration is usually indicated during the boot sequence for one or two second:

![open the boot menu](img/bootmenu1.png)

The boot menu let you select the boot peripheral without going into the BIOS 
configuration

![select a disk ti boot from](img/bootmenu3.png)

Use the keyboard direction arrows to select the right peripheral (in this example 
the USB key is the "Removable Devices").

#### BIOS configuration

If the Boot-order menu is not available, you must modify the Boot priority inside the BIOS.

Once entered in the BIOS settings, the operations to execute are very simple, but 
you should be extremely careful to not modify other parameters. Hopefully, in case 
of mistake, the program let you quit without recording the changes, by pressing 
the ESC (or Escape) key. Other keys like F9 or F10 will let you reload the 
default parameters, or to record the changes before quitting:

- **F9**: reload default parameter (or factory setting)
- **F10**: Record the modifications and quit
- **ESC**: cancel the modifications and quit.

The navigation is done by using the directional arrows on the keyboard. One validate 
or enter inside an option using the **ENTER** key. In most of the BIOS models, you 
move until the **Boot** menu is highlighted, then find the peripheral selection for 
the boot (boot device, boor sequence, boot priority) and finally put the various 
devices in the first, second, third etc … positions, reflecting the boot order you wish.

![BIOS: setting the Boot order (sources linuxtrack.net)](img/bios-startup.png)  

Hereafter some peripheral names, like they might appear in your BIOS settings:

- A **Cd-Rom** reader will usually appear as** CD/DVD** or **CD-ROM**
- A **Hard disk** will usually appear as **HDD** or **HARD DRIVE** or 
**HARD DISK** or even **IDE~**...
- A **USB** peripheral will usually appear as **USB HDD**, **USB DRIVE** 
or **USB DEVICE** or **Removable Device**.

#### BIOS/UEFI/Secure Boot configuration

If your machine uses a BIOS/UEFI equipped with the wonderful "Secure Boot", 
the handling is slightly different.

Each manufacturer has its own UEFI, and the following images and explanations 
might be different at your place.

**Disable the secure boot**

Using the mouse or the navigation arrow keys, select the "Security" or 
"Authentification" tab. Verify that the "Secure Boot" is "Disabled".

![SecureBoot : disabling 1 (sources linuxtrack.net)](img/secureboot1.png)

![SecureBoot : disabling 2 (sources linuxtrack.net)](img/secureboot2.png)  

**Change the boot order**

Now we must change the boot order so that the computer starts first from the 
USB (or the DVD). Click on the "Boot" tab and modify the order, if necessary, 
so that your medium becomes the first of the list as explained on the previous 
chapter (BIOS).

![SecureBoot: Save setting (sources linuxtrack.net)](img/bios-uefi-save.png)  

Now, you save your changes and you start the Debian installation.

## Testing Debian safely

The best way to make your choice: try the system directly on your computer!  
There are two possibilities: you can test a GNU/Linux distribution from a 
stand-alone "Live" session or directly from your windows® system thanks to 
virtualization software: VirtualBox.

### Testing Debian in Live session

Debian provides standalone "live" images for safe testing of an environment. 
Their peculiarity is to change nothing on the computer, everything happens in 
RAM and is forgotten once the computer is turned off. These images sometimes 
have a setup launcher on the desktop that allows you, once tested, to install 
Debian directly from your session.  
To get a live image, visit this page : 
[https://www.debian.org/CD/live/index.html](https://www.debian.org/CD/live/index.html).

![noob](img/noob-modarp-circle-icon.png)  
**The principle of the Live CD** is the capability to use/test a distribution on 
a computer without any risks for your personal data. The Live also let you check 
the distribution compatibility with your hardware.  
The Debian software is "compressed" within a special file (the 'squashfs.filesystem' 
file) and embedded in the downloaded ISO image. This same special file is 
"uncompressed" during the Live utilization, and will later be copied on your 
hard disk during the installation process.

At launch, the menu is different from the classic Debian ISOs since the "Live" 
entry awaits you:

![Launching Debian Live session](img/deb9-debianlive.png)  

![](img/deb9-debianlive-install-launcher.png)  
Whatever version you choose (Gnome, KDE, Xfce ...), you will find on the desktop 
and / or in the "System" menu an entry to install directly Debian. Thus, after 
having tested the compatibility of your machine, you can directly install the 
chosen environment from the "live" session.

### Testing Debian from Windows® with VirtualBox

![vbox](img/vbox_logo.png)  
**Virtualbox** ([http://www.virtualbox.org/](http://www.virtualbox.org/)) is a 
virtualization software available for major operating systems. This software 
will allow you to test your Debian or any other GNU/Linux OS directly on your 
Windows® desktop. Virtualbox is not the only virtualization software: more 
information about virtualization on Wikipédia 
([https://en.wikipedia.org/wiki/Virtualization#Nested_virtualization](https://en.wikipedia.org/wiki/Virtualization#Nested_virtualization)).

Internet is full of tutorials about installing VirtualBox. I leave you to 
consult these few links which will allow you to take charge quickly this software:

- WikiHow documentation: 
[http://www.wikihow.com/Install-VirtualBox](http://www.wikihow.com/Install-VirtualBox) (cc-by-nc-sa)
- The official VirtualBox documentation in PDF: 
[http://download.virtualbox.org/virtualbox/5.1.18/UserManual.pdf](http://download.virtualbox.org/virtualbox/5.1.18/UserManual.pdf)

![noob](img/noob-modarp-circle-icon.png)  
**... We enter the virtual world here??**  
Kind'of'. The virtualization software makes it possible to test or install 
**a system in a system**, and to ensure the independence of the two installations. 
VirtualBox will "make believe" the system you want to test (referred to as "*guest system*") 
that it is all alone on its computer, while ensuring exchanges (shared folder, network 
connection sharing, etc.) With the *host system*.  
Your guest system will be installed on a "virtual disk", which will simply be like a 
big archive in a folder of your documents.  
Note that this test method does not verify hardware compatibility and is only for 
you to discover a distribution before installing it on your hard drive.

## Installation simple boot

Here comes the long-awaited moment, the actual installation of the Debian 
GNU/Linux system on your machine …  Are you ready to begin the exciting journey?

Hereafter a visual step by step explanation on how to easily install Debian, 
using the graphical installer. **This method erases the whole disk** and 
installs Debian as the unique operating system on your machine. The ISO 
image is of the type "netinst".  
The Debian installer displays an explanation at every step of the process: with 
GNU/Linux ![gnulinux](img/gnulinux-mini.png), there is no advertising, so please 
take few seconds of your time to read the little messages which talk about your 
future system

### Starting the installation

When the installer begins, we select the installation mode:

![installation mode (BIOS)](img/debian-9-install-01.png)

If your motherboard boots under UEFI, the display might be slightly different:

![installation mode (UEFI)](img/debian-9-install-01-efi.png)

More information on UEFI in the Debian wiki 
([https://wiki.debian.org/UEFI](https://wiki.debian.org/UEFI)). 

During the start-up, the installer proposes a choice of several installation modes:

- The **Install** mode proposes a text in gray color on a blue background, and 
cursor move possible only by using the keyboard arrows, the TAB key and the 
Space bar to tick options on/off.
- The **Graphical Install** mode offers a prettier interface, enabling the mouse usage.

Each installer pane displays a clear explanation message. When you don't know the 
answer, take the default setting. However, **pay great attention during the 
partitioning phase**, since this operation could erase the existing data on your hard disk(s).

### Select your language

Selection of the system language by default: this choice will also modify the installer 
language itself, which then will display its own messages in English (if you chose "English").

![installation: language](img/deb9-isntall-en-02.png)

### Select your geographical location

This information allows the system to automatically set the date and time of your system, 
using a remote time server.

![installation: country](img/deb9-isntall-en-03.png)

### Configure your keyboard

![installation: keyboard](img/deb9-isntall-en-04.png)

### System name setting

![installation: hostname](img/deb9-isntall-en-05.png)

### Domain name setting (if necessary)

Leave this field empty if you don't know the answer.

![installation: domain](img/deb9-isntall-en-06.png)

### Root account creation

Enter the administrator password (twice to confirm). Like indicated in the information 
message, creating a "root" administrator account is not mandatory. If you leave the 
fields empty, the first user will receive all the rights to perform administrative 
tasks with the "sudo" command and your password.  
More information about passwords on dedicated section (chap.10.1.3).

![installation: admin password](img/deb9-isntall-en-07.png)

### First user account creation

Start by entering the full name of the main user:

![installation: user full name](img/deb9-isntall-en-08.png)

Then enter its **identifier**, that is the pseudo which will be used during the 
connection (login) to a session:

![installation: user login](img/deb9-isntall-en-09.png)

### First user password setting

Like for the administrator account, you need to enter the password twice for confirmation.

![installation: user password](img/deb9-isntall-en-10.png)

### Selecting the guided or manual partitioning scheme

![noob](img/noob-modarp-circle-icon.png)  
**... "Partitioning" ... What's that?**  
The partitioning process consists in organizing different partitions on your system.
Partitions are non-overlapping area defined on your hard disk(s), each one of 
them having its specific properties (file system type, unique identifier, size, 
etc.). In order to function correctly, Debian GNU/Linux needs at least one partition 
to install the operating system. An additional exchange partition (called SWAP) is 
highly recommended, to deal not only with insufficient memory space, but also with 
the suspend or hibernation functionality.  
The guided partitioning scheme proposes 2 partitions (system + swap), but you can 
opt for a scheme proposing a separated "/home" partition, and use another hard 
disk to store your personal data (as an example).

![installation: partition mode](img/deb9-isntall-en-11.png)

### Selecting the disk to partition

![installation: disk to partition](img/deb9-isntall-en-12.png)

### Patitioning scheme selection

![installation: partition scheme](img/deb9-isntall-en-13.png)

### Modify or finish the partitioning

![installation: check partition](img/deb9-isntall-en-14.png)

### Partitioning overview and formating process launch

![warning](img/warning.png)  
**Warning: this is the step during which the installer will format the partitions.**  
**The data on the selected disk will be erased!**

![installation: formating and install](img/deb9-isntall-en-15.png)

The base system is installed. You will choose your main interface later.

![installation](img/deb9-isntall-en-16.png)

### Analysis of the CD/DVD complementary contents (if necessary)

Optional: Had you opted for a Debian CD set including all the packages needed for 
the installation, you would now insert the second CD (and tick the "Yes" option).

![installation: optional additional CD](img/deb9-isntall-en-17.png)

### Selecting the network mirror country

![noob](img/noob-modarp-circle-icon.png)  
**... where should I put this mirror?**  
**"A repository mirror"** is a computer server, available on-line, hosting the 
full set of the Debian packages. If you don't have all the CDs on hand, Debian 
will grab the software or the packages you need on the network mirror servers.  
There are repository servers all around the world, each one being the exact 
copy (mirror) of the others. In this example we select "Yes" and in the following 
section we will pick an Internet mirror located in the United Kingdom.

![installation: mirror country](img/deb9-isntall-en-18.png)

### Selecting the server hosting the archive mirror

The server proposed by default is usually a good choice.

![installation: mirror server](img/deb9-isntall-en-19.png)

### Configuring a proxy server (if necessary)

Leave this field empty, if you don't know.

![installation : configuration du proxy](img/deb9-isntall-en-20.png)

### Participating - or not - to the Debian popularity-contest

![installation: stats Debian](img/deb9-isntall-en-21.png)

This can be useful to the community, but it is not mandatory.

### Selecting software

![installation: softwares and main desktop](img/deb9-isntall-en-22.png)

Now, it is time to select your main desktop interface, as well as the main 
services to be installed by default. Note that you can select several desktops 
in view of directly installing several environments.
Hereafter a summary description of the proposed choices:

- **Debian desktop environment** let you install, or not, a desktop. The desktop 
environment installation can be done later on, or never, and it is not indispensable, 
for example on a server. But if you are a beginner, a graphical interface will be 
easier to master.
- **web server** includes a preselection of packages dedicated to Web servers.
- **print server** includes a preselection of packages dedicated to Print servers.
- **SSH server** allows you to activate the ssh protocol, and computer remote control. 
**Warning!** Enabling the SSH service might create a security breach if it is not 
correctly configured. For **experienced users** only.
- **standard system utilities** includes a collection of applications to manage 
your operating system.

### Packages installation

Time to take a little pause. Downloading and installing the packages may take 
quite some time, depending on the power of your computer and the Internet 
connection bandwidth.

![installation: packages](img/deb9-isntall-en-23.png)

### Installing Grub, the boot loader

![installation: GRUB](img/deb9-isntall-en-24.png)

### Selecting the Grub location

When the system installation is of type "simplified single-boot", you should 
install Grub on your machine **main disk**, usually identified under the 
nice "/dev/sda" name.

![installation: disk for GRUB](img/deb9-isntall-en-25.png)

### Installation complete

![installation: end and reboot](img/deb9-isntall-en-26.png)

Don't forget to remove the  CD or USB key to avoid re-launching the installer 
during the next boot.  
Then complete the installation process by pressing the *Continue* button.

### Debian first start-up

Grub boot loader selection menu:

![installation: reboot on GRUB](img/deb9-grub.png)

Gnome login in:

![Debian: login screen](img/deb9-gdm.png)

Interface by default on the Debian Gnome-Shell desktop:

![launching Gnome-Shell](img/deb9-gnome-shell.png)

## On-line installation details

Debian offers more possibilities than just the two methods explained in the 
previous chapters.  
But this manual being intended for the beginners, the more complex 
installation modes are detailed in the official Debian documentation: 
[https://www.debian.org/releases/stable/installmanual](https://www.debian.org/releases/stable/installmanual)

Just to give you a flavor of the other installation modes:

### Dual-boot installation

The **Dual-boot** mechanism let you install side-by-side two independent 
operating systems, and doing so, allows you to select, at boot time, 
which one you want to launch.

![warning](img/warning.png)  
**This is not the recommended method**: there are inherent risks for 
your data during the partition resizing. If you choose this solution to 
get started on Debian, please remember that you can very well test it first 
by using a **Live session** (chap.5.3.1) with no risks for your data.

More information of the Debian Dual-Boot Wiki: 
[https://wiki.debian.org/DualBoot](https://wiki.debian.org/DualBoot)

### LVM ... what's that?

**LVM** is an acronym for **L**ogical **V**olume **M**anager.

Is usage consists in:

- Create a specific partition of type "LVM" (which corresponds to a 
mounted **disk**)
- Within this partition we can create "Logical Volumes" (corresponding 
to **partitions**)  that we can modify at will, themselves sized and 
formated to our own convenience.

So, the "Logical Volumes" utilization, replaces straight out the usual disk 
"partitioning", and provide a more flexible way to manage disk space by allowing 
modifications on them at any point of time.

It is possible, as well, to keep one part of the LVM partition without installed 
"Logical Volume" pending further decisions.

More details on the dedicated Debian LVM Wiki: 
[https://wiki.debian.org/LVM](https://wiki.debian.org/LVM)

### Encryption ... what's for?

The confidentiality of personal data is is a pretty hot topic these days. In 
order to better protect your data, you can opt for an **encrypted installation**; 
With this kind of installation, even in the case of theft of your computer, or 
the removal of its hard disk, no data access is possible with the password !

The Debain Administrator's Handbook provide us with more details 
([https://debian-handbook.info/browse/en-US/stable/sect.installation-steps.html#sect.install-partman](https://debian-handbook.info/browse/en-US/stable/sect.installation-steps.html#sect.install-partman)):

> This feature can be added underneath any filesystem, since, as for **LVM**, Linux (and more particularly the "dm-crypt" driver) uses the Device Mapper to create a virtual partition (whose content is protected) based on an underlying partition that will store the data in an encrypted form (thanks to **LUKS**, **L**inux **U**nified **K**ey **S**etup, a standard format that enables the storage of encrypted data as well as meta-information that indicates the encryption algorithms used).

To put it simply, LUKS creates a container to host an encrypted volume protected by a password.

For more information and a complete guide, visit the digital self-defense guide: 
[https://ssd.eff.org/en](https://ssd.eff.org/en)

### RAID ... does it hurt?

[Wikipédia](https://en.wikipedia.org/wiki/RAID) is my friend ...

> **RAID** is the acronym of redundant *array of independent disks*.
> 
> **RAID** is a data storage virtualization technology that combines multiple physical disk drive components into a single logical unit for the purposes of data redundancy, performance improvement, or both.

Please note that the Debian installation in RAID mode, uses the RAID software (thanks to the *mdadm* tool) and not the RAID hardware (handled by a physical RAID controller).

For more details and installation tutorial see the official Debian wiki 
[https://wiki.debian.org/SoftwareRAID](https://wiki.debian.org/SoftwareRAID) 
or the dedicated section of the Debian Administrator's HandBook 
[https://debian-handbook.info/browse/stable/advanced-administration.html#sect.raid-soft](https://debian-handbook.info/browse/stable/advanced-administration.html#sect.raid-soft)

