![logo cahiers](img/logo_cahiers_big.png)

![debian text mini](img/logo_debian_txt_mini.png) *sin dolores de cabeza*

![deb9](img/debian_stretch.png)

[https://lescahiersdudebutant.fr/index-es.html](https://lescahiersdudebutant.fr/index-es.html)

**-- Acerca de este manual --**

**"El manual del principiante"** es un manual simplificado para instalar y tomar el 
control del sistema Debian.

En las siguientes páginas encontrará **las respuestas a sus primeras preguntas** 
referentes al sistema **Debian GNU/Linux** ![](img/gnulinux-mini.png), su historia, 
cómo obtenerlo, instalarlo, dominarlo, configurarlo y administrarlo.

Podrá **ir más allá** y obtener información sobre la protección de la privacidad, 
la copia de seguridad de sus datos y los diferentes actores del mundo del Software 
Libre.

Por lo general, los manuales comienzan por enseñarle las bases teóricas y el uso del 
terminal. Este manual se ocupa de la parte del "entorno gráfico": está diseñado 
para permitirle empezar rápidamente con Debian, la pantalla encendida, los dedos en 
el teclado y el ratón cerca de ![](img/big_smile.png).

**-- La misión de este manual no es ser exhaustivo. --**

Hay muchos enlaces externos disponibles en este manual. No dude en hacer clic en 
ellos para leer información más detallada.

Para una documentación más detallada, por favor visite la Wiki oficial de Debian: 
[https://wiki.debian.org/FrontPage](https://wiki.debian.org/FrontPage)

Si necesita un manual completo de Debian, por favor lea el **Manual del Administrador de Debian** 
de Raphaël Hertzog y Roland Mas 
[https://debian-handbook.info/browse/stable/](https://debian-handbook.info/browse/stable/).

**-- ¿Cómo utilizar este manual?--**

Depende de la versión que esté viendo:

- la [versión HTML](https://dflinux.frama.io/thebeginnershandbook/) tiene un campo de 
búsqueda eficaz, no dude en introducir una o dos palabras,
- la [versión en PDF](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.pdf) 
incluye un resumen *más que* detallado y una tabla de figuras al final de la guía. 
Esta versión está incluida en el [paquete debian](https://framagit.org/dflinux/thebeginnershandbook/tree/master/docs/paquet_debian/),
- la [versión de archivo HTML](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.html.tar.gz) 
se abre en una sola página, facilitando la búsqueda desde el navegador utilizando el acceso directo [Ctrl]+'f'
- la [versión ePub](https://framagit.org/dflinux/lescahiersdudebutant/raw/master/docs/les_cahiers_du_debutant.epub) 
aprovecha la detallada tabla de contenidos y las funciones de búsqueda de su lector de libros electrónicos.

---

El manual del principiante de Debian:  
[https://lescahiersdudebutant.fr/index-es.html](https://lescahiersdudebutant.fr/index-es.html)

![cahiers minibanner](img/minibanner-cahiers.png)

