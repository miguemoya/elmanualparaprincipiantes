
---

**-- Acerca de este manual --**

**"El manual del principiante"** es un manual simplificado para instalar y tomar el 
control del sistema Debian.

En las siguientes páginas encontrará **las respuestas a sus primeras preguntas** 
referentes al sistema **Debian GNU/Linux** ![](img/gnulinux-mini.png), su historia, 
cómo obtenerlo, instalarlo, dominarlo, configurarlo y administrarlo.

Podrá **ir más allá** y obtener información sobre la protección de la privacidad, 
la copia de seguridad de sus datos y los diferentes actores del mundo del Software 
Libre.

Por lo general, los manuales comienzan por enseñarle las bases teóricas y el uso del 
terminal. Este manual se ocupa de la parte del "entorno gráfico": está diseñado 
para permitirle empezar rápidamente con Debian, la pantalla encendida, los dedos en 
el teclado y el ratón cerca de ![](img/big_smile.png).

---

**-- La misión de este manual no es ser exhaustivo. --**

Hay muchos enlaces externos disponibles en este manual. No dude en hacer clic en 
ellos para leer información más detallada.

Para una documentación más detallada, por favor visite la Wiki oficial de Debian: 
[https://wiki.debian.org/FrontPage](https://wiki.debian.org/FrontPage)

Si necesita un manual completo de Debian, por favor lea el **Manual del Administrador de Debian** 
de Raphaël Hertzog y Roland Mas 
[https://debian-handbook.info/browse/stable/](https://debian-handbook.info/browse/stable/).


---

**-- ¿Cómo utilizar este manual?--**


Esta versión en PDF incluye un resumen detallado y una tabla de figuras al final de la guía.  
Nota: Este manual incluye algunas órdenes o bloques de código que a veces son más largos que 
el ancho de la página. En este caso, se añade una barra invertida `\`  y el resto de la 
orden o código se transfiere a la siguiente línea.

---

![cahiers minibanner](img/minibanner-cahiers.png) El manual del principiante de Debian: 
[https://lescahiersdudebutant.fr/index-es.html](https://lescahiersdudebutant.fr/index-es.html)



