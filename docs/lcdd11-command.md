# ![console](img/term.png) Basic Command Memo

![a shell to rule them all](img/lotr.png)

Debian GNU/Linux systems have all the graphical applications needed to perform 
your daily tasks, so why use the command line?

- it's faster,
- not all options are present in the graphical interfaces,
- use the command line without GUI saves resources,
- it makes learning the Debian GNU/Linux system easier.

This section gathers some basic commands. For a more complete list, visit 
the Debian documentation: 
[https://wiki.debian.org/ShellCommands](https://wiki.debian.org/ShellCommands).

> *The ultimate command:* **man**

### Browse directories

    command    action
    ----------------------------------------------------------------------
    pwd        Print Working Directory
    cd rep     Change Directory to rep
    cd         Change Directory to /home/$USER or ~/
    cd ..      Browse the parent directory
    ls rep     List information about file(s) in rep
    ls -a      ls with hidden files displayed
    ls -l      ls with size and rights

### Action on files or directories

    command                action
    ----------------------------------------------------------------------
    mv source target       move the file from source to target
    cp source target       copy the file from source to target
    cp -R source target    copy the directory source to target
    ln source link         create a hard link from source to link
    ln -s source link      create a symbolik link from source to link
    touch file             create the file file or update its 
                           modification date
    mkdir rep              create a repertory rep
    mkdir -p rep/rep2      mkdir with creation of parent directory if needed
    rm file                remove file
    rm -f file             remove the write-protected file
    rmdir rep              remove an empty directory
    rm -R rep              remove a directory
    du -h file or rep      display size of the file or the rep

### View/Compare files

    command               action
    ----------------------------------------------------------------------
    wc file               Print byte, word and line counts of file
    cat file              display the contents of a file
    more file             displays file page by page. 'Space'=next page, 
                          'Enter'=next line, 'u'=up
    less file             displays file with fine navigation 
                          Left/Right/Up/Down/PageUp/PageDown
    head -n x file        displays 'x' first lines of file
    tail -n x file        displays 'x' last lines of file
    tail -f file          dynamicaly display last line of file
    diff file1 file2      Displays differences between two text files
    diff -u file1 file2   Displays differences between file1 and file2 
                          with patch syntax
    comp file1 file2      compares two binary files
    comp file1 file2 n N  compare two files, file1 from the octet n and 
                          file2 from octet N

### Users and goups

    command action
    ----------------------------------------------------------------------
    whoami          Print the current user id and name
    who             Print all usernames currently logged in
    id              Print user and group id's (uid & gid)
    id user         Print user and group id's (root only)
    finger user     Print informations about user
    write user      Print a message on user's terminal
    tty             Print the current terminal's name
    su - sudo       Switch to administrator mode, superuser
    passwd          Change the password of the current user
    adduser         add a user
    deluser         delete a user
    addgroup        add a group
    delgroup        delete a group

### Process

    command             action
    ----------------------------------------------------------------------
    ps                  Process Status. Lists running process
    ps ax               Print all running processes
    ps aux              Print all process identified by users
    pstree              Print all process in a tree
    top                 List processes running on the system in a 
                        semi-graphical table
    kill signal pid     kill a process using its pid
    pkill signal name   kill a process using its name

**signaux** utilisés par *kill/pkill*

    signal   mode      action
    ----------------------------------------------------------------------
    -1       (HUP)     Reload the process configuration file
    -2       (INT)     Interrupt the process
    -3       (QUIT)    Quit the process
    -9       (KILL)    Kill the process (to avoid, try '-15' first)
    -15      (TERM)    Complete the process properly
    -18      (STOP)    Freeze the process
    -20      (CONT)    Resume execution of a frozen process

### Hardware

    command                   action
    ----------------------------------------------------------------------
    lsusb                     Lists connected USB devices
    lspci                     Lists connected PCI devices
    cat /proc/cpuinfo         Displays processor information
    cat /proc/partitions      Displays mounted partitions

**examples :** (sources linuxtrack) 

Display the model of the graphics card:

    lspci | egrep "3D|Display|VGA"

Show the model of the Wifi card:

    lspci | grep -i "net" | cut -d: -f3

Show the soundcard model:

    lspci | grep -i audio | cut -d: -f3

### Network

    command                      action
    ----------------------------------------------------------------------
    hostname                     Print or set system name
    ping machine                 send a ping to a machine on the network
    traceroute machine           displays a traceroute through machine
    netstat                      Displays the use of the network 
                                 by the processes
    netstat -a                   netstat with the display of the 
                                 server processes
    lsof                         Detailed list of file and network usage
    ifconfig                     Displays the config of the interfaces
    ifconfig interface IP mask   configure the network  interface
    route                        Displays the routing table

ex : displays its locap IP on eth0

    ip address show eth0 | grep "inet " | tr -s " " ":" | cut -d: -f3
    /sbin/ifconfig eth0 | grep "inet " | tr -s " " ":" | cut -d: -f4
    ip address show eth0 | grep "inet " | tr -s " " ":" | tr -s "/" ":" | cut -d: -f3

### Search

    command/option       action
    ----------------------------------------------------------------------
    locate pattern       search for file with a pattern name
    updatedb             update locate database
    find path options    search for file corresponding to options in path
    find -name pattern   search for file with a pattern
    find -type f/d/l     search by filetype: f=file, d=directory, l=link
    find -exec cmd       execute *cmd* to found files

**Example**: search for all *png* files in the 'Images' directory, then copy 
all files to *tmp* directory ( '{}' stands for found files).

    find $HOME/Images -name "*.png" -exec cp {} $HOME/tmp/ \;

### Archives

    format           compress                              extract
    ----------------------------------------------------------------------
    .tar.bz2, .tbz2  tar -cvjf archive.tar.bz2 directory   tar xvjf
    .tar.gz, .tgz    tar -cvzf archive.tar.gz directory    tar xvzf
    .bz2             bzip2 file                            bunzip2
    .rar             -                                     unrar x
    .gz              gzip file                             gunzip
    .tar             tar -cvf archive.tar files            tar xvf
    .zip             zip -r archive.zip files              unzip
    .Z               compress files                        uncompress
    .7z              7z a files                            7z x
    .xz              xz -z directory                       unxz

### Kernel

Version of the Linux kernel used, its name, the version of the compiler used:

    cat /proc/version

Version of the Linux kernel used:

    uname -r

List all kernels installed on your machine:

    dpkg -l | egrep "linux-(header|image)"

### Links and references

- Dedicated page on Debian-Facile (fr): 
[https://debian-facile.org/doc:systeme:commandes:gnu_linux](https://debian-facile.org/doc:systeme:commandes:gnu_linux)
- Dedicated Debian documentation: 
[https://www.debian.org/doc/manuals/debian-reference/ch01.html](https://www.debian.org/doc/manuals/debian-reference/ch01.html)
- Commands list: 
[http://www.epons.org/commandes-base-linux.php](http://www.epons.org/commandes-base-linux.php)
- Commands list on Debian wiki: 
[https://wiki.debian.org/ShellCommands](https://wiki.debian.org/ShellCommands)

