# ![libre](img/tux.png) Free Softwares

You can find hereafter a list of the sites providing more detailed information 
concerning the Free Culture (in general) and the Free Software (in particular).

## The web sites

### Free Software Foundation

[https://www.fsf.org/](https://www.fsf.org/)  
The Free Software Foundation (FSF) is a nonprofit with a worldwide mission to 
promote computer user freedom. We defend the rights of all software users.  
Free software developers guarantee everyone equal rights to their programs; 
any user can study the source code, modify it, and share the program. By 
contrast, most software carries fine print that denies users these basic 
rights, leaving them susceptible to the whims of its owners and vulnerable 
to surveillance.

### The GNU project

[https://www.gnu.org/](https://www.gnu.org/)  
Philosophy of the GNU Project: Free software means that the software's users 
have freedom. (The issue is not about price.) We developed the GNU operating 
system so that users can have freedom in their computing.  
Specifically, free software means users have the four essential freedoms: 
(0) to run the program, (1) to study and change the program in source code 
form, (2) to redistribute exact copies, and (3) to distribute modified versions.  
Software differs from material objects—such as chairs, sandwiches, and 
gasoline—in that it can be copied and changed much more easily. These 
facilities are why software is useful; we believe a program's users should 
be free to take advantage of them, not solely its developer.

### The Linux Foundation

[https://www.linuxfoundation.org/](https://www.linuxfoundation.org/)  
The Linux Foundation partners with the world's leading developers and 
companies to solve the hardest technology problems and accelerate open 
technology development and commercial adoption.  
Founded in 2000, The Linux Foundation today provides tools, training, 
and events to scale any open source project, which together deliver 
an economic impact not achievable by any one company.

## The Wikimedia Foundation

![](img/logo_wm-foundation.png)  
You all know **Wikipedia** the free on-line encyclopedia.  
But actually Wikipedia is part of a large family, under the benevolent 
umbrella of the [Wikimedia Foundation](http://www.wikimedia.org/)

The **Wikimedia Foundation** is a global movement whose mission is to 
bring free educational content to the world. Quoting the foundation home page :  
"*Imagine a world in which every single human being can freely share in the 
sum of all knowledge. That's our commitment.*". 
[https://wikimediafoundation.org/wiki/Home](https://wikimediafoundation.org/wiki/Home)

### Wikipédia

![](img/logo_wikipedia.png)  
**wikipedia, the free encyclopedia** 
[https://wikipedia.org/](https://wikipedia.org/)  
Wikipedia is an universal, multilingual encyclopedia, written and maintained 
by volunteers on the Internet, and functioning under the Wiki principle.

### Wikimedia

![](img/logo_wm-commons.png)  
**wikimedia commons, the multimedia library** 
[https://commons.wikimedia.org/wiki/Main_Page](https://commons.wikimedia.org/wiki/Main_Page)  
Wikimedia Commons, which is often refereed to as *Commons*, is a media file 
repository making available public domain and freely-licensed educational 
media content (images, sound and video clips) to everyone, in their own language.

### Wiktionary

![](img/logo_wiktionary.png)  
**wikitionnary, the dictionary** 
[https://en.wiktionary.org/](https://en.wiktionary.org/)  
Wikitionary was initially designed as the lexical companion to Wikipedia, the 
encyclopedia project, Wiktionary has grown beyond a standard dictionary and now 
includes a thesaurus, a rhyme guide, phrase books, language statistics and 
extensive appendices. We aim to include not only the definition of a word, but 
also enough information to really understand it. Thus etymologies, pronunciations, 
sample quotations, synonyms, antonyms and translations are included.

### Wikiquote

![](img/logo_wikiquote.png)  
**wikiquote, the free quote compendium** 
[https://en.wikiquote.org/wiki/Main_Page](https://en.wikiquote.org/wiki/Main_Page)  
The Wikiquote collaborative project, is a free online compendium of sourced 
quotations from notable people and creative works in every language, translations 
of non-English quotes, and links to Wikipedia for further information.

### Wikisource

![](img/logo_wikisource.png)  
**wikisource, the free library** 
[https://en.wikisource.org/wiki/Main_Page](https://en.wikisource.org/wiki/Main_Page)  
Wikisource is a project to create a growing free content library of source 
texts, as well as translations of source texts in any language. This work 
is done by voluntary contributors.  
Some things Wikisource includes are: Source texts previously published by any 
author, Translations of original texts, Historical documents of national or 
international interest, Bibliographies of authors whose works are in Wikisource.  
Some basic criteria for texts excluded from Wikisource are: Copyright 
infringements, Original writings by a contributor to the project, Mathematical 
data, formulae, and tables, Source code (for computers), Statistical source 
data (such as election results).  
Unless otherwise noted, all user contributions to Wikisource are released under 
the Creative Commons Attribution/Share-Alike License (CC-BY-SA) (replacing the 
previously used GNU Free Documentation License).

### Wikibooks

![](img/logo_wikibooks.png)  
**Wikibooks, the open-content textbooks collection** 
[https://fr.wikibooks.org/wiki/Accueil](https://fr.wikibooks.org/wiki/Accueil)  
Wikibooks is a collection of open-content textbooks. Wikibooks is for textbooks, 
annotated texts, instructional guides, and manuals. These materials can be used 
in a traditional classroom, an accredited or respected institution, a 
home-school environment, as part of a Wikiversity course, or for self-learning.

### Wikijuniors

![](img/logo_wikijunior.png)  
**wikijuniors, the free textbooks for children** 
[https://en.wikibooks.org/wiki/Wikijunior|Wikijunior](https://en.wikibooks.org/wiki/Wikijunior|Wikijunior)  
Wikijunior produces non-fiction books for children from birth to age twelve. 
These books could take the form of macropedias, textbooks, or primers. These 
Wikijunior books are written with children in mind. The subjects of these books 
are chosen because they are appealing to kids. These books are richly 
illustrated with photographs, diagrams, sketches, and original drawings.

### Vikidia

![](img/logo_vikidia.png)  
**vikidia, the free encyclopedia for children** 
[https://en.vikidia.org/wiki/Main_Page](https://en.vikidia.org/wiki/Main_Page)  
Vikidia is an English encyclopedic project, based on wiki technology, 
for 8-13 year-old readers and contributors, but not only.  
The website is independent from Wikimedia Foundation which owns Wikipedia. 
It was launched in November 2006 in French and in 2013 in English. Vikidia 
is handled by the Association Vikidia, a non-profit-organization located in France.

### Wikiversity

![](img/logo_wikiversity.png)  
**Wikiversity, the open learning community** 
[https://www.wikiversity.org/](https://www.wikiversity.org/)  
Wikiversity is a Wikimedia Foundation project devoted to learning resources, 
learning projects, and research for use in all levels, types, and styles of 
education from pre-school to university, including professional training 
and informal learning.  
Teachers, students, and researchers are invited to join Wikiversity in 
creating open educational resources and collaborative learning communities.

### Wikispecies

![](img/logo_wikispecies.png)  
**wikispecies, the free species directory** 
[https://species.wikimedia.org/wiki/Main_Page](https://species.wikimedia.org/wiki/Main_Page)  
Wikispecies is a wiki-based on-line project supported by the Wikimedia Foundation. 
Its aim is to create a comprehensive free content catalog of all species; the 
project is directed at scientists, rather than at the general public. Wikispecies 
is available under the GNU Free Documentation License and CC BY-SA 3.0.

### Wikivoyage

![](img/logo_wikivoyage.png)  
**wikivoyage, the free travel guide** 
[https://www.wikivoyage.org/](https://www.wikivoyage.org/)  
Wikivoyage is a free web-based travel guide for travel destinations and travel 
topics written by volunteer authors ("Wiki-travelers" from all around the world). 
It is a sister project of Wikipedia and supported and hosted by the same 
non-profit Wikimedia Foundation. Wikivoyage has been called the "Wikipedia 
of travel guides".

### Wikinews

![](img/logo_wikinews.png)  
**wikinews, the free news source** 
[https://en.wikinews.org/wiki/Main_Page](https://en.wikinews.org/wiki/Main_Page)  
Wikinews is a free-content news source Wiki and a project of the Wikimedia 
Foundation. The site works through collaborative journalism. Wikipedia co-founder 
Jimmy Wales has distinguished Wikinews from Wikipedia by saying "on Wikinews, 
each story is to be written as a news story as opposed to an encyclopedia article." 
The neutral point of view policy espoused in Wikinews distinguishes it from other 
citizen journalism efforts. In contrast to most projects of the Wikimedia 
Foundation, Wikinews allows original work under the form of original reporting 
and interviews.

### Wikidata

![](img/logo_wikidata.png)  
**wikidata, the free and open knowledge base** 
[https://www.wikidata.org/wiki/Wikidata:Main_Page](https://www.wikidata.org/wiki/Wikidata:Main_Page)  
Wikidata is a collaboratively edited knowledge base operated by the Wikimedia 
Foundation. It is intended to provide a common source of data which can be used 
by Wikimedia projects such as Wikipedia, and by anyone else, under a public domain 
license. This is similar to the way Wikimedia Commons provides storage for media 
files and access to those files for all Wikimedia projects, and which are also 
freely available for reuse. Wikidata is powered by the software Wikibase.  
This project was launched by Wikimedia Deutschland. It was presented during the 
Semantic Tech & Business Conference in February 2012, and the site was open to the 
first contributions on 30 October 2012.

### Wiki Meta-wiki

![](img/logo_wm-meta-wiki.png)  
**wikimedia Meta-wiki, the global community site for the Wikimedia projects** 
[https://meta.wikimedia.org/wiki/Main_Page](https://meta.wikimedia.org/wiki/Main_Page)  
Meta (or Wikimedia's Meta-Wiki) is a wiki-based web site that is auxiliary for 
coordination of all the Wikimedia Foundation projects.  
Meta currently serves as one of the major avenues of discussion for Wikimedians 
including Wikipedians, the others being the mailing lists, the IRC channels, 
and the talk pages of individual articles and users. Meta is an independent and 
autonomous project from the English language Wikipedia and thus has its own 
policies and customs, which often differ from those here.  
Originally focused on the English language version of Wikipedia, Meta has, 
since its upgrade to Wikipedia's custom MediaWiki software, become a 
multilingual discussion forum used by all Wikimedia language communities.

### Wiki-incubator

![](img/logo_wm-incubator.png)  
**wikimedia incubator, where possible new languages for existing projects are tested** 
[https://incubator.wikimedia.org/wiki/Incubator:Main_Page](https://incubator.wikimedia.org/wiki/Incubator:Main_Page)  
The Wikimedia Incubator founded on 2 June 2006, is a wiki-based website hosted by 
the Wikimedia Foundation. It serves as a platform where anyone can build up a 
community in a certain language edition of a Wikimedia project (Wikipedia, Wiktionary, 
Wikibooks, Wikinews, Wikiquote and Wikivoyage) that does not yet have its own sub-domain, 
provided that it is a recognized language.  
This is where potential Wikimedia project Wikis in new language versions can be arranged, 
written, tested and proven worthy of being hosted by the Wikimedia Foundation.

### MediaWiki

![](img/logo_mediawiki.png)  
**mediawiki, a free software open source Wiki package, supporting all the Wikimedia projects** 
[https://www.mediawiki.org/wiki/MediaWiki](https://www.mediawiki.org/wiki/MediaWiki)  
MediaWiki is free server-based software, licensed under the GNU General Public License 
(GPL). It's designed to run on a large server farm for a website that gets millions of hits per day.

## The GNU/Linux distributions

### Debian

![](img/debian-90.png)  
Debian, the universal operating system, aka the "Mother" of numerous children distributions 
[https://www.debian.org/index.en.html](https://www.debian.org/index.en.html)  
By the way, Debian provides more than a pure OS: it comes with over 43000 packages, 
precompiled software bundled up in a nice format for easy installation on your machine.  
Debian is also a democratic community organization, which aims at the development of 
operating systems exclusively based on free software.

### Emmabuntüs

![](img/logo_emmabuntus.png)  
Emmabuntüs ([http://www.emmabuntus.org](http://www.emmabuntus.org)) a distro for all seasons.  
Emmabuntüs is a distribution designed to facilitate the refurbishing of computers given 
to humanitarian associations, in particular the Emmaüs community 
([http://www.emmaus-international.org/en/](http://www.emmaus-international.org/en/)) 
(hence, the name of the distribution), to help the discovery of the **GNU**/**Linux** world 
by beginners, and limit the the electronic wastes generated by the hardware overconsumption, 
by extending the lifespan of aging computers.

### Mageia

![](img/mageia_logo.png)  
**Mageia** ([https://www.mageia.org](https://www.mageia.org)) is a free operating system, 
based on GNU/Linux. It is a community project 
([https://www.mageia.org/en/community/](https://www.mageia.org/en/community/)), 
supported by an association law 1901 consisting of elected contributors.  
The goal of Mageia: to make superb tools for people. Beyond a secure, stable and 
sustainable operating system for your computers, the goal is to establish stable and 
credible governance to coordinate collaborative projects.  
Contributions are based on their own license 
([https://www.mageia.org/en/about/license/](https://www.mageia.org/en/about/license/)) 
and the community complies with a Code of Conduct 
([https://www.mageia.org/en/about/code-of-conduct](https://www.mageia.org/en/about/code-of-conduct/)).  
Mageia has an active community and detailed documentation. 
[https://www.mageia.org/en/support/](https://www.mageia.org/en/support/)

### Fedora

![](img/logo_fedora.png)  
**Fedora** ([https://getfedora.org/](https://getfedora.org/)) is a polished, easy 
to use operating system for laptop and desktop computers, with a complete set of 
tools for developers and makers of all kinds.  
Fedora comes with a "Server" release, a powerful, flexible operating system that 
includes the best and latest datacenter technologies. It puts you in control of 
all your infrastructure and services.

### Entirely free distributions

![](img/logo_gnu.png)  
[https://www.gnu.org/distros/free-distros.fr.html](https://www.gnu.org/distros/free-distros.fr.html)  
This page lists the GNU/Linux distributions that are entirely free as in freedom.  
The Free Software Foundation seems quite tough in what concerns the real freedom ... 
But can we put the blame on an organization which is seeking a situation that should 
be commonplace. 

## Free software alternatives to proprietary software

- **Equivalence between proprietary software:** A list of free alternatives proposing 
functionalities close to those of proprietary software, well established in their 
application domain. This list is far to be comprehensive, and does not have the goal 
to spotlight particular software, but rather to present different (proprietary or free) 
software within the same application domain. 
[http://wiki.linuxquestions.org/wiki/Linux_software_equivalent_to_Windows_software](http://wiki.linuxquestions.org/wiki/Linux_software_equivalent_to_Windows_software)

- **The Free Software Directory:** FSD, or simply Directory is a project of the 
Free Software Foundation (FSF). We catalog useful free software that runs under 
free operating systems — particularly the GNU operating system and its GNU/Linux 
variants. 
[https://directory.fsf.org/wiki/Main_Page](https://directory.fsf.org/wiki/Main_Page)

## Compatible hardware

**h.node:** This project aims at the construction of a hardware database in order 
to identify what devices work with a fully free operating system. The h-node.org 
website is structured like a Wiki in which all the users can modify or insert new 
contents. The h-node project is developed in collaboration and as an activity of 
the Free Software Foundation (FSF).  
[https://h-node.org/home/index/en](https://h-node.org/home/index/en)

