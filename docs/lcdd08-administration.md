# ![admin](img/admin.png) System administration

## The software sources

![software](img/software.png)  
Debian GNU/Linux uses the repository methodology to distribute applications. 
This methodology allows the software centralization and the usage of simple 
interfaces to administrate and upgrade your system: you have no need to visit 
the software sites themselves.

### The sources.list file

The Internet addresses of the Debian repositories are stored in the 
**/etc/apt/sources.list** and the files of the type 
**/etc/apt/sources.list.d/xxx.list**.

![An example of 'sources.list' file on Debian 9](img/deb9-sourceslist.png)

Details concerning the various information found in the 'sources.list' 
file (the lines beginning with a "#" are just comments):

- "deb": means a *binary* repository (the compiled software itself)
- "deb-src": means a *source* repository (the program code files used 
to compile the software)
- "http:·...": the Internet address of the repository server
- "stretch", "stretch/updates": the branch in the repository tree
- "main": the repository section.

![noob](img/noob-modarp-circle-icon.png)  
**... why "stretch" and not "stable" since the system is based on Debian Stable ??**  
"**stretch**"  is the precise version name of the installed system. It sets a 
given version of each packages included in the "stretch" repository (the 
version of the generic kernel, for example).  
"**stable**" is the generic name of the *currently* stable.  
For the time being, "stretch" is the "stable" version, thus you could used 
either designation. But when the Debian "stable" version becomes "buster", 
then "stretch" will change to "oldstable".  
Using the precise name of your version allows you to control **if and when** 
you want to upgrade your system to the next version, as opposed to some 
systems which want to impose their upgrades...  
For more detailed information, I invite you to visit the dedicated Debian wiki 
[https://www.debian.org/releases/index.html](https://www.debian.org/releases/index.html)

### About repositories, branches and sections/components

Debian organizes its software within repositories. These repositories are divided 
into branches and sections/components. To learn more about the "testing" and 
"unstable" branch read the chapter 8.8. One word, however, about the 
sections/components in the repositories.

There are 3 sections in the official Debian repositories:

| section | component selection criteria for the packages |
|---------|-----------------------------------------------|
|main|complies to the DFSG without any "non-free" dependency|
|contrib|complies to the DFSG with some "non-free" dependencies|
|non-free|does not comply to the DFSG|

**DFSG** (**D**ebian **F**ree **S**oftware **G**uidelines) : principes du 
logiciel libre selon Debian 
([https://www.debian.org/social_contract.html#guidelines](https://www.debian.org/social_contract.html#guidelines))

Only the packages within the **main** section/component are officially supported 
by the Debian project and are 100% free software. Rather, those proposed in 
*contrib* and *non-free* are partially or totally non-free.

Having said that, and depending on your type of hardware, it is very 
possible that some services do not function correctly without using 
specific (proprietary) drivers. In that case, you need to modify the 
/etc/apt/sources.list file (details in the following chapter)

- More details about the Debian versions in the Debian Wiki: 
[https://wiki.debian.org/DebianReleases](https://wiki.debian.org/DebianReleases).
- For more details on sources.list it's here: 
[https://wiki.debian.org/SourcesList](https://wiki.debian.org/SourcesList).
- For a complete documentation on the Debian package management, it's there: 
[https://www.debian.org/doc/manuals/debian-reference/ch02.html](https://www.debian.org/doc/manuals/debian-reference/ch02.html).

### Modifying the Repositories

Before you start modifying the software sources of your system, you must be 
conscious of the risks your are taking by using the "contrib" or "non-free" 
components of the archived branch.

- lack of freedom for this king of packages
- lack of support by the Debian project (you cannot maintain a piece of 
software without having the source code at your disposal)
- the contamination of your fully free Debian system.

Now, that you are warned that the non-free people kill the pink rabbits, 
let's move on:

To modify your software sources, it is enough to edit the 'sources.list' 
file. Open a terminal in terminal mode (chap.3.8.3), and enter:

    apt edit-sources

This command opens the appropriated file with the default text editor 
(nano or vim). Once you are done with your modifications, save the file 
("[Ctrl]+x" with nano, or ":wq" with vim [http://www.vim.org/](http://www.vim.org/)).

**Example of line entry for the free packages:**

    deb http://deb.debian.org/debian/ stable main

**Example of line entry for the free packages and the proprietary packages:**

    deb http://deb.debian.org/debian/ stable main contrib non-free

Now you can help yourself in the 3 package sections and install the 
non-free codecs and drivers.

Note also that you can modify your software sources by using the graphical 
Synaptic package manager (chap.8.3).

## APT in a terminal

![term](img/terminal.png)  
The following sections present the graphical interface of the **APT** 
(**A**dvanced **P**ackage **T**ool) program. This application is also available 
directly from the command line, allowing a better fine tuning of your system. 

This section presents the basic APT commands to manage the Debian packages 
from a terminal.

Debian supports also "aptitude", another package manager, with a different 
syntax and behavior. This manual being intended for beginners, no need to 
explicit these commands here: to learn more about them, visit the dedicated 
Debian Aptitude Wiki: 
[https://wiki.debian.org/Aptitude](https://wiki.debian.org/Aptitude).

### 'User' command to search and display information

These commands can be executed as simple user, because they do not impact 
your system.

|command|description|
|-------|-----------|
|apt show *toto*|display information about the package *toto*|
|apt search *toto*|look for packages corresponding to the *toto*|
|apt-cache policy *toto*|display the available version of *toto*|

### 'Administrator' mode commands for system maintenance

These commands must be executed with the "root" administrator rights, 
because they impact the system. To move into the administrator mode from 
a terminal, type "**su -**": the administrator password is requested.

|command|description|
|--------|-----------|
|apt update|Update the repositories metadata|
|apt install *toto*|Install the *toto* package and its dependencies|
|apt upgrade|Secured update of the installed packages|
|apt dist-upgrade|Update of the installed packet, by adding/removing |
||other packages if necessary|
|apt remove *xyz*|Remove the *xyz* package, but not the configuratrion files|
|apt-get autoremove|Auto remove the unecessary packages|
|apt purge *xyz*|Purge the *xyz* package and its configuration files|
|apt-get clean|Clean the local cache of the installed package|
|apt-get autoclean|Clean the local cache of the obsolete packages|
|apt-mark showmanual|Mark a package as being "manually-installed"|

For more detailed information and the apt/aptitude equivalence, visit the 
dedicated page of the Debian manual: 
[https://www.debian.org/doc/manuals/debian-reference/ch02.html](https://www.debian.org/doc/manuals/debian-reference/ch02.html)

**All-in-One** command line (in administrator mode) to update the repositories 
information + update your system + clean the packages in cache:

    apt update && apt dist-upgrade && apt-get autoclean

Which goes to show that managing your system with a terminal is not that complex.

**Apt vs Apt-get**

The Apt program is currently going through some streamlining and offers now a 
simplified syntax for its commands and options. Thus, you will find both syntax 
(*apt* and/or *apt-get*) in this manual as well as in most of the GNU/Linux 
documentations.

### Backport packages

Debian offers also some special repositories called **backports**, which contain 
**more recent versions** of some applications. These repositories are not 
activated by default, but do not present any particular risks for your system: 
**the "regular" repositories have the highest priority during the update process**, 
only the applications installed from the backports will look into these specific 
repositories.

![noob](img/noob-modarp-circle-icon.png)  
**... what do you mean exactly by "backports" ?**  
Nothing to do, in fact, with the "backdoors" used to spy on your machine running 
proprietary systems...  
The **backport** is a mechanism allowing an application currently hold in the 
Debian development repositories, to be *ported back* to the "stable" version.  
For example, the Debian developers take in the development repositories the most 
recent version of LibreOffice, and re-compile (re-build) the package holding 
the application, while taking care of all the dependencies existing in the 
"stable" version.

More details about *Backports* ine the dedicated page of the Debian wiki 
([https://wiki.debian.org/Backports](https://wiki.debian.org/Backports)). 
If you are looking for specific application, you have two solutions: use 
the search package tool 
([https://backports.debian.org/Packages/](https://backports.debian.org/Packages/)) 
[en] or the search by category 
([https://packages.debian.org/jessie-backports/](https://packages.debian.org/jessie-backports/)).

## Synaptic: the comprehensive package manager

![synaptic](img/synaptic-icon.png)  
**Synaptic** is the comprehensive graphical interface of the Debian package manager. 
It allows a total vision of the proposed packages, whether installed or not. It is 
a lot more detailed than the Software Center, or Apper (see the following chapters) 
since it displays the **full set**  of available packages (including the libraries).

- Provide the same functionality as apt or apt-get.
- You need to provide the administrator password to open qand use Synaptic.
- An active Internet connection is also neede to install or update your software.

### Main interface

The Synaptic windows is divided in 4 areas: the tool bar at the top, the left pane 
allowing different ways of sorting and selecting the packages, the center pane 
displaying the package list itself, and below the pane hosting the description of 
the currently selected package (the selection is done with a click).

In front of each package, you notice a little box (white for non-installed 
packages, green when they are installed, red when they are broken).
Next to this status box, a Debian logo indicates that this package is 
"free" (as in freedom).

![Synaptic: the default interface of the package manager](img/synaptic_default.png)

**The very first thing to do** when you launch Synaptic, is to **click on the 
"Reload" button** in order to update all the information (metadata) concerning 
the repositories, the packages and the available applications.

![](img/synaptic_refresh_icon.png)

Don't hesitate to click on all the menus to explore Synaptic and become more 
familiar with it. It is a good way to discover its numerous functionalities.

Don't be afraid to break your system since nothing will really happen until 
you click on the "Apply" button. On top of that, a message asking for 
confirmation will always be displayed first.

### Managing the repositories

The repositories allow to update and install additional packages.

Open the Synaptic package manager (menu System > Synaptic package manager)

In the top menu bar, click on "Settings, then "Repositories".

The **GNOME** desktop repository management uses an explicit "check" interface:

![Synaptic: managing sources from Gnome](img/deb9-synaptic-sources-gtk.png)

The management of the repositories on the other desktops like **Xfce** or 
**LXDE** is in "text" mode with the displayed addresses:

![Synaptic: managing sources from Xfce](img/deb9-synaptic-repotxt.png)

You'll notice that the list corresponds to the contents of the 
/etc/apt/sources.list file mentioned in chapter 8.1.1.  
Now, you can modify your repository sources at your entire convenience. Simply 
click on a source to modify it, or on the "New" button to add a new source.  
Once your modifications are validated, the application will invite you to 
reload the repositories list in order to take your changes into account.

Note that if you want the "check" interface on Xfce Desktop, you have to 
install the "software-properties-gtk' package.

### Updating the system

Before updating the system, it is necessary to "Reload" the package list, 
by clicking on the corresponding button, or by going in the menu "Edit > 
Reload Packages Information" (or even [Ctrl]+r if you want to use a keyboard 
shortkey). This action checks if the version of the packages residing on 
your system is the most recent or not.

![check repositories informations](img/synaptic_refresh_icon.png)

Then click on "Mark All Upgrades" or menu "Edit > Mark All Upgrades...".

![upgrades request](img/synaptic-upgrade-icon.png)

A new window appears with the list of the packages to be upgraded as well 
as the additional dependencies, if some are required:

![resquest changes list](img/synaptic-upgrade-resume.png)

You only have to click on the "Apply" button, and accept the requested 
confirmation:

![](img/synaptic-upgrade-apply.png)  

![confirm changes](img/synaptic-upgrade-confirm.png)

The system updating process begins with the package downloading, and 
continues with their installation. A message informs you that all the 
changes were applied.

![Synaptic: downloading packages](img/synaptic-upgrade-dwl.png)

![Synaptic: upgrading packages](img/synaptic-upgrading.png)

![system upgraded](img/synaptic-upgraded.png)

### Searching for a software

**If you don't know the name of the package** you need, you can parse the list 
using the filtering by sections, status, origin, etc ...

By example, if you are looking for a game, click on Sections in the bottom part 
of the left pane, scroll down to the "Games and Amusement" section, click on it, 
and the packages concerning games and amusement are listed in the center pane. 

**If you know the name of the package** or if you are looking precisely for 
something, click on the search button (in the top bar) and enter the keywords 
of your search in the window which opens.

Other "Custom Filters" are available. Click on the button to explore them.

#### Look at a package detailed information

By clicking on a package, its description is displayed on the bottom center pane 
of Synaptic. To obtain even more information on a package, right-click on it, 
and select Properties, or go to menu "Packages > Properties".

Then you will know everything - positively absolutely everything - on this 
package; dependencies, installed files, size and version.

### Installing / uninstalling softwares

#### Installing a package

**To install** one or several packages, right-click on the little box in front 
of the package name, and select the "Mark for Installation" option.

![Synaptic: marking a package for installation](img/synaptic_install1.png)

If, in order to be functional, this package requires the installation of other 
packages (the famous dependencies) they are automatically added to the selection.

Then, you simply need to click on the "Apply" button, and confirm the summary 
of the changes to be applied.

![Synaptic: summary of the pending changes](img/synaptic_install2.png)

![Synaptic: changes being applied](img/synaptic_install3.png)

![Synaptic: installation of pacman](img/synaptic_install4.png)

#### Uninstalling a package

Like for the installation, right-click on the little box in front of the package 
name, and select the "Mark for Removal" option. Then click on "Apply".

**The simple removal** keep the package configuration files on your system, 
in case you would like to re-install it, later on.

**To remove also the configuration files** select the "Mark for Complete Removal" 
option (equivalent to the "purge" in a terminal command line)

#### Reinstalling a package

Sometimes we want to re-install a package which is already installed. In that 
case select the "Mark for Reinstallation" option. This allows, for example, 
to update the default configuration for the applicatoin if you modified it.

#### Cleaning useless packages

Often, when software is uninstalled, some packages (the dependencies) remain 
in the system while no longer useful, since all the packages needing them are 
gone. These useless packages can be easily removed with Synaptic.

When Synaptic is launched, click on the "Status" button at the bottom of the left 
pane. If the "Installed (Auto removable)" category shows up, click on it to 
display the corresponding package(s) (see image below):

![Synaptic: auto removable packages](img/synaptic_paquets_inutiles.png)

All you have to do next is a right-click on each package in the center pane, and 
select the "Mark for Complete Removal" option. Once all the packages are marked, 
click on the "Apply" button.

#### Removing configuration residues

Although one choose to completely remove a software, some configuration residues 
might still remain in the system, bur they can be removed with Synaptic.  
Click on the "Status" button at the bottom of the left pane. If the category 
"Not installed (residual config)" shows up, select it (see image below):

![Synaptic: removing configuration residues](img/synaptic_residus_config.png)

All you have to do next is a right-click on each package in the center pane, and 
select the "Mark for Complete Removal" option. Once all the packages are marked, 
click on the "Apply" button.

### Synaptic preferences

"Preferences" is a well-named category, existing in most applications, and 
which is also present here...  
**But keep in mind that Synaptic is a very special case**: it manages the full 
set of software installed on your system. When you remove a program, it does 
not go in the wastebasket (where you could have potentially retrieved it) !

After these scary warnings, let's move to the settings available for Synaptic. 
the Preferences window (launched via menu Settings > Preferences) displays 6 
different tabs:

![Synaptic: Preferences window](img/synaptic_preferences.png)

- **General**: the options in there are rather explicit. Note: it is possible 
to un-tick the option "Consider recommended packages as dependencies", if that 
helps you keeping an ultra-light system. But this could induce problems when 
installing future new packages. Thus an option to be handled carefully.
- **Columns and Fonts**: allows you to display/mask some columns in the 
package list, and define the font, if necessary. 
- **Colors**: you can define here the package colors according to their status.
- **Files**: When you install a piece of software, it is first stored in the 
*cache* (which is a specific folder of the file system) before being uncompressed 
and installed. These packages can occupy more and more disk space as you make 
usage of your computer. Here you can delete them immediately or configure an 
automatic action.
- **Network**: This is the way Synaptic connects to Internet. You should know 
if your situation requires a modification of these parameters.
- **Distribution**: Defines the package upgrade behavior and is very explicit. 
**In case of doubts, do not modify**

**Remember**: by using a terminal (chap.8.2) you can  achieve the same results 
more quickly and with less manipulations.

## Discover: the KDE package manager

The Synaptic package manager is the default interface for the software 
management, but it is sometimes "too" complete. Gnome uses "Software" to manage 
applications in a simplified way, KDE integrates **Discover**, an intuitive and 
efficient software.

**Discover** simply launched from the KDE main menu > Applications> System > 
Software Center:

![Discover launcher](img/deb9-kde-discover-1.png)

![Discover: default interface](img/deb9-kde-discover-2.png)

### Discover: Manage your applications

**Search and install** applications from the dedicated search field or by visiting 
the categories of Discover. A click on the "Install" button is enough:

![Searching an application with Discover](img/deb9-kde-discover-3.png)

![Browsing categories with Discover](img/deb9-kde-discover-4.png)

**Install Plasma desktop wisgets** directly from Discover by visiting the dedicated 
category (here with the "Weather" addon):

![Installing Plasma modules](img/deb9-kde-discover-5.png)

**Uninstaliing an application** with Discover, simply by visiting the "Installed" 
category then click on "Remove":

![Uninstalling with "Discover"](img/deb9-kde-discover-6.png)

A confirmation will be asked for any action on the software. The process will 
then be launched in the background. You can follow the progress of the changes 
from the KDE notification area.

### Discover: upgrading your applications

When KDE notifies you of one or more updates, it is "Discover" that launches 
to perform them:

![update notification](img/deb9-kde-update-2.png)

Simply click on "Update all" and confirm with the administrator password.

![upgrading system on KDE](img/deb9-kde-update-3.png)

As with software management, you can follow the process from the KDE 
notification area.

## Software : the simplified package manager

![gsoft](img/gnome-software.png)  
**Software** is a simplified manager for Debian applications. It allows you 
to search, install, delete or update packages containing your applications. 
You can find it in the "System" category of your menus or directly from the 
Gnome search box by typing "Software".

![Software: default Debian interface.](img/deb9-gnome-software.png)

### Software: searching an application

Directly from the search magnifying glass button, or by clicking on one of 
the displayed categories:

![Searching an application by its name.](img/deb9-gnome-software-search.png)

![Searching applications through categories.](img/deb9-gnome-software-category.png)

### Software: installing an application

**Install an application** simply by clicking on its form and then "Install". 
The administrator password will be requested. You can follow the progress in 
the main window and then launch directly the newly downloaded application.

![Selecting an application for installation.](img/deb9-gnome-software-install-1.png)

![Authentification.](img/deb9-gnome-software-install-2.png)

![You can follow installation process.](img/deb9-gnome-software-install-3.png)

![The installation is successful: you can launch your application directly.](img/deb9-gnome-software-install-4.png)

### Software: remove an application

**Uninstall an application** simply by visiting the "Installed" category 
(at the top of the interface) and clicking on the "Remove" button. You will 
be asked for confirmation:

![Selecting an application for removal.](img/deb9-gnome-software-remove.png)

![confirmation](img/deb9-gnome-software-remove-confirm.png)

### Software: upgrading your applications

**Update your system** from the dedicated section "Updates" which will indicate 
the available and/or already downloaded updates. If no update is available, you 
can check the repositories using the dedicated button at the top left.  
In our example, an update series that includes the "operating system update" 
requires a reboot. You can list all the updated applications in this batch by 
clicking on the relevant entry:

![updates tab](img/deb9-gnome-update-1.png)

![checking available updates](img/deb9-gnome-update-2.png)

You then have to restart by clicking on the dedicated button.

![](img/deb9-gnome-update-btn.png)\

![reboot to apply updates](img/deb9-gnome-update-3.png)

Note that for smaller updates, restart is not necessary.

## Cleaning the system

![clean](img/clean.png)  
Even if the capacity of hard disks increased dramatically during the last years, 
you might need some free space. Several scripts automate the disk cleaning process, 
however I must confess that I prefer to check before using the **rm** command 
(standing for **r**e**m**ove. chap.11.2).

### Disk space information

The first thing to do, of course, is to find out the used space on your disk. 
Several tools are available to you:

**Disk space in terminal mode:** a summary of the disk space usage for each system 
mount points (disks and partitions):

    df -h

**List your folders sorted by decreasing size:**

    du -ks * | sort -nr

**Ncdu:** disk space analyzer in console mode. To launch it, simply type "ncdu" in 
your terminal. To install this software (in administrator mode):

    apt update && apt install ncdu

![Ncdu launched in the user personal folder](img/ncdu.png)

**Baobab:** disk space analyser in graphic mode, integrated in Gnome.

![Baobab: disk space analyzer on Gnome](img/baobab.png)

**Fslint:** utility to find and clean various forms of unwanted extraneous files in 
your file system, like duplicates, broken links, empty folders, wrong encoding, etc ... 
To be manipulated with extreme caution: double check carefully the pending changes 
before validating the whole process.

### Cleaning the packages

**Apt**/aptitude/dpkg are the usual Debian package managers. When you install a 
package its archive-source/deb file is stored in your system 
(in the /var/cache/apt/archives/ folder) to enable a potential re-installation 
without Internet connection. To clean the "apt cache" use a simple command in 
administrator mode (chap.3.8.3):

    apt-get clean

Once the cache of the installed packages is cleaned, you can also remove the 
useless packages from your system, as well as the configuration files. **Warning!** 
Remember to check carefully the list of the packages planed for removal, before 
accepting the operation:

    apt-get autoremove --purge

### Emptying the trash bins

Three different bins must be taken into account:

**The user wastebasket** : ~/.local/share/Trash/ . You can empty it with the 
system file manager, or with a terminal:

    rm -Rf ~/.local/share/Trash/*

**The administrator wastebasket** : /root/.local/share/Trash/ . To empty it 
with the proper manner, use a terminal in administrator mode:

    rm -Rf /root/.local/share/Trash/*

**The external wastebaskets** : locates on your external disks, they are 
usually named '/media/your_id/your_disk/.Trash_1000', where your_id 
corresponds to your login name.

### Purging application caches

Some applications use a "cache" folder, where they store images, videos, and 
miscellaneous information in order to run faster. Usually these data do not 
occupy too much disk space, however if (using the tools described above) you 
detect that a folder becomes too fat, don't hesitate to remove it.

Each application has its own way to manage its own cache: some purge it 
systematically when they close, others store their data in the /tmp folder, 
which will be cleared during the session logout, others keep all their 
information in a specific folder.

For Firefox, as an example, you can purge the cache from the preferences menu, 
and even automate this action every time the application is closed.

### Purging the thumbnails

Every time you open a folder containing pictures or videos, thumbnails are 
created to represent these graphic files. These thumbnails are stored in a 
specific folder to reuse them, rather than being forced to recompute them, 
every time you access this kind of file.  
The problem raised when you delete a graphic file, because its thumbnail is 
kept in the system, and this leads to a certain amount of disk space used 
to store obsolete thumbnails.  
To purge them, it is enough to remove their corresponding folder:

    rm -Rf ~/.thumbnails

This folder will be created again, the next time the system needs to store a 
newly generated thumbnail.

## Installing external ".deb" packages

Debian GNU/Linux uses the package repository system to better manage the software 
and increase the security of your system. But it may happen that you need an 
external package of the ".deb" format.

![noob](img/noob-modarp-circle-icon.png)  
**... but who is this "deb" ??**  
**deb** is the short for "debian", the mother company. To distribute its software, 
Debian uses a specific archive file format: ".deb". It is a compressed format, 
like the ".zip" that you use to save your data. These ".deb" archives are recognized 
by the different Debian package managers (APT and its graphical interface Synaptic) 
and thus can be handled more easily.

### Installation in graphic mode with gdebi

Gdebi is a graphical utility with allows the installation of external packages of 
the ".deb" format, while managing the dependencies.

To install it, look for "gdebi" in your favorite package manager (Synaptic, Apper, 
Packages) or more simply from a terminal in administrator mode using "**su**" (chap.3.8.3):

    apt update && apt install gdebi

When you download a Debian external package, right-click on it and select "Open with gdebi".

### Installation in terminal mode with Dpkg

Dpkg is a software utility handling the packages like apt, but it does not manage the 
dependencies. This means that if you use dpkg to install external packages, you need to 
install the "dependent" packages one by one from your terminal. Dpkg is integrated in 
Debian by default, and must be used in administrative mode.

**To install an external package:**

    dpkg -i package.deb

An error message will let you know if some dependencies are missing, an error message 
will let you know, and then simply install them the classic way with apt:

    apt install dependent_1 dependent_2 ...

Then relaunch the installation of your external package:

    dpkg -i adresse_du_paquet.deb

**To remove an external package:**

    dpkg --purge package_name

## Who is this Sid guy?

First of all, one must know that **several Debian distribution branches** exist in parallel.

Namely the **oldstable**, **stable**, **testing** and **unstable** distributions, as well as 
an **experimental** branch.

The **Stable distribution** is the Debian official distribution, the one released at 
this moment, with is maintained and updated par the Debian teams. The only changes 
made concern the security updates and the bug fixes. It is recommended to favor this Version.

The **Oldstable distribution** is the previous stable version. It is usually supported 
by the Debian teams during one year after the release of the new stable version. Then 
it might live longer if enough individuals or companies continue to assure its maintenance. 
Then it is called a LTS (standing for Long Term Support) distribution: we extend its life span.

The **Testing distribution** is the future Stable version. It is used to prepared the next 
stable version. When everything is OK, when all the bit and pieces are functioning well 
together, when all the features targeted by the Debian teams are included, and after a 
period of software freeze and bug hunting, then the Testing version becomes the official 
new Stable distribution.

The **Unstable distribution**, *nicknamed Sid* is the version which receives all the 
new packages versions, and sits at the cutting edge of innovation, but is not very 
stable: it's a research lab. Nevertheless some brave adventurers use it on a daily basis.

The **Experimental distribution** is not a Debian distribution *per se*, but rather a 
repository where alpha or beta software versions are tested.

![Toy Story © Disney & Pixar](img/toystory.jpg)

All these distributions are given a name picked among the characters of the Toy Story® 
cartoon. Currently, the name of the **stable version is Stretch**, the name of the 
**testing version is Buster**, the name of the **oldstable version is Jessie**, 
the Experimental as no nickname. 

The name of **unstable is Sid**, but who is this Sid guy?  
Sid is the little boy, in the Toy story® cartoon, who breaks all his toys 
([http://pixar.wikia.com/wiki/Sid_Phillips](http://pixar.wikia.com/wiki/Sid_Phillips)).  
![sid](img/sid.jpg)  

More detailed information on the dedicated Debian Wiki 
[https://wiki.debian.org/DebianUnstable](https://wiki.debian.org/DebianUnstable).

