# ![démarrage](img/launch.png) Fast boot after installation

First things to do after installation: Wi-fi network connection, screen resolution, 
printer configuration ... everything you'll need to make your workstation operational.

## Network connection setting

![wifi](img/wireless.png)  
Using either an Ethernet wired connection or a Wi-fi wireless connection, your Debian 
system is expected to access the network. The Ethernet connection is recognized 
natively. As far as the Wi-fi is concerned, it is sometimes necessary to use a 
non-free driver.

- **If your Wi-fi connection is recognized natively**, jump directly to the Network 
manager sections (forthe Gnome, Mate, Cinnamon or Xfce desktop), Wicd (for the LXDE 
desktop) or the connection editor under KDE.
- **If you network device is not functioning**, you need to go to the "material 
recognition" section (chap. 6.1.4).

### Network-manager-gnome

This is the network manager delivered with Gnome, Mate, Cinnamon, and Xfce. It is 
accessible from its icon within the notification area (here on Gnome and LXDE):

![Network management on Gnome](img/deb9-wifi-gnome-1.png)

![Network management on LXDE](img/gnome_networkmanager2.png)

All the active or inactive connections are listed here, and the Wi-fi networks 
are detected as well. Left-click on the network you want to connect to, and a 
dialog box will pop up. Select your network then "Connect". A password is then 
required (the one given by your Internet Service Provider). Here on the Gnome desktop:

![network selection](img/deb9-wifi-gnome-2.png)

![network private key](img/deb9-wifi-gnome-3.png)

Then enjoy your Wi-fi connection:

![wi-fi network connected](img/deb9-wifi-gnome-4.png)

To tweak the Wi-fi settings, select the dedicated entry in the main menu:

![network configuration menu](img/deb9-wifi-gnome-5.png)

You will be able to modify the connection settings, and even "Forget" it:

![network selection](img/gnome_networkmanager7.png)

![network settings](img/gnome_networkmanager8.png)

![forget the network](img/gnome_networkmanager9.png)

### Wicd

**Wicd** ([https://wiki.debian.org/WiFi/HowToUse#Wicd](https://wiki.debian.org/WiFi/HowToUse#Wicd)) 
(**W**ireless **I**nterface **C**onnection **D**aemon) is an independent network manager 
delivered with LXDE. However, it can be used on any other desktop.

Its interface is slightly different compared to the one of the Gnome manager, 
but the functionality is the same.

Wicd is available from its icon in the task bar (usually at the bottom right, 
in the notification area); A left-click opens the main interface which let 
you select your network access:

![Wicd: connection properties](img/deb9-lxde-wicd-1.png)

If the network does not show up, verify first that in the Wicd "Preferences", 
the right interface (the very one found in the previous chapter: "wlp2s0" 
for example) is selected for the Wi-fi network.

![Wicd: Connection manager preferences](img/deb9-lxde-wicd-2.png)

### Éditeur de connexion KDE

On KDE, the network connections are managed like on the other desktops: through 
a graphical interface. A left-click on the network icon in the notification 
area and you access to the list of available networks. A click on "Connect" and 
KDE ask you to enter the network Wi-fi key:

![KDE: network selection](img/deb9-kde-wifi-1.png)

![KDE: network private key](img/deb9-kde-wifi-2.png)

When this is done, you can connect your computer using this Wi-fi network 
and/or modify its settings.

![KDE: network connexion editor](img/deb9-kde-wifi-3.png)

### Check if the driver is present

**Driver** or **Firmwares** are microprograms often provided by the manufacturer. 
The kernel must load this driver into the Wi-Fi card itself.

In order to check if the driver is present, we are using the command:

    /sbin/ifconfig

which should return a result like:

     enp4s0     Link encap:Ethernet  HWaddr xx:xx:xx:xx:xx:xx  
                BROADCAST MULTICAST  MTU:1500  Metric:1
                RX packets:0 errors:0 dropped:0 overruns:0 frame:0
                TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
                collisions:0 lg file transmission:1000 
                RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
    
      lo        Link encap:Boucle locale  
                inet adr:127.0.0.1  Masque:255.0.0.0
                adr inet6: ::1/128 Scope:Hôte
                UP LOOPBACK RUNNING  MTU:65536  Metric:1
                RX packets:552 errors:0 dropped:0 overruns:0 frame:0
                TX packets:552 errors:0 dropped:0 overruns:0 carrier:0
                collisions:0 lg file transmission:0 
                RX bytes:37827 (36.9 KiB)  TX bytes:37827 (36.9 KiB)
    
     wlp2s0     Link encap:Ethernet  HWaddr xx:xx:xx:xx:xx:xx  
                UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                RX packets:638993 errors:0 dropped:0 overruns:0 frame:0
                TX packets:412031 errors:0 dropped:0 overruns:0 carrier:0
                collisions:0 lg file transmission:1000 
                RX bytes:617235341 (588.6 MiB)  TX bytes:42145548 (40.1 MiB)

If the interface named something like "wpl2s0" or "wpl2s1" does not appear in the list, 
this means that the Wi-fi driver is missing.

#### Identifying a network device, and installing its driver

If your network connection is not functioning, it is usually a driver issue. In order 
to select the right one, we need to identify first the network device.

Within a terminal, in user mode, launch the command:

    lspci | grep -i "net" | cut -d: -f3

which will return a result like:

    Intel Corporation 82567LM Gigabit Network Connection (rev 03)
    Broadcom Corporation BCM4322 802.11a/b/g/n Wireless LAN Controller ...

Here, the detected Wi-fi device is the **Broadcom BCM4322**

If I run a simple Internet search, like "*Debian Broadcom BCM4322*", I found 
quickly the right page 
([https://wiki.debian.org/bcm43xx](https://wiki.debian.org/bcm43xx)) which 
tells me to install the **firmware-b43-installer**.

The Wi-fi connection setting is a very touchy subject for the GNU/Linux 
distributions, due to the non-free nature of the drivers to be installed. 

More information on the Debian Wi-fi Wiki: [https://wiki.debian.org/éWiFi](https://wiki.debian.org/WiFi).

## Configuring your printer

![print](img/print.png)  
C.U.P.S 
([https://wikipedia.org/wiki/Common_Unix_Printing_System](https://wikipedia.org/wiki/Common_Unix_Printing_System)), 
is an acronym for **C**ommon **U**nix **P**rinting **S**ystem, and as its name 
suggests it, a printer management system created by Apple Inc. for OSX and other 
Unix-like systems. It is used by default on Debian to manage printers. To achieve 
this, CUPS uses the Internet Printing Protocol (ipp) to drive both the local 
and the network printers.  
What is enough to remember is that CUPS is the tool managing the printers.

If you chose an exotic installation, you might need to install yourself 
some Debian packages. 
Within a terminal in administrator mode (thanks to "su" (chap.3.8.3) :

    apt update && apt install --install-recommends task-print-server

**Compatibility**

Likewise Wi-fi devices, printers need external drivers. However their detection 
by default is much better, which enables Debian to embed a simplified printer 
setting interface (detailed in the first section of this chapter). Methods 
may vary from one manufacturer to another.

- for Brother: visit the dedicated page: 
[http://support.brother.com/g/b](http://support.brother.com/g/b),
- for Epson: add the following repository (chap.8.1.3) to your "sources.list" 
file: "deb http://download.ebz.epson.net/dsc/op/stable/debian/ lsb3.2 main" (sans les "")
- for HP: make sure the 'hplip' package is installed on your machine. If your 
printer is very recent, visit the dedicated page 
[http://hplipopensource.com/hplip-web/index.html](http://hplipopensource.com/hplip-web/index.html), 
download the packages corresponding to tour printer for the Debian system (file 
ending by ".deb") and install these packages using either the **dpkg** or 
**gdebi** tool (chap.8.7).

To check your printer compatibility, you can visit the dedicated page of the 
openprinting.org site: 
[https://www.openprinting.org/printers](https://www.openprinting.org/printers).

![noob](img/noob-modarp-circle-icon.png)  
Regardless of the chosen method, don't forget to plug the power cord at both ends, 
to verify that the paper tray is not empty, to connect the printer either 
directly to your computer, or to the local network, and turn its power on.

### Simplified graphical method with system-config-printer

The Debian GNU/Linux desktops include the **system-config-printer** tool, 
a simplified graphical manager to add and configure your printer.

If you followed the Debian classic installation procedure, this tool is already 
present on your system and can be found usually at "Sysem > Print Settings", or 
in the Control Centers for Gnome or KDE. If you don't find it, simply open a 
terminal and launch:

    system-config-printer

In the first window which opens you can press the "Add" button to add a printer.

![System-config-printer: defaut interface](img/system-config-printer-01.png)

Then you access the "New Printer" window which presents the local or network 
"Detected Devices" list. Select your printer and click the "Forward" button:

![printer selection](img/system-config-printer-02.png)

If your printer was correctly detected, its driver has been selected for you. 
You can describe your printer in the next window. Once you are done with the 
modifications click on the "Apply" button:

![printer description](img/system-config-printer-03.png)

The installer then ask you if you want to test the printer:

![print test page?](img/system-config-printer-04.png)

The printer is now correctly added to your machine. It is available for the 
printing tasks executed by any of the installed applications on your system 
(LibreOffice, Evince (for pdf files), etc.).

To change your printer settings, double-click on its icon:

![configuration de l'imprimante](img/system-config-printer-05.png)

Small hint: to change the printer behavior in case of error, go to your printer 
settings, click on the "Policies" category,  and select the "**Abort job**" 
option (instead of "**Stop printer**") in the "Error policy" field.

### Universal graphical method with CUPS

The simplified interface is handy, but the classic CUPS interface is no longer 
as austere as it used to be. You can access it from your Internet navigator 
since CUPS is a *print server* offering a web interface.

So let's meet on the page 
[http://localhost:631/printers/](http://localhost:631/printers/) which displays 
all the detected printers (don't forget to connect your printer either directly 
to your computer, or to the local network, and to turn its power on).

![CUPS: "Printers" tab of the web interface](img/cups1.png)

A left-click on your printer opens the dedicated page:

![active printer page](img/cups2.png)

which includes the "Maintenance" and "Administration" menus. Depending on the 
selected operations, the administrator password will be requested. (chap.3.8.3).

![CUPS cfg1](img/cups3.png)  ![CUPS cfg2](img/cups4.png)

More information of the Debian SystemPrinting Wiki: 
[https://wiki.debian.org/SystemPrinting](https://wiki.debian.org/SystemPrinting).

## Check for updates

![update](img/update.png)  
**Debian updates** have nothing to do with *Windows' updates*...  
The GNU/Linux system updates are the **direct evidence of one of the free 
software strengths: the public sources**.

As opposed to the proprietary systems, hiding their software flaws (since 
they *sell* it) as long as possible, the free distributions publish their 
software flaws as soon as they are discovered and fix them immediately.

So, when a small window suddenly pops up and invite you to update your 
system, say yes, of course, and your computer will thank you a all lot.

- Updates using a *terminal* (chap.6.3.1)
- Updates using the *Synaptic* package manager (chap.8.3.3)
- Updates using *Discover* on KDE (chap.8.4.2)
- Updates with the simplified Debian software center (chap.8.5.4)

### Update with a terminal

The terminal, this little box which frightens you so much, whereas it 
is actually your friend ... yes yes ... your friend.

You want a proof ? Easy: we are going to update the entire system using just one line.  
We start by opening a terminal and log in as administrator. That is (depending 
on your environment) from the Applications menu > System > "Administrator Terminal", 
or in the more classic way, open a terminal and type the command:

    su

In both cases, the administrator password is requested. Once connected 
as "root" in the terminal, cut and paste this line:

    apt update && apt dist-upgrade

![noob](img/noob-modarp-circle-icon.png)  
**... wait a minute, what does that mean exactly?**

- "apt": the command to invoke Debian package manager ;
- "update": reload the information concerning the available packages ;
- "&&": once this is done without error, launch what follows ;
- "apt": the same application verifies and applies the modifications ;
- "dist-upgrade": execute a complete update of the installed applications, adding 
or removing packages as necessary. A confirmation to proceed will be requested. 
For a complete example have a look on the Administrator example  (chap.3.8.3).

### Updates notification

The GNU/Linux desktops use **Software** or the **Synaptic package manager** on 
the Gnome, Xfce and LXDE interfaces, or **Discover** on the KDE interface, and 
both tools include a **notification** function.  
Thus, your system checks the repositories on a regular basis (when the network 
is available, of course), without even bothering you and displays a notification 
at the beginning of a new session when some updates are available. It is enough 
to accept the updates, or to click on the notification bubble opening up, to 
display the update manager tool integrated in your system.

![Update notification on the KDE interface](img/deb9-kde-update-1.png)

If you want to **manually check** for available updates, you can use the 
procedure listed at the beginning of this chapter.

## Retrieve your email account

![mail](img/mail.png)  
If you have an **online messaging service** like gmail.com or free.fr, you can 
access it from your Firefox web browser (launches from the application menus 
in the "Internet" section). Enter the address of your online account provider 
(openmailbox.org, yahoo.fr, orange.fr, google.com ...) in the address bar and 
launch by pressing [Enter].

If you do not have an e-mail account, or you want to create a new e-mail account, 
we recommend that you use the services that respect your privacy. For example:

- [https://protonmail.com/](https://protonmail.com/)
- [https://www.openmailbox.org/](https://www.openmailbox.org/)
- [https://www.mailoo.org/](https://www.mailoo.org/)
- [https://www.net-c.com/](https://www.net-c.com/)
- [https://lavabit.com/](https://lavabit.com/)
- [https://www.toile-libre.org/](https://www.toile-libre.org/)
- [http://www.zaclys.com/](http://www.zaclys.com/)
- [https://webmail.vivaldi.net/](https://webmail.vivaldi.net/)
- [https://www.mail.be/](https://www.mail.be/)
- [https://tutanota.de/#%21home](https://tutanota.de/#%21home)
- [http://mailfence.com/](http://mailfence.com/)
- mailbox and web domain: [https://www.gandi.net/domaine](https://www.gandi.net/domaine)

If you are using a **local e-mail client**, a dedicated software, Debian offers 
severals tools but all working on the same model: Gnome uses "Evolution", 
KDE includes "KMail" and Xfce integrates **Thunderbird**.

### Thunderbird: your free mail client ![thundernird](img/thunderbird-mini.png)

**Thunderbird** 
([https://www.mozilla.org/fr/thunderbird/](https://www.mozilla.org/fr/thunderbird/)) 
is primarily an **emailer**, but it is also a communication hub that allows you 
to read and write within newsgroups, chat with others (discussion group), and 
read RSS feeds provided by some websites.

If you have two or more email accounts, it might not be very practical to access 
each of them using the browser! Thunderbird is an application that brings 
together all the mail boxes of all your email accounts, in one single 
convenient interface.

To open Thunderbird, from an application list or a menu, select the "Mail client" 
from the "Internet" category.

#### Thunderbird setup

At first startup, **Thunderbird** provides temporary configuration interface 
and a foreground window that allows you to create an account with two 
suppliers and partners.

If you want to take advantage of this option, fill in the fields and let you 
guide, the configuration will be automatic.

If you are not interested you can click on "Skip this step and use my existing address".

Another window opens. Then provide the requested information about your account. 
You can choose whether Thunderbird must remember the password. If you uncheck 
this option, you must enter your password each time you connect.

![Thunderbird: mail account configuration](img/thunderbird-1.png)

Click on "continue". Thunderbird then searches in its database then specific 
parameters for your eamil account. When finished click on "done".

![Thunderbird: mail account verification](img/thunderbird-2.png)

Thunderbird now displays your account details in the right column, starts 
downloading all your emails and, depending on your provider, all your contacts 
(this may take sometime). Now click on your account in left pane to unfold it, 
then on "Inbox". The interface takes its normal appearance:

![Thunderbird: default display](img/thunderbird-3.png)

#### Thunderbird interface

The Thunderbird interface is relatively intuitive. In the top tool bar you can 
check your mail, compose a new message, open a chat session, access your address 
book, put a label on a message, or filter your messages.  
At the right of the menu bar you can find the Thunderbird menu, represented 
three small horizontal bars, from which you set your preferences, and launch 
various actions.  
For example if you click on the Message menu (or right-click on given message), 
a list of actions is proposed like Reply, Forward, Archive, Mark as Junk, etc.

The right panel is reserved for the "Lightning" calendar, very intuitive: a 
click on a date will open the assistant to make appointments.

If you are **looking for a specific message** you have several options:

- Type few characters in the search field of the top menu bar;
-é Click on one of the column headers ("Subject", "From", "Date", "Attachments", 
"Star", "Tag" if you tag your messages, Read/Unread status, etc.): your messages 
will be sorted immediately according to the selected criterion (defined by the 
header name), which will let you find quickly an old message rather than scrolling 
through an endless list of emails. By clicking a second time on the column header 
the sort will be done in reverse order. Thus, by using this tool, you can customize 
the way your messages are presented according to your own criteria.

#### Retrieve other email account(s)

If you have more email accounts you want to access via Thunderbird, click on the 
main menu (the three small horizontal bars at the right of the tool bar) and 
select "New Message" > "Existing Mail Account ..." and continue the Mail Account 
Setup procedure like explained above.

#### Thunderbird settings

In the modern presentation of Thunderbird (as well as other communication tools 
like Firefox and Chromium, for example) the application menu is define by the 
three small horizontal bars at the top right of the window. From this menu you 
have access to the parameters of the application.  
If you prefer the classic presentation, with the menu bar at the top of the 
window, click on this Thunderbird menu and then "Preferences" and tick 
"Menu Bar" on.

It is advisable to visit the Preferences setting window, where you can define 
the handling of junk mails, define a master password for all your account, 
customize your tag list, among all the settings available in there.

#### Protect your new and forwarded e-mails

![noob](img/noob-modarp-circle-icon.png)  
**... Encrypt your e-mails ... what does that mean?**  
Let us take the example of the regular mail, sent via post-mail. You send a 
postcard to your mother-in-law for example, you know that what is written on 
it can be read by the postman or anyone else. Well, with your e-mails, it's 
exactly like the postcard,if you do not encrypt them.

"*Does not matter*" you say, "*I don't care if someone knows that I wrote 
to my mother-in-law that the weather is fine during our vacations*". Perhaps. 
But you can write more than just mundane wordings in your messages, and you 
never know what can happen to them. You are not convinced? I ask you to 
reconsider if you really have nothing to hide by visiting this page on the 
**FSF** website: [https://emailselfdefense.fsf.org](https://emailselfdefense.fsf.org).

![locked](img/locked.png)  
**To effectively protect your electronic exchanges, use EnigMail!:** 
[https://emailselfdefense.fsf.org](https://emailselfdefense.fsf.org).

## Login configuration

GNU/Linux systems follow the principle of rights and permissions by default. 
When you install Debian, a password is requested for the primary user. 
This password is requested at the beginning of the session.

![Gnome : demande de mot de passe au lancement de la session](img/deb9-gdm-mini.png)

If you are using Debian in single-user mode, you can disable this password request 
in order to begin your working session directly when the computer is started.  
Debian uses three different default connection managers: **GDM** (on Gnome & 
Cinnamon), **LightDM** (on MATE, LXDE & Xfce) and **SDDM** (on KDE).

### Autologin with GDM

**GDM3** ([https://wiki.debian.org/GDM](https://wiki.debian.org/GDM)) is the 
default Gnome Display Manager for the default desktop environment on Debian 
9 "Stretch". To enable automatic login:

- From the system widget, click on your nickname then "Account settings":

![user account configuration](img/deb9-gnome-gdmcfg1.png)

- Unlock the application, the administrative password will be asked.

![unlock user preferences](img/gdm3_guiedit3.png)

- Activate the automatic login:

![auto login enabled](img/gdm3_guiedit4.png)

### Autologin with LightDM

![LightDM](img/deb9-lightdm.png)

You can setup **LightDM** 
([https://wiki.debian.org/LightDM](https://wiki.debian.org/LightDM)) by editing 
its main configuration file. To enable autologin, you'll have to edit it with 
a root terminal (chap.3.8.3).

Open a terminal and become root with the "**su**" command. The administrator 
password will be asked. Once connected in root terminal, copy (to backup) 
and edit the dedicated file with in 2 commands:

    cp /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.bak
    nano /etc/lightdm/lightdm.conf

The second command opens the configuration file in the CLI **Nano** editor. 
Use arrows to scroll and locate those lines:

    #autologin-user=
    #autologin-user-timeout=0

Remove the "#" (uncomment) in front of each line then add your login like that:

    autologin-user=my_login
    autologin-user-timeout=0

With "my_login" remplaced by **your login**.

Save the file with the [Ctrl]+x shortcut, then answer "Y" for "yes":

![LightDM: configuration file edition in Néano CLI editor](img/lightdm_termedit.png)  

You can close your terminal: your password won't be asked anymore at login time.

In case of troubles, just enter this command in administrator mode to restore your 
previous LightDM configuration:

    mv -f /etc/lightdm/lightdm.conf.bak /etc/lightdm/lightdm.conf

### Login configuration on KDE

![SSDM: login screen on Debian 9](img/deb9-kde-sddm-debian-breeze.png)

**KDE** is a truly complete environment and **SDDM** (Simple Desktop Display Manager) 
provides you with a graphical interface to configure your login screen. The 
configuration tool will allow you to manage the wallpaper, the language used, some 
options and of course, the automatic login.

To access SDDM configuration, direction the main menu > Applications > Settings > 
System Settings > Startup and Shutdown:

![KDE: launching SDDM configuration](img/deb9-kde-sddm-config-1.png)

Enable automatic login from the "Advanced" tab of the SDDM configuration interface:

![SDDM: graphical configuration tool](img/deb9-kde-sddm-config-2.png)

Select the "Automatic login" checkbox and select the user from the "User" drop-down 
menu. As this action changes the system settings, you will be prompted for the 
administrator password. On next login, your password won't be asked.

## Navigate on Internet

![web](img/browser.png)  
The primary function of a web browser is to let you consult information available 
on the Web (World Wide Web).  
The user gives the browser the web address of the resource to consult. There are 
three ways to give a web address:

- **Type yourself the web address** in the address bar of the browser,
- Select a favorite in your **list of bookmarks**, knowing that each is associated 
with a favorite web address,
- **Follow a link** on a web page, knowing that each link is associated with a web address.

The browser connects to the web server hosting the target resource. The communication 
protocol commonly used is HTTP or HTTPS (secure version).

- If the resource is an HTML page, a compatible video file, or a PDF file, the 
browser display a page.
- If the resource is unknown or not handled by the browser, the choice is yours: 
download or open the resource with an external application.

### Firefox: a free web browser ![firefox](img/firefox-mini.png)

**Firefox** 
([https://www.mozilla.org/en-US/firefox/desktop/](https://www.mozilla.org/en-US/firefox/desktop/)) 
is an open and free web browser, developed and distributed by the **Mozilla Foundation** 
([https://www.mozilla.org](https://www.mozilla.org)) with the help of thousands of 
volunteers using the free software/open source development methodology 
(dixit [wikipédia](https://wikipedia.org/wiki/Mozilla_Firefox)).

Debian integrates **Firefox-ESR**, the "**E**xtended **S**upport **R**elease" 
version, aimed at large organizations (and small ones) for mass deployments. 
This version is maintained for 10 months from its release date and only benefits 
from security updates, ensuring a stable and secure browser.

![Firefox on Debian](img/firefox.png)

The main window consists of the following elements (from top to bottom):

- **The tab bar** displays the open Internet pages and allows you to switch from 
one to another with a single click.
- **The tools bar** displays: the forward button, the address bar, the search 
field, bookmarks button, the main Firefox menu.
- **the browsing pane** displays the web pages contents.

**Customize and configure** Firefox through its main menu that appears when 
clicking on the 3 bar menu icon (at the right end of the tool bar):

- The first line of the menu allows you to perform **common editing 
operations** (cut/copy/paste).
- Then comes the **zoom line**... it's explicit.
- Several **tools** are available to navigate in "private mode", view your 
history or directly print a web page.
- The **Preferences button** takes you to the main configuration menu: 
8 tabs with explicit entries.
- The **Addons button** takes you to the extensions configuration menu 
(active or inactive).
- The **Customize** button launches the "edit mode" of Firefox: the left pane 
presents "Additional Tools and Features" that you can move either to the Firefox 
main menu (currently open) or directly in the tool bar, and vice versa ... Well, 
you tweak the look of the browser by just dragging and dropping items in the interface. 
When you are done click on "Exit Customize" at the bottom of the main menu. 
- The "**?**" button takes you to the official Firefox on-line help. Mozilla 
contributors have worked well. Online help 
([https://support.mozilla.org/en/products/firefox](https://support.mozilla.org/en/products/firefox)) 
is comprehensive and detailed: you can explore there all the possibilities of your browser.

To add features to your Firefox browser, navigate to Preferences > Add-ons. 
In the tab that opens, select "Get Add-ons" and choose from the available modules 
([https://addons.mozilla.org/fr/firefox/extensions/](https://addons.mozilla.org/fr/firefox/extensions/)).

![Firefox: Add-ons catalog](img/firefox_addons.png)  

## Watch a video

![video](img/video.png)  
Debian GNU/Linux comes with a video player on each Desktop Environment (DE like 
Gnome or Xfce). Debian natively recognizes most common video formats (ogv, mkv, 
mp4, avi, webm, etc).

Once your system is installed (or in Live session), a double-click on a video 
file will open it with the default player of your DE.

For Gnome or Cinnamon, it's the "Videos" player (Totem) that is automatically 
launched when double-clicking on a video:

![Totem: "Videos" and "Places" windows on Gnome-Shell](img/totem_sur_gnome.png)

Its use is very simple and intuitive. At the slightest movement of the pointer, 
the playback menu is displayed and allows you to browse the video file, set 
loop playback (the vertical bar formed by 3 points) or adjust the volume. 
The main menu gives you access to other functions:

![Totem: video player menu](img/totem_menu.png)

Each DE integrates its own player, all featuring the same basic functions, 
largely enough for a first use of a Debian system.

As usual on Debian, you can add the multimedia software of your choice. 
I'll let you visit the list of applications available on the Debian wiki 
([https://wiki.debian.org/Multimedia](https://wiki.debian.org/Multimedia)).

The Xfce desktop comes with a multi-platform media player widely used by 
Windows®: **VLC** ([https://www.videolan.org/vlc/](https://www.videolan.org/vlc/)), 
a way to get started on Debian without changing your habits.

**VLC** is a free media player and a system capable of playing most multimedia 
files as well as DVDs, Audio CDs, VCDs, and various broadcasted protocols.

![VLC: open a media from the "Media" menu](img/vlc2.png)

VLC can also play network streams (podcasts), listen to an online radio, 
capture the screen, as well as encode a stream to save it.

## Listen to music

![audio](img/audio.png)  
Debian GNU/Linux comes with a media player for each DE. Some allow the management 
of a large music library, including sort management, play lists, cover artwork, 
etc (such as Amarok or Rhythmbox described in the following section), others are 
simple, lightweight and easy to master (Such as Audacious 
[http://audacious-media-player.org/](http://audacious-media-player.org/) or XMMS 
[https://xmms2.org/wiki/Main_Page](https://xmms2.org/wiki/Main_Page)).

### About audio format natively recognized

[Wikipédia](https://en.wikipedia.org/wiki/Audio_file_format) is my friend ...

> An audio file format is a file format for storing digital audio data on a computer system. The bit layout of the audio data (excluding metadata) is called the audio coding format and can be uncompressed, or compressed to reduce the file size, often using lossy compression. The industry has produced many formats for either production or distribution  
> The program element which transforms the signal into a file and the file back into a signal is called a codec ([https://wikipedia.org/wiki/Codec](https://wikipedia.org/wiki/Codec)), an abbreviation for **co**der-**dec**oder.

Debian recognizes (among other things) the 4 main default formats (mp3, ogg, flac, 
wav). If you need to install codecs or non-free audio software, you will need to 
modify your repositories (chap.8.1.3) to add the "contrib" and "non-free" sections.

### Rhythmbox: your music player

**Rhythmbox** 
([https://wiki.gnome.org/Apps/Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox)) 
is the default music player in the Gnome environment, which allows you to play 
and organize your music collection.

Here are its main features:

- Sort by artists, albums or genres..
- Playlist management.
- Display of jackets and lyrics.
- Management of podcasts and web-radios.
- Integration of [Jamendo](http://www.jamendo.com), 
[Magnatune](http://www.magnatune.com/) and [Last.fm](http://www.lastfm).
- Support for portable players (MTP or iPod).
- Support infrared remote controls.
- Sharing and playing music on a local network.

At first launch, Rhythmbox scans your "Music" folder, but you can add more 
folders to your music library.

![Rhythmbox: default view](img/rhythmbox1.png)

![Rhythmbox: menu](img/deb9-musicmenu.png)

![Rhythmbox: preferences settings](img/rhythmbox3.png)

Rhythmbox integrates a plugin system that adds functionality to the player.

![Rhythmbox: configure plugins](img/rhythmbox4.png)  

Default audio player of the Gnome desktop, it benefits from a complete 
integrated help:

![Rhythmbox: complete help](img/rhythmbox5.png)

The main desktops also have their dedicated player, which works on the same 
principle: a music library scanned by the application which offers you simple 
reading or by "playlists", options, preferences and plugins.  
Let your mouse wander, you do not risk anything: a confirmation will be asked 
for each action involving the modification or the deletion of your musical files.

## Work on word processor

![office](img/office.png)  
**Debian** integrates by default the full office suite LibreOffice which allows 
you to work on documents of any type format coming from different office suites.

If you don't need such a comprehensive tool suite, you can use the **Abiword** 
([https://packages.debian.org/stretch/abiword](https://packages.debian.org/stretch/abiword)) 
or **Gnumeric** 
([https://packages.debian.org/stretch/gnumeric](https://packages.debian.org/stretch/gnumeric)) 
tools, lighter while maintaining a high level of compatibility.

### LibreOffice the free office productivity suite

**LibreOffice** ([http://libreoffice.org/](http://libreoffice.org/)) is a free 
(as in freedom) office suite that offers tools for word processing (Writer), 
spreadsheet (Calc), presentation (Impress), drawing (Draw), database (Base) 
and editing mathematical formulas (Math).

![Libreoffice: default view](img/deb9-libreoffice.png)

LibreOffice is the default office suite for Debian 9 and will be integrated 
with the main desktops.  
There are many possibilities for LibreOffice. The official wiki of the community 
is very well done and will provide you with an efficient and complete help (no 
need to reinvent the wheel ...): 
[https://wiki.documentfoundation.org/FAQ](https://wiki.documentfoundation.org/FAQ)

Good reading.

## Edit your photos with the Gimp

Debian includes the graphic manipulation software **the Gimp**.

![gimp](img/logo_gimp.png)  
**GIMP**, standing for **G**NU **I**mage **M**anipulation **P**rogram, is an image 
processing software. Often considered as a competitor of Adobe™ photoshop™, it is, 
at any rate, a safe alternative with all the same functionalities, and, on top of 
it, it is free!

Available for a whole range of OS, it runs naturally on Linux, MacOsX, BSD and Windows®.

There are already very good documentation concerning The GIMP, and this page is 
only to gather, in few lines, the most basic hints, without having to browse 
through dozens of sites hoping to find its happiness.

### Interface Overview

At first launch, Gimp displays 3 windows with several elements:

![The Gimp on Gnome-Shell](img/thegimp.png)

**1** The tools box - **2** The tools options - **3** The Gimp menu bar - **4** 
The active picture - **5** & **6** The tabs displaying the values of tools and layers.

- **The toolbox** contains the icons for the various Gimp tools. You can add or 
remove them from the menu bar > Edit > Preferences > Toolbox. The function of 
each tool is displayed on a pop-up window if selected.
- **The tool options** show the parameter values of the tool being used: the 
size and hardness of the brush, for example, or the opacity of the filling...
- **The menu bar** gives you access to all functions and possibilities of the Gimp.
- **The active image** is displayed in the main window: the changes made are 
directly visible.
- The right window contains various tabs that you can modify or delete from the 
small arrow at the top right of the tab.

By default, the first tab displays the active layers in the image.

![noob](img/noob-modarp-circle-icon.png)  
**... What does those "layers" exactly means??**  
**Gimp uses the principle of layers**, ie the superimposition of images for a 
final rendering. It is the same principle as recording a piece of music: the 
instruments are all recorded on separate "audio tracks", the voices also, then 
"mixed" all together to obtain the final piece of music.  
For the layers of Gimp, it is the same: you separate the different elements on 
layers and the final image will be exported in the format of your choice 
(JPEG, PNG ...).

**The default format of Gimp is "xcf"**, a format that allows to preserve the 
independence of the layers and thus, to be able to modify an element without 
touching the other layers.

### First Tips

**Prepare a folder with a copy of the images to modify:**

It is often said, **the backup is your friend** and it is also true for image 
editing. Before working on your project, create a folder where you will place 
a copy of your original image as well as all the elements you want to add 
(other images to process, modify, your graphics resources in short).

**Record your work regularly in ".xcf" format:**

Gimp uses the default "xcf" format to save your project. This format allows to 
save the layers and therefore, permits a fine modification of your work. 
**To save** in xcf format, direction the menu "File"> "Save". Once your project 
is finalized, you can **export your project** in a compressed format (jpeg, 
png, gif) from the menu bar: "File" > "Export as".

**Switch to single-window mode:**

If the default interface with its 3 windows disturbs you, Gimp has provided a 
"single-window" mode available from the menu "Windows" > "Single window mode".

![The Gimp en mode "fenêtre unique"](img/thegimp_mono.png)

**Have Fun!**

One way to discover **the Gimp** is to test, try, have fun editing your family 
photos for fun... Create a folder to play with Gimp and place your favorite 
photos or images and start to *gimp-it* 

**Tutorials**

The Gimp website provides a large choice of tutorials where you could start 
to play: [https://www.gimp.org/tutorials/](https://www.gimp.org/tutorials/)

**Print an image**

If Gimp prints only white pages, and the preview is desperately showing a blank 
page too, you can fix this problem by installing the gimp-gutenprint package in 
administrator mode (chap.3.8.3) :

    apt install gimp-gutenprint

Then, to print a image, all you have to do is: "File > Print with Gutenprint". 
A page is open where you can set up your printer and print layout.

