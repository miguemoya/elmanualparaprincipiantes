## Fuentes de construcción para [el manual del principiante](https://lescahiersdudebutant.fr/index-es.html)

Este repositorio integra las fuentes a construir:

- [la documentación en línea](https://dflinux.frama.io/thebeginnershandbook/)
- [la versión PDF](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.pdf)
- [la versión PDF para imprimir](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook_print.pdf) (en blanco y negro)
- [la versión ePUB](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.epub)
- [la versión HTML-standalone](https://framagit.org/dflinux/thebeginnershandbook/raw/master/docs/the_beginners_handbook.html.tar.gz)
- [el paquete debian](https://framagit.org/dflinux/thebeginnershandbook/tree/master/docs/paquet_debian/)

### Contenido

- mkdocs.yml : archivo de configuración para exportar a mkdocs
- .gitlab-ci.yml : archivo de configuración para la creación del sitio en línea a través de `páginas`.

- docs/css/
  - cahiers.css : hoja de estilo para mkdocs
  - epub.css : hoja de estilo para exportar a ePUB
  - les_cahiers_du_débutant.png : Cubierta para el ePUB

- docs/img/ : imágenes para la documentación en 96dpi

- docs/paquet_debian/ : dossier de creación para el paquete debian.

- docs/intro[pub].md : primera página para las versiones PDF y ePUB
- docs/foo.md : fuentes de la documentación
- docs/foo.phatch : listas de acción para phatch (optimización de imágenes)
- docs/pandoc-lcdd.txt : bloque de yaml para pandoc
- docs/pandoc-tpl.foo : plantillas para exportar pandoc en PDF y HTML

- docs/mklcdd : generar todas las versiones (considere cambiar la fecha de la versión del paquete debian)
- docs/.mklcdd-foo : scripts independientes para cada versión

### Depende de

- mkdocs
- pandoc
- ps2pdf
- exiftool
- equivs
